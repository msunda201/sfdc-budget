trigger Rate_Update on Rate__c (after update) {

if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
            try {
                   
                   TriggerConstant.FORECAST_TRIGGER_FLAG = false;
                   
                   for (Rate__c newRateEntry: Trigger.New) 
                   {
                 
                        List<Budget_2014__c> budgets =[select id,Budget_Category_Prefix__c,Budget_Setting_Status__c,Account_ID__c,Off_Shore_Labor__c,Effort_Projection__c,Spent_Projection__c,EXTERNAL_LABOR_RATE_OFFSHORE__c,EXTERNAL_LABOR_RATE_ONSHORE__c,INTERNAL_LABOR_RATE__c from Budget_2014__c where Budget_Setting_Status__c=TRUE and Project_Lock__c=FALSE and Rate_ID__c =:newRateEntry.ID];
                        
                        if(budgets.size() > 0)
                        {
                            for(Budget_2014__c bdgt : budgets)
                            {
                            
                                //system.debug('### inside get listing bdgt.Budget_Setting_Status__c-->'+bdgt.Budget_Setting_Status__c);
                                //system.debug('### inside get listing bdgt.Effort_Projection__c-->'+bdgt.Effort_Projection__c);
                                //system.debug('### inside get listing bdgt.Account_ID__c-->'+bdgt.Account_ID__c); 
                                //system.debug('### inside get listing bdgt.INTERNAL_LABOR_RATE__c-->'+bdgt.INTERNAL_LABOR_RATE__c);
                                //system.debug('### inside get listing bdgt.EXTERNAL_LABOR_RATE_ONSHORE__c-->'+bdgt.EXTERNAL_LABOR_RATE_ONSHORE__c);
                                //system.debug('### inside get listing bdgt.EXTERNAL_LABOR_RATE_OFFSHORE__c -->'+bdgt.EXTERNAL_LABOR_RATE_OFFSHORE__c );
                                if(bdgt.Effort_Projection__c>0)
                                {
                                    if(bdgt.Budget_Category_Prefix__c=='CICL' || bdgt.Account_ID__c=='OIL')
                                    {
                                        bdgt.Spent_Projection__c      = newRateEntry.INTERNAL_LABOR_RATE__c*bdgt.Effort_Projection__c;
                                    }
                                    else
                                    {
                                        if(bdgt.Off_Shore_Labor__c!=null)
                                        {
                                            bdgt.Spent_Projection__c     =((newRateEntry.EXTERNAL_LABOR_RATE_ONSHORE__c *(1-(bdgt.Off_Shore_Labor__c/100)))+ (newRateEntry.EXTERNAL_LABOR_RATE_OFFSHORE__c *(bdgt.Off_Shore_Labor__c/100)))*bdgt.Effort_Projection__c;
                                        }
                                    }
                                }
                                
                                //system.debug('### inside get listing bdgt.Spent_Projection__c-->'+bdgt.Spent_Projection__c);
                                
                                bdgt.EXTERNAL_LABOR_RATE_OFFSHORE__c    = newRateEntry.EXTERNAL_LABOR_RATE_OFFSHORE__c;
                                bdgt.EXTERNAL_LABOR_RATE_ONSHORE__c     = newRateEntry.EXTERNAL_LABOR_RATE_ONSHORE__c;
                                bdgt.INTERNAL_LABOR_RATE__c             = newRateEntry.INTERNAL_LABOR_RATE__c;
                            
                            }
                           update budgets; 
                        }               
                   }  
            }
            finally
            {
                TriggerConstant.FORECAST_TRIGGER_FLAG = true;
            }
        }

}