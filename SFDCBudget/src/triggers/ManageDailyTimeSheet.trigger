trigger ManageDailyTimeSheet on Timesheet__c(after insert, after update, before delete){
    List<Daily_Timesheets__c> listDailyTimeSheet = new List<Daily_Timesheets__c>();
    if(Trigger.isInsert){
        List<Timesheet__c> listTimesheet = [select Id,Name,Timesheet_Header__c,Task__c,Timesheet_Period__c,Mon__c,Tue__c,Resource_Projects__c,
                                            Wed__c,Thu__c,Fri__c,Sat__c,Sun__c,Timesheet_Calendar__c,Timesheet_Header__r.Timesheet_Calendar__c,
                                            Timesheet_Header__r.Timesheet_Calendar__r.Start_Date__c,Timesheet_Header__r.Timesheet_Calendar__r.End_Date__c
                                            from Timesheet__c where id in: Trigger.newMap.keySet()];
                                            
        for(Timesheet__c timesheet : listTimesheet){
            if(timesheet.Timesheet_Header__c != null && timesheet.Timesheet_Header__r.Timesheet_Calendar__c != null &&
               timesheet.Timesheet_Header__r.Timesheet_Calendar__r.Start_Date__c != null && 
               timesheet.Timesheet_Header__r.Timesheet_Calendar__r.End_Date__c != null){
               
                Date startDate = timesheet.Timesheet_Header__r.Timesheet_Calendar__r.Start_Date__c;
                Date endDate = timesheet.Timesheet_Header__r.Timesheet_Calendar__r.End_Date__c;
                
                
                if(startDate < endDate){
                    Date stDate = startDate;
                    Map<String,Date> mapDate = new Map<String,Date>();
                    while(stDate <= endDate){
                        Datetime dt = DateTime.newInstance(stDate.year(),stDate.month(),stDate.day());
                        String dayOfWeek = dt.format('E');
                        mapDate.put(dayOfWeek,stDate);
                        
                        stDate = stDate.addDays(1);
                    }
                    system.debug('##### mapDate = '+mapDate);
                    if(mapDate.containsKey('Mon')){
                        listDailyTimeSheet.add(new Daily_Timesheets__c(Resource_Projects__c = timesheet.Resource_Projects__c ,
                                            Task__c =  timesheet.Task__c,Timesheet_Detail__c = timesheet.Id,
                                            Timesheet_Date__c = mapDate.get('Mon'),
                                            Timesheet_Header__c = timesheet.Timesheet_Header__c ,
                                            Timesheet_Hours__c = timesheet.Mon__c));
                    }
                    if(mapDate.containsKey('Tue')){
                        listDailyTimeSheet.add(new Daily_Timesheets__c(Resource_Projects__c = timesheet.Resource_Projects__c ,
                                            Task__c =  timesheet.Task__c,Timesheet_Detail__c = timesheet.Id,
                                            Timesheet_Date__c = mapDate.get('Tue'),
                                            Timesheet_Header__c = timesheet.Timesheet_Header__c ,
                                            Timesheet_Hours__c = timesheet.Tue__c));
                    }
                    if(mapDate.containsKey('Wed')){
                        listDailyTimeSheet.add(new Daily_Timesheets__c(Resource_Projects__c = timesheet.Resource_Projects__c ,
                                            Task__c =  timesheet.Task__c,Timesheet_Detail__c = timesheet.Id,
                                            Timesheet_Date__c = mapDate.get('Wed'),
                                            Timesheet_Header__c = timesheet.Timesheet_Header__c ,
                                            Timesheet_Hours__c = timesheet.Wed__c));
                    }
                    if(mapDate.containsKey('Thu')){
                        listDailyTimeSheet.add(new Daily_Timesheets__c(Resource_Projects__c = timesheet.Resource_Projects__c ,
                                            Task__c =  timesheet.Task__c,Timesheet_Detail__c = timesheet.Id,
                                            Timesheet_Date__c = mapDate.get('Thu'),
                                            Timesheet_Header__c = timesheet.Timesheet_Header__c ,
                                            Timesheet_Hours__c = timesheet.Thu__c));
                    }
                    if(mapDate.containsKey('Fri')){
                        listDailyTimeSheet.add(new Daily_Timesheets__c(Resource_Projects__c = timesheet.Resource_Projects__c ,
                                            Task__c =  timesheet.Task__c,Timesheet_Detail__c = timesheet.Id,
                                            Timesheet_Date__c = mapDate.get('Fri'),
                                            Timesheet_Header__c = timesheet.Timesheet_Header__c ,
                                            Timesheet_Hours__c = timesheet.Fri__c));
                    }
                    if(mapDate.containsKey('Sat')){
                        listDailyTimeSheet.add(new Daily_Timesheets__c(Resource_Projects__c = timesheet.Resource_Projects__c ,
                                            Task__c =  timesheet.Task__c,Timesheet_Detail__c = timesheet.Id,
                                            Timesheet_Date__c = mapDate.get('Sat'),
                                            Timesheet_Header__c = timesheet.Timesheet_Header__c ,
                                            Timesheet_Hours__c = timesheet.Sat__c));
                    }
                    if(mapDate.containsKey('Sun')){
                        listDailyTimeSheet.add(new Daily_Timesheets__c(Resource_Projects__c = timesheet.Resource_Projects__c ,
                                            Task__c =  timesheet.Task__c,Timesheet_Detail__c = timesheet.Id,
                                            Timesheet_Date__c = mapDate.get('Sun'),
                                            Timesheet_Header__c = timesheet.Timesheet_Header__c ,
                                            Timesheet_Hours__c = timesheet.Sun__c));
                    }
                }
            }
        }
        if(listDailyTimeSheet.size() > 0){
            insert listDailyTimeSheet;
        }
    }else if(Trigger.isDelete){
        List<Timesheet__c> listTimesheet = [select Id,(select id from Daily_Timesheets__r) from TimeSheet__c where id in: Trigger.oldMap.keySet()];
        for(Timesheet__c timesheet : listTimesheet){
            if(timesheet.Daily_Timesheets__r != null && timesheet.Daily_Timesheets__r.size() > 0){
                listDailyTimeSheet.addAll(timesheet.Daily_Timesheets__r);
            }
        }
        if(listDailyTimeSheet.size() > 0){
            delete listDailyTimeSheet;
        }
    }else if(Trigger.isUpdate){
        List<Timesheet__c> listTimesheet = [select Id,Name,Timesheet_Header__c,Task__c,Timesheet_Period__c,Mon__c,Tue__c,Resource_Projects__c,
                                            Wed__c,Thu__c,Fri__c,Sat__c,Sun__c,Timesheet_Calendar__c,Timesheet_Header__r.Timesheet_Calendar__c,
                                            Timesheet_Header__r.Timesheet_Calendar__r.Start_Date__c,Timesheet_Header__r.Timesheet_Calendar__r.End_Date__c,
                                            (select id,name,Resource_Projects__c,Task__c,Timesheet_Detail__c,Timesheet_Date__c,Timesheet_Header__c,
                                            Timesheet_Hours__c from Daily_Timesheets__r) from TimeSheet__c where id in: Trigger.newMap.keySet()];
        for(Timesheet__c timesheet : listTimesheet){
            Timesheet__c old = Trigger.oldMap.get(timesheet.id);
            if(timesheet.Mon__c != old.Mon__c || timesheet.Tue__c != old.Tue__c || timesheet.Wed__c != old.Wed__c || timesheet.Thu__c != old.Thu__c || timesheet.Fri__c != old.Fri__c ||
                timesheet.Sat__c != old.Sat__c || timesheet.Sun__c != old.Sun__c || timesheet.Task__c != old.Task__c || timesheet.Resource_Projects__c != old.Resource_Projects__c ){
                
                if(timesheet.Daily_Timesheets__r != null && timesheet.Daily_Timesheets__r.size() > 0){
                    for(Daily_Timesheets__c dailyTmsheet : timesheet.Daily_Timesheets__r){
                        Date mydate = dailyTmsheet.Timesheet_Date__c;
                        Datetime dt = DateTime.newInstance(mydate.year(),mydate.month(),mydate.day());
                        String dayOfWeek = dt.format('E');
                        if(dayOfWeek == 'Mon'){
                            dailyTmsheet.Timesheet_Hours__c = timesheet.Mon__c;
                        }else if(dayOfWeek == 'Tue'){
                            dailyTmsheet.Timesheet_Hours__c = timesheet.Tue__c;
                        }else if(dayOfWeek == 'Wed'){
                            dailyTmsheet.Timesheet_Hours__c = timesheet.Wed__c;
                        }else if(dayOfWeek == 'Thu'){
                            dailyTmsheet.Timesheet_Hours__c = timesheet.Thu__c;
                        }else if(dayOfWeek == 'Fri'){
                            dailyTmsheet.Timesheet_Hours__c = timesheet.Fri__c;
                        }else if(dayOfWeek == 'Sat'){
                            dailyTmsheet.Timesheet_Hours__c = timesheet.Sat__c;
                        }else if(dayOfWeek == 'Sun'){
                            dailyTmsheet.Timesheet_Hours__c = timesheet.Sun__c;
                        }
                       
                         dailyTmsheet.Resource_Projects__c = timesheet.Resource_Projects__c;
                         dailyTmsheet.Task__c = timesheet.Task__c;
                        listDailyTimeSheet.add(dailyTmsheet);
                        
                    }
                }
            }
        }
        if(listDailyTimeSheet.size() > 0){
            update listDailyTimeSheet;
        }
    }
}