trigger budget_setting_update on Budget_Setting__c (after insert,after update) 
{

if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
            try {
                   
                   TriggerConstant.FORECAST_TRIGGER_FLAG = false;
                  
                   
                   for (Budget_Setting__c Budget_Setting_Entry: Trigger.New) 
                   {
                        if(Budget_Setting_Entry.Active__c==True)
                        {

                           List<Projects_2014__c> Projects_all=[select id,Lock_Project__c from Projects_2014__c where Budget_Setting__c =: Budget_Setting_Entry.ID];
                        
                            if(Projects_all.size() > 0)
                            {
                                for(Projects_2014__c prj_all: Projects_all)
                                {  
                                    //system.debug('### inside get listing prj.id-->'+prj.id);
                                    prj_all.Lock_Project__c    = Budget_Setting_Entry.Lock_All_Projects__c;
                            
                                }
                                update Projects_all; 
                            }    
                        }
                        else
                        {
                            List<Projects_2014__c> Projects=[select id,Lock_Project__c from Projects_2014__c where Lock_Project__c=FALSE and Budget_Setting__c =: Budget_Setting_Entry.ID];
                        
                            if(Projects.size() > 0)
                            {
                                for(Projects_2014__c prj: Projects)
                                {  
                                    //system.debug('### inside get listing prj.id-->'+prj.id);
                                    prj.Lock_Project__c    = True;
                            
                                }
                                update Projects; 
                            }
                        
                         }           
                   }  
                    

            }
            finally
            {
                TriggerConstant.FORECAST_TRIGGER_FLAG = true;
            }
        }

}