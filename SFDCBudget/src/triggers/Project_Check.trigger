trigger Project_Check on Projects_2014__c (before update) {
 if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
          try {
              
               User current_user=[SELECT Profile_Name__c FROM User WHERE Id= :UserInfo.getUserId()] ;
               List<Projects_2014__c> newTriggers = Trigger.new;
               List<Projects_2014__c> oldTriggers = Trigger.old;
              
               Boolean oldlock_prj= oldTriggers[0].Lock_Project__c;
               Boolean oldSetting_budget= oldTriggers[0].Budget_Setting__r.Active__c;
               //Boolean newlock_prj= newTriggers[0].Lock_Project__c;
               
              for(Projects_2014__c prj:Trigger.New)
              { 
                if(Trigger.IsUpdate )
               {
                      //if(current_user.Profile_Name__c == 'Program Owners' && prj.Project_Type__c=='POR')
                      //{
                          //prj.AddError('Access Denied for Program Owners to create POR Specific Projects.');
                      //}
                      //if(current_user.Profile_Name__c=='Finance Owners' || current_user.Profile_Name__c=='Program Owners' || current_user.Profile_Name__c=='Finance Members' || current_user.Profile_Name__c=='Contractor' || current_user.Profile_Name__c=='Intake Owners' || current_user.Profile_Name__c=='Comcast Employee')
                      if(current_user.Profile_Name__c=='Program Owners' || current_user.Profile_Name__c=='Finance Members' || current_user.Profile_Name__c=='Contractor' || current_user.Profile_Name__c=='Intake Owners' || current_user.Profile_Name__c=='Comcast Employee')
                       {
                            if(oldlock_prj==True && prj.Lock_Project__c==True)
                                 prj.AddError('Access Denied. Contact Finance Owner to unlock the Project');
                       }
                       
                      
                                   
                     //if((current_user.Profile_Name__c=='Program Owners')&&(prj.Created_by_Userid__c!=current_user.id))
                     //{
                         //prj.AddError('Edit Access Denied for the Projects created by other Users');                 
                     //}
              
               }
              
              } 
               
               
            
              }
              finally
              {
                  TriggerConstant.FORECAST_TRIGGER_FLAG = true;
              }
            
         }
}