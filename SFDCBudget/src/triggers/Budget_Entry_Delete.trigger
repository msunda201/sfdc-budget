trigger Budget_Entry_Delete on Budget_Entry_Form__c (before delete) 
{
       if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
          try {
             User current_user=[SELECT Profile_Name__c,id FROM User WHERE Id= :UserInfo.getUserId()] ;
              
            for(Budget_Entry_Form__c bef:Trigger.Old)
             {  
                   if(Trigger.isDelete && ((current_user.Profile_Name__c=='Contractor') || (current_user.Profile_Name__c=='Comcast Employee')))
                   {
                       if(bef.Created_by_Userid__c!=current_user.id)
                       {
                           bef.AddError('Delete Access Denied for the Budget Entry Forms created by other Users.');
                       }
                   }
                   
                  
             
             }
            
              }
             finally
              {
                  TriggerConstant.FORECAST_TRIGGER_FLAG = true;
              }
            
         }
}