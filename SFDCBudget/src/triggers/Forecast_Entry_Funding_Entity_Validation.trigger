trigger Forecast_Entry_Funding_Entity_Validation on Forecast_Entry_Form__c (before insert,before update) 
{
          User current_user=[SELECT Profile_Name__c,id,User_Role_ID__c FROM User WHERE Id= :UserInfo.getUserId()] ;
             
          date dv = system.today();
          String Forecast_Calendar;
          
          Forecast_Calendar__c listFC = null;
          List<Forecast_Calendar__c> resultFC = [select id,Name from Forecast_Calendar__c where (Forecast_Start_Date__c <= :dv and Forecast_End_Date__c >= :dv)];
      
          if(resultFC.size() > 0)
          {
            Forecast_Calendar= resultFC[0].id;
          }
                         
          for(Forecast_Entry_Form__c fef:Trigger.New)
          {        
                   if(((current_user.Profile_Name__c=='Finance Owners') || (current_user.Profile_Name__c=='Finance Members')) && (fef.Funding_Entity__c==null))
                   {                          
                       if(fef.Funding_Entity__c==null)
                       {
                            fef.AddError('Please select Funding Entity.');
                       }
                   }
                   
                   if((current_user.Profile_Name__c=='Intake Owners')||(current_user.Profile_Name__c=='Program Owners')||(current_user.Profile_Name__c=='Comcast Employee')||(current_user.Profile_Name__c=='Contractor')||(current_user.Profile_Name__c=='Contractor PM'))
                   {
                       if(fef.Forecast_Calendar__c!=Forecast_Calendar && fef.Forecast_Type__c == 'Multiple Month by Account')
                       {
                           fef.AddError('Please Contact Finance team to Adjust the Forecast Calendar Month.');
                       }
                   
                   }

          }     
   
}