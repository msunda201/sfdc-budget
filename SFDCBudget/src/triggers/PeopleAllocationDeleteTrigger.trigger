trigger PeopleAllocationDeleteTrigger on PeoplewithProjects__c (after delete) {
List<People_Allocation__c> peoplealocation = new List<People_Allocation__c>();

Integer year = null;

	for (PeoplewithProjects__c oldpplallocation: Trigger.Old) 
   	  { 
   	  	
   	  	system.debug('Inside for Delete Peopleallocation-->' + 'ID--' + oldpplallocation.Id + 'People-->' + oldpplallocation.People__c + 'Resource_Programs_c' + oldpplallocation.Resource_Programs__c + 'Year' + year);
		if (oldpplallocation.Year__c != null){
		year = Integer.valueOf(oldpplallocation.Year__c);
		}
		peoplealocation =  [ select id  from People_Allocation__c where  People_Id__c =:  oldpplallocation.People__c  and Resource_Projects__c =: oldpplallocation.Resource_Programs__c and  Year__c =: year];	
	     	         
	   }
	   if (!peoplealocation.isEmpty()) 
	   {
	   	   delete peoplealocation;
	   }
}