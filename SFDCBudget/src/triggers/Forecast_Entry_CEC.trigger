trigger Forecast_Entry_CEC on Forecast_Entry_Form__c (before update) 
{
             User current_user=[SELECT Profile_Name__c,id,User_Role_ID__c FROM User WHERE Id= :UserInfo.getUserId()];
             
             //This Trigger validation is applicable for Comcast Employees and Contractors
             //The Budget Entry Form cannot be edited by Comcast Employee/Contractors unless the record is created by them or the form is created by the user who belongs to the same Role.

             for(Forecast_Entry_Form__c fef:Trigger.New)
             {                      
                   if(Trigger.isUpdate && ((current_user.Profile_Name__c=='Contractor') || (current_user.Profile_Name__c=='Comcast Employee')))
                   {
                        if(((fef.Created_by_Userid__c!=current_user.id)&&(fef.Created_by_Role_ID__c !=current_user.User_Role_ID__c)&&(fef.Project_Owner__c!=current_user.id))||((current_user.User_Role_ID__c==NULL)||((current_user.User_Role_ID__c!=fef.Created_by_Role_ID__c))))
                           {
                                fef.AddError('Edit Access Denied for the Forecast Entry Forms created by other VP Groups.');
                           }   
                           else
                           {
                           string str;
                           }                      
                   }

             }     

}