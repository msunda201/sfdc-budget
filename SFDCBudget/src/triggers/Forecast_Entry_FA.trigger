trigger Forecast_Entry_FA on Forecast_Entry_Form__c (before insert,before update) 
{
       if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
          try {
             User current_user=[SELECT Profile_Name__c,id FROM User WHERE Id= :UserInfo.getUserId()] ;
                         
             for(Forecast_Entry_Form__c fef:Trigger.New)
             {                      
                   if(fef.Forecast_Type__c=='Finance Adjustment' && (current_user.Profile_Name__c=='Contractor PM' || current_user.Profile_Name__c=='Program Owners' || current_user.Profile_Name__c=='Contractor' || current_user.Profile_Name__c=='Comcast Employee'))
                   {
                            fef.AddError('Access Denied to create Finance Adjustment Forecast Type.');                
                   }
             }     
            
              }
             finally
              {
                  TriggerConstant.FORECAST_TRIGGER_FLAG = true;
              }
            
         }
}