trigger PeopleAllocation on PeoplewithProjects__c (after insert, after update) 
{

Integer i;
Integer k;
Integer CYear;
Date Datestr1;
Date Datestr2;
Date Datestr3;
Date Datestr4;
Date Datestr5;
Date Datestr6;
Date Datestr7;
Date Datestr8;
Date Datestr9;
Date Datestr10;
Date Datestr11;
Date Datestr12;

List<People_Allocation__c> sr = new List<People_Allocation__c>();

if (Trigger.isUpdate) {
for (PeoplewithProjects__c newPeoplewithprojects_old: Trigger.old) 
{   

    //Delete People Allocation specific to a People
    Integer year_old = Integer.valueOf(newPeoplewithprojects_old.Year__c);
    
    
    List<People_Allocation__c> PeopleAllocationLst_old;
    PeopleAllocationLst_old= [select id from People_Allocation__c where Year__c=: year_old and Resource_Projects__c=:newPeoplewithprojects_old.Resource_Programs__c and People_Id__c=: newPeoplewithprojects_old.People__c];
        
   if(PeopleAllocationLst_old.size() > 0)
        delete PeopleAllocationLst_old;     
}
}

for (PeoplewithProjects__c newPeoplewithprojects: Trigger.New) 
{   

   Integer year = Integer.valueOf(newPeoplewithprojects.Year__c);
    
    List<People_Allocation__c> PeopleAllocationLst;
    PeopleAllocationLst= [select id from People_Allocation__c where Year__c=: year and Resource_Projects__c=:newPeoplewithprojects.Resource_Programs__c and People_Id__c=: newPeoplewithprojects.People__c];
        
   if(PeopleAllocationLst.size() > 0)
        delete PeopleAllocationLst;     

    
    
    if(newPeoplewithprojects.Jan__c>0)
    {
         sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,1,1),
                     Allocation__c=newPeoplewithprojects.Jan__c
               )); 
    } else{
        sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,1,1),
                     Allocation__c=0
               )); 
    }
    
    if(newPeoplewithprojects.Feb__c>0)
     {
         sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,2,1),
                     Allocation__c=newPeoplewithprojects.Feb__c
               )); 
    }else{
         sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,2,1),
                     Allocation__c=0
               )); 
    }
    if(newPeoplewithprojects.Mar__c>0)
     {
         sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,3,1),
                     Allocation__c=newPeoplewithprojects.Mar__c
               )); 
    }
    else{
     sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,3,1),
                     Allocation__c=0
               )); 
    }
    if(newPeoplewithprojects.Apr__c>0)
     {
         sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,4,1),
                     Allocation__c=newPeoplewithprojects.Apr__c
               )); 
    }else{
        sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,4,1),
                     Allocation__c=0
               )); 
    }
    if(newPeoplewithprojects.May__c>0)
    {
         sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,5,1),
                     Allocation__c=newPeoplewithprojects.May__c
               )); 
    }else{
        sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,5,1),
                     Allocation__c=0
               )); 
    }
    if(newPeoplewithprojects.Jun__c>0)
    {
         sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,6,1),
                     Allocation__c=newPeoplewithprojects.Jun__c
               )); 
    }else {
        sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,6,1),
                     Allocation__c=0
               )); 
    }
    if(newPeoplewithprojects.Jul__c>0)
    {
         sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,7,1),
                     Allocation__c=newPeoplewithprojects.Jul__c
               )); 
    }else{
        sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,7,1),
                     Allocation__c=0
               )); 
    }
    if(newPeoplewithprojects.Aug__c>0)
    {
         sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,8,1),
                     Allocation__c=newPeoplewithprojects.Aug__c
               )); 
    }else{
    sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,8,1),
                     Allocation__c=0
               )); 
    }
    if(newPeoplewithprojects.Sep__c>0)
    {
         sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,9,1),
                     Allocation__c=newPeoplewithprojects.Sep__c
               )); 
    }else{
    sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,9,1),
                     Allocation__c=0
               ));
    }
    if(newPeoplewithprojects.Oct__c>0)
    {
         sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,10,1),
                     Allocation__c=newPeoplewithprojects.Oct__c
               )); 
    }else{
        sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,10,1),
                     Allocation__c=0
               )); 
    }
    
    if(newPeoplewithprojects.Nov__c>0)
    {
         sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,11,1),
                     Allocation__c=newPeoplewithprojects.Nov__c
               )); 
    }else{
        sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,11,1),
                     Allocation__c=0
               )); 
    }
    
    if(newPeoplewithprojects.Dec__c>0)
    {
         sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,12,1),
                     Allocation__c=newPeoplewithprojects.Dec__c
               )); 
    }else{
    sr.add(new People_Allocation__c(
                     People_Id__c= newPeoplewithprojects.People__c,
                     Resource_Projects__c=newPeoplewithprojects.Resource_Programs__c,
                     month__c=date.newinstance(year,12,1),
                     Allocation__c=0
               )); 
    }
   
               

     

}
  // Insert the People Allocation
        if(sr.size() > 0)
            insert sr; 





}