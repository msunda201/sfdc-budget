trigger Forecast_Entry_Form_before_update on Forecast_Entry_Form__c (before update) 
{    
   
                   date dv = system.today();
                   String Forecast_Calendar;
          
                   Forecast_Calendar__c listFC = null;
                   List<Forecast_Calendar__c> resultFC = [select id,Name from Forecast_Calendar__c where (Forecast_Start_Date__c <= :dv and Forecast_End_Date__c >= :dv)];
      
                   if(resultFC.size() > 0)
                   {
                        Forecast_Calendar= resultFC[0].id;
                   }
                   
                   User current_user=[SELECT Profile_Name__c FROM User WHERE Id= :UserInfo.getUserId()] ;
                   
                   List<Forecast_Entry_Form__c> oldTriggers = Trigger.old;
                   String old_fcalvalue= oldTriggers[0].Forecast_Calendar__c;
                    
                   for(Forecast_Entry_Form__c fef:Trigger.New)
                   {  
                       if((current_user.Profile_Name__c=='Intake Owners')||(current_user.Profile_Name__c=='Program Owners')||(current_user.Profile_Name__c=='Comcast Employee')||(current_user.Profile_Name__c=='Contractor')||(current_user.Profile_Name__c=='Contractor PM'))
                       { 
                           if(old_fcalvalue!=Forecast_Calendar)
                           {
                                fef.AddError('The forecast entry form is locked for the forecasting period you selected.');    
                           } 
                       }              
                   }
            
}