trigger Budget_Entry_Form_CEC1 on Budget_Entry_Form__c (before update) 
{

    if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
          try 
          {

             User current_user=[SELECT Profile_Name__c,id,User_Role_ID__c FROM User WHERE Id= :UserInfo.getUserId()] ;
                 
             for(Budget_Entry_Form__c bef:Trigger.New)
             {        
                   string tmp;
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   if(Trigger.isUpdate && ((current_user.Profile_Name__c=='Contractor')||(current_user.Profile_Name__c=='Comcast Employee')||(current_user.Profile_Name__c=='Program Owners')||(current_user.Profile_Name__c=='Contractor PM')))
                   {
                      
                      //system.debug('%%%%%%%%%%%%%%bef.Project_Owner__c'+bef.Project_Owner__c);
                      //system.debug('%%%%%%%%%%%%%%current_user.id'+current_user.id);

                      //system.debug('%%%%%%%%%%%%%%bef.Created_by_Role_ID__c'+bef.Created_by_Role_ID__c);
                      //system.debug('%%%%%%%%%%%%%%current_user.User_Role_ID__c'+current_user.User_Role_ID__c);

                      
                       if((bef.Project_Owner__c!=current_user.id)&&(bef.Created_by_Role_ID__c !=current_user.User_Role_ID__c))
                       {
                            //bef.AddError('Edit Access Denied for the Forecast Entry Forms created by other VP Groups1.');
                            tmp='False';
                       }
                      
                       
                      
                       if((bef.Created_by_Userid__c!=current_user.id)&&(bef.Project_Owner__c!=current_user.id)&&((current_user.User_Role_ID__c==NULL)||((current_user.User_Role_ID__c!=bef.Created_by_Role_ID__c))))
                       {
                                //bef.AddError('Edit Access Denied for the Forecast  Entry Forms created by other VP Groups2.');
                                 tmp='False';
                       }
                          
                           
                       if(tmp=='False')
                       {
                           bef.AddError('Edit Access Denied for the Budget Entry Forms created by other VP Groups.');
                       }
                      
                       
                   }

             }     
            
            }
            finally
            {
                  TriggerConstant.FORECAST_TRIGGER_FLAG = true;
            } 
     }       
         
}