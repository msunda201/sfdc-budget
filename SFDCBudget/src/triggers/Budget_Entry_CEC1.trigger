trigger Budget_Entry_CEC1 on Budget_Entry_Form__c (before update) 
{
       if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
          try {
             User current_user=[SELECT Profile_Name__c,id,User_Role_ID__c FROM User WHERE Id= :UserInfo.getUserId()] ;
                         
             for(Budget_Entry_Form__c bef:Trigger.New)
             {        
                   if(Trigger.isUpdate && ((current_user.Profile_Name__c=='Program Owners')))
                   {
                       //if((bef.Project_Owner__c!=current_user.id)&&(bef.Created_by_Userid__c!=current_user.id))
                       if((bef.Project_Owner__c!=current_user.id)&&(bef.Created_by_Role_ID__c !=current_user.User_Role_ID__c))
                       {
                            bef.AddError('Edit Access Denied for the Budget Entry Forms created by other VP Groups.');
                       }
                   
                   }

             }     
            
              }
             finally
              {
                  TriggerConstant.FORECAST_TRIGGER_FLAG = true;
              }
            
         }
}