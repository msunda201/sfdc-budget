trigger Budget_Entry_CEC on Budget_Entry_Form__c (before update) 
{
       if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
          try {
             User current_user=[SELECT Profile_Name__c,id,User_Role_ID__c FROM User WHERE Id= :UserInfo.getUserId()];
             
             //This Trigger validation is applicable for Comcast Employees and Contractors
             //The Budget Entry Form cannot be edited by Comcast Employee/Contractors unless the record is created by them or the form is created by the user who belongs to the same Role.

             for(Budget_Entry_Form__c bef:Trigger.New)
             {                      
                   if(Trigger.isUpdate && ((current_user.Profile_Name__c=='Contractor') || (current_user.Profile_Name__c=='Comcast Employee')))
                   //if(Trigger.isUpdate && ((current_user.Profile_Name__c=='Contractor')))
                   {
                       //if(bef.Created_by_Userid__c!=current_user.id)
                       if(bef.Created_by_Role_ID__c !=current_user.User_Role_ID__c)
                       {
                            bef.AddError('Edit Access Denied for the Budget Entry Forms created by other VP Groups.');
                       }
                   
                   }
                  
    
             }     
            
              }
             finally
              {
                  TriggerConstant.FORECAST_TRIGGER_FLAG = true;
              }
            
         }
}