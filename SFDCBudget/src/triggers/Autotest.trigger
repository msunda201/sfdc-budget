trigger Autotest on Forecast__c (after insert,after update) {

 if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
            try {
                     TriggerConstant.FORECAST_TRIGGER_FLAG = false;

Integer MON_CNT;
Integer MON_CNT_ALERT;
Integer DAY_VAL;
Integer OPEX_MONTH;
Integer CAPEX_DAY_VAL;
Decimal NON_RECUR_AMOUNT;
Integer DAY_VAL_DEF;
Integer NUM;
String QTR_STR;
Set<Id> forcastIds = new Set<Id>();

List<Forecast_Expenditure__c> sr = new List<Forecast_Expenditure__c>();
for (Forecast__c newForecast: Trigger.New) 
{

    forcastIds.add(newForecast.Id);
    //newForecast.addError('this is  test error');
    if(newForecast.Category__c == 'Capex - External Offshore' || newForecast.Category__c=='Capex - External Onshore')
    {
       CAPEX_DAY_VAL=30;
    }
    else
    {
        CAPEX_DAY_VAL=0;
    }   
    
if (newForecast.Recurring__c == 'No')
{
     
    date myDate=newForecast.Delivery_Date__c; 
             
    if(newForecast.Net_Terms__c=='Net 15')
    {
        DAY_VAL=15+CAPEX_DAY_VAL;
    }
    else if(newForecast.Net_Terms__c=='Net 30')
    {
        DAY_VAL=30+CAPEX_DAY_VAL;
    }
    else if(newForecast.Net_Terms__c=='Net 45')
    {
        DAY_VAL=45+CAPEX_DAY_VAL;
    }
    else if(newForecast.Net_Terms__c=='Net 60')
    {
        DAY_VAL=60+CAPEX_DAY_VAL;
    }
      else if(newForecast.Net_Terms__c=='Net 90')
    {
        DAY_VAL=90+CAPEX_DAY_VAL;
    }
    else if(newForecast.Net_Terms__c=='Net 120')
    {
        DAY_VAL=120+CAPEX_DAY_VAL;
    }
    else
    {
        DAY_VAL=30+CAPEX_DAY_VAL;
    }
   
    //Capex Labor - add 30 days to start date + net payment terms 
    //if(newForecast.Category__c == 'Opex - External Offshore' || newForecast.Category__c == 'Opex - External Onshore' || newForecast.Category__c == 'Opex - Membership/Sponsorship' ||  newForecast.Category__c == 'Opex - Training' || newForecast.Category__c == 'Opex - Tuition Reimbursement')
    //{
      //   DAY_VAL=0;
         //OPEX_MONTH=12-newForecast.Delivery_Date__c.MONTH()+1;
         //NON_RECUR_AMOUNT=newForecast.Forecast_Amount__c/OPEX_MONTH;
    //}
    //else if(newForecast.Category__c == 'Opex - S/W Maintenance') 
    //{
      //  DAY_VAL=0;
        //OPEX_MONTH=0;
        //NON_RECUR_AMOUNT=newForecast.Forecast_Amount__c;
    //}
    //else
    //{
        OPEX_MONTH=0;
        NON_RECUR_AMOUNT=newForecast.Forecast_Amount__c;
    //}
    
  
    if(OPEX_MONTH==0)
    {
       
        date newDate = mydate.addDays(DAY_VAL); 
        
        
        if(newDate.MONTH() < 4)
        {
            QTR_STR='Q1';
        }
        else if(newDate.MONTH() < 7 && newDate.MONTH() > 3)
        {
            QTR_STR='Q2';
        }
        else if(newDate.MONTH() < 10 && newDate.MONTH() > 6)
        {
            QTR_STR='Q3';
        }
        else
        {
            QTR_STR='Q4';
        }
    
    sr.add(new Forecast_Expenditure__c(
                            Forecast__c = newForecast.ID,
                            Quarter__c=QTR_STR,
                            Quarter_Year__c=QTR_STR+newDate.YEAR(),
                            Date_Spent__c=newDate,
                            Category__c=newForecast.Category__c,
                            Submitted__c=newForecast.Submitted__c,
                            Sharepoint_ID_SOW__c=newForecast.Sharepoint_ID_SOW__c,
                            Amount_Spent__c = newForecast.Forecast_Amount__c));
    }
    /**else
    {
        MON_CNT=30;
        date newDate=newForecast.Delivery_Date__c;
        For(Integer I=0;I<OPEX_MONTH; I++)
        {
         if(I==0)
         {
             newDate = newForecast.Delivery_Date__c;
         }
         else
         {
              newDate = newDate.addDays(MON_CNT);
         }
              

        if(newDate.MONTH() < 4)
        {
            QTR_STR='Q1';
        }
        else if(newDate.MONTH() < 7 && newDate.MONTH() > 3)
        {
            QTR_STR='Q2';
        }
        else if(newDate.MONTH() < 10 && newDate.MONTH() > 6)
        {
            QTR_STR='Q3';
        }
        else
        {
            QTR_STR='Q4';
        }
            
    
             sr.add(new Forecast_Expenditure__c(
                            Forecast__c = newForecast.ID,
                            Quarter__c=QTR_STR,
                            Quarter_Year__c=QTR_STR+newDate.YEAR(),
                            Date_Spent__c=newDate,
                            Category__c=newForecast.Category__c,
                            Submitted__c=newForecast.Submitted__c,
                            Sharepoint_ID_SOW__c=newForecast.Sharepoint_ID_SOW__c,
                            Amount_Spent__c = NON_RECUR_AMOUNT));
       
        }
    }**/

}    

if (newForecast.Recurring__c == 'Yes')
{
 
    date myDate=newForecast.Start_Date__c;
    Integer MON_START;
    
    if (newForecast.Frequency__c == 'Monthly')
    {
        //NUM=(newForecast.Start_Date__c.daysBetween(newForecast.End_Date__c))/30;
        MON_START=newForecast.Start_Date__c.month();
        DAY_VAL_DEF=30;
    }
    else
    {
        //NUM=(newForecast.Start_Date__c.daysBetween(newForecast.End_Date__c))/7;  
        MON_START=1;
        DAY_VAL_DEF=7;
    }

    if(newForecast.Net_Terms__c=='Net 15')
    {
        DAY_VAL=15+CAPEX_DAY_VAL;
    }
    else if(newForecast.Net_Terms__c=='Net 30')
    {
        DAY_VAL=30+CAPEX_DAY_VAL;
    }
    else if(newForecast.Net_Terms__c=='Net 45')
    {
        DAY_VAL=45+CAPEX_DAY_VAL;
    }
    else if(newForecast.Net_Terms__c=='Net 60')
    {
        DAY_VAL=60+CAPEX_DAY_VAL;
    }
      else if(newForecast.Net_Terms__c=='Net 90')
    {
        DAY_VAL=90+CAPEX_DAY_VAL;
    }
    else if(newForecast.Net_Terms__c=='Net 120')
    {
        DAY_VAL=120+CAPEX_DAY_VAL;
    }
    else
    {
        DAY_VAL=DAY_VAL_DEF+CAPEX_DAY_VAL;
    }
    
    if(newForecast.Category__c == 'Opex - External Offshore' || newForecast.Category__c == 'Opex - External Onshore' || newForecast.Category__c == 'Opex - Membership' || newForecast.Category__c == 'Opex - S/W Maintenance' || newForecast.Category__c == 'Opex - Other')
    {
         DAY_VAL=0;
    }
  
    
    For(Integer I=0;I<newForecast.No_of_Occurences__c; I++)
    {

        MON_CNT=DAY_VAL+(DAY_VAL_DEF*I);
              
        date newDate = mydate.addDays(MON_CNT);

        if(newDate.MONTH() < 4)
        {
            QTR_STR='Q1';
        }
        else if(newDate.MONTH() < 7 && newDate.MONTH() > 3)
        {
            QTR_STR='Q2';
        }
        else if(newDate.MONTH() < 10 && newDate.MONTH() > 6)
        {
            QTR_STR='Q3';
        }
        else
        {
            QTR_STR='Q4';
        }


        if(newDate>date.newInstance(2013,12,31))
        {
                //newForecast.addError('No of Occurences is going out of Range for the Budgeted Year');
        }
        
            sr.add(new Forecast_Expenditure__c(
                            Forecast__c = newForecast.ID,
                            Quarter__c=QTR_STR,
                            Quarter_Year__c=QTR_STR+newDate.YEAR(),
                            Date_Spent__c=newDate,
                            Category__c=newForecast.Category__c,
                            Submitted__c=newForecast.Submitted__c,
                            Sharepoint_ID_SOW__c=newForecast.Sharepoint_ID_SOW__c,
                            Amount_Spent__c = newForecast.Forecast_Amount__c));
                        
                            
        }                        
                            
    }                    

}

    List<Forecast_Expenditure__c> forcastExpLst;
    if(forcastIds.size() > 0)
        forcastExpLst = [select id from Forecast_Expenditure__c where Forecast__c IN: forcastIds];

    if(forcastExpLst.size() > 0)
        delete forcastExpLst;
        
    if(sr.size() > 0)
        insert sr;
    
    
     }
            finally
            {
                TriggerConstant.FORECAST_TRIGGER_FLAG = true;
            }
       
        }
 

}