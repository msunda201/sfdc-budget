/**
 *  @ Name               : DurableTeamPeoplefromResources
 *  @ Description        : The Trigger inserts/Updates the Junction object -DT_with_People__c  
 							whenever a person is associated with a project                
 *
 *  @ Author             : Hemalatha Manoharan on Mar 17,2014
 *  @ History            :
 *  @ Last Modified By   :  
 *  @ Last Modified Date : 
 *
 **/
trigger DurableTeamPeoplefromResources on PeoplewithProjects__c (after insert, after delete) {
	//List to hold the people to be added to the durableteam
	List<DT_with_People__c> addDTwithpeople = new List<DT_with_People__c>();
	//List to check duplicatedurable teams
	List <DT_with_People__c> checkduplicateDT = new List<DT_with_People__c>();
	//List to hold the people associated to the project of this durable team
	List<DTwithProjects__c> DTwithprojectlist= new List<DTwithProjects__c>();
	
	//Insert trigger
  	if (Trigger.isInsert) {
		for (PeoplewithProjects__c  newPeoplewithprojects: Trigger.New) 
     	{
     		//get the list of people associated with the projects
        	DTwithprojectlist =[select id,   DURABLE_TEAMS__C from DTwithProjects__c where RESOURCE_PROJECTS__C =: newPeoplewithprojects.RESOURCE_PROGRAMS__C ];
        	system.debug('No of team' + DTwithprojectlist.size() );
       		if (!DTwithprojectlist.isEmpty()) {
        		for (DTwithProjects__c toaddDTwithpeople : DTwithprojectlist)
		        {
		          system.debug('Inside for Insert team -->' + toaddDTwithpeople.DURABLE_TEAMS__C + 'Durable Team Project name-->' + newPeoplewithprojects.RESOURCE_PROGRAMS__C );
		          checkduplicateDT =  [select id  from DT_with_People__c where People__c =: newPeoplewithprojects.PEOPLE__C and Durable_Teams__c =: toaddDTwithpeople.Durable_Teams__c ];	
		          if (checkduplicateDT.isEmpty()) {
		           	 addDTwithpeople.add(new DT_with_People__c (
		        	 People__c = newPeoplewithprojects.PEOPLE__C,
		        	 Durable_Teams__c =toaddDTwithpeople.DURABLE_TEAMS__C
		        	  ));
		         	}
		         
		        }
		         system.debug('End of if-->' );
		         //Insert Durable team with people.
		        
		  	}
     	}
     	 if (!addDTwithpeople.isEmpty()) {
		          insert addDTwithpeople;
		         }
	}
         
	      /*//Update trigger
	        if (Trigger.isUpdate ) {
	        	
		        for (DTwithProjects__c toupdateDTwithpeople : DTwithprojectlist)
		        {
		          system.debug('Inside for People-->' + 'ID--> ' + toupdateDTwithpeople + 'durable team-->' + toupdateDTwithpeople.DURABLE_TEAMS__C + 'Durable Team-->' + newPeoplewithprojects.RESOURCE_PROGRAMS__C );
		              	
		          checkduplicateDT =  [select id  from DT_with_People__c where People__c =: newPeoplewithprojects.PEOPLE__C and Durable_Teams__c =: toupdateDTwithpeople.Durable_Teams__c ];
		            
		             addDTwithpeople.add(new DT_with_People__c (
		        	 People__c = newPeoplewithprojects.PEOPLE__C,
		        	 Durable_Teams__c =toupdateDTwithpeople.DURABLE_TEAMS__C
		        	  ));
		            }
		         }
		          system.debug('End of if-->' );
		         //Insert Durable team with people.
		         if (!checkduplicateDT.isEmpty()) {
		         	delete checkduplicateDT;
		         }
		          
		        // Update Durable team with people
		         insert addDTwithpeople;	            	
		            	
		    }*/
		    
		 
	//Delete trigger
	if (Trigger.isDelete) {
		for (PeoplewithProjects__c  oldPeopleprojects: Trigger.Old) {
		  system.debug('Inside for Delete People-->' + 'ID--' + oldPeopleprojects.People__c + 'Resource programs-->' + oldPeopleprojects.Resource_Programs__c );
	      checkduplicateDT =  [select id, People__c, Durable_Teams__c  from DT_with_People__c where People__c =:  oldPeopleprojects.People__c ];	
	       	 // addDTpeople.add(new DT_with_People__c (
	    	// People__c = toupdatepeoplewithteam.PEOPLE__C ,
	    	// Durable_Teams__c =newDTprojects.Durable_Teams__c
	    	// ));
	    }
	    if (!checkduplicateDT.isEmpty()) {
	       		delete checkduplicateDT;
	    }
	     	
	  }
                
           
   }