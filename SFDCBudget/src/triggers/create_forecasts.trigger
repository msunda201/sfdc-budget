trigger create_forecasts on Forecast_Entry_Form__c (after insert,after update)
{
    if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
            try {
                   TriggerConstant.FORECAST_TRIGGER_FLAG = false;
                   //Variable Declaration
                   
    Integer sdays;
    Integer edays;
    Integer sday;
    Integer eday;
    Integer smon;
    Integer syear;
    Integer emon;
    Decimal mon_projection;
    Decimal avg_mon_projection;
    Decimal smon_projection;
    Decimal emon_projection;
    String QTR_STR; 
    String M_QTR_STR;
    Decimal Effort_Projection_CICL;
    Decimal CAPEX_INTERNAL_CAP_LABOR;
    Decimal Effort_Projection_OIL;
    Decimal OPEX_INTERNAL_LABOR;
    Decimal Effort_Projection_CECL;
    Decimal CAPEX_EXTERNAL_CAP_LABOR;
    Decimal Effort_Projection_OEL;              
    Decimal OPEX_EXTERNAL_LABOR;
    Integer CYear;
    Integer CYear1;
    Integer CMonth;
    Integer DAY_VAL;
    Decimal Hrs;
    Integer k=0;
    Integer m=0;
    date cDate;
    date mDate;
    integer MYear;
    Decimal EXTERNAL_LABOR_RATE_OFFSHORE;
    Decimal EXTERNAL_LABOR_RATE_ONSHORE;
    Decimal INTERNAL_LABOR_RATE;
    String Rate_ID;     
    Decimal Purchase_Cost;        
    String HW_Budget_Category;
    String Category_Type;
    String Funding_Entity_Other;
    String Forecast_Funding_Type;
    string Rate_Flag;
    Boolean Forecast_Flag;
    String Forecast_Funding_Type1;
    
    Set<ID> ForecastIds = new Set<ID>();

    //Declare List Array for Budgets
    List<Forecasts__c> sr = new List<Forecasts__c>(); 
    List<Financial_Report__c> fr = new List<Financial_Report__c>(); 
    
    
    List<Budget_Category__c> lstbud_oswm =[select ID,Capex_Opex__c from Budget_Category__c where Budget_Category_Prefix__c = 'OSWM'];       
    List<Budget_Category__c> lstbud_cicl =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'CICL'];
    List<Budget_Category__c> lstbud_oil =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'OIL'];
    List<Budget_Category__c> lstbud_cecl =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'CECL'];
    List<Budget_Category__c> lstbud_oel =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'OEL'];
    List<Budget_Category__c> lstbud_chwo =[select ID,Capex_Opex__c from Budget_Category__c where Budget_Category_Prefix__c = 'CSWL'];
    RecordType rt = [select id from RecordType where developername = 'Forecast'];
    
    for (Forecast_Entry_Form__c newForecastEntry: Trigger.New) 
    {   
        if(newForecastEntry.Forecast_Flag__c==TRUE)
        {
            Forecast_Flag=TRUE;
        }
        //temporary fix
        //Forecast_Flag=TRUE;
        if(newForecastEntry.Forecast_Job__c==TRUE)
        //if(1==1)
        {
        
        ForecastIds.add(newForecastEntry.ID);
        
        
    
        //Optimize
        //RecordType rt = [select id from RecordType where developername = 'Forecast'];

        if(newForecastEntry.Net_Terms__c=='Net 10')
            {
                DAY_VAL=10;
            }  
           else if(newForecastEntry.Net_Terms__c=='Net 15')
            {
                DAY_VAL=15;
            }
            else if(newForecastEntry.Net_Terms__c=='Net 20')
            {
                DAY_VAL=15;
            }
            else if(newForecastEntry.Net_Terms__c=='Net 30')
            {
                DAY_VAL=30;
            }
            else if(newForecastEntry.Net_Terms__c=='Net 45')
            {
                DAY_VAL=45;
            }
            else if(newForecastEntry.Net_Terms__c=='Net 60')
            {
                DAY_VAL=60;
            }
            else if(newForecastEntry.Net_Terms__c=='Net 90')
            {
                DAY_VAL=90;
            }
            else if(newForecastEntry.Net_Terms__c=='Net 120')
            {
                DAY_VAL=120;
            }
            else
            {
                DAY_VAL=0;
            }
            
            if(newForecastEntry.Forecast_Category_Type__c=='capex') 
            {
                Forecast_Funding_Type=newForecastEntry.Capex_All_Other_Funding__c;
            }
            
            if(newForecastEntry.Forecast_Category_Type__c=='opex')
            {
                Forecast_Funding_Type=newForecastEntry.Opex_All_Other_Funding__c;
            }
            
            if(newForecastEntry.Forecast_Funding_by_Account__c=='Capex Internal Labor')
            {
                Forecast_Funding_Type1=newForecastEntry.CICL_Funding__c;
            }
            else if(newForecastEntry.Forecast_Funding_by_Account__c=='Capex Third Party Labor')
            {
                Forecast_Funding_Type1=newForecastEntry.CECL_Funding__c;
            }
            else if(newForecastEntry.Forecast_Funding_by_Account__c=='Opex External Labor')
            {
                Forecast_Funding_Type1=newForecastEntry.OEL_Funding__c;
            }
            else if(newForecastEntry.Forecast_Funding_by_Account__c=='Opex Internal Labor')
            {
                Forecast_Funding_Type1=newForecastEntry.OIL_Funding__c;
            }
             else if(newForecastEntry.Forecast_Funding_by_Account__c=='Opex All Other')
            {
                Forecast_Funding_Type1=newForecastEntry.Opex_All_Other_Funding__c;
            }
            else
            {
                Forecast_Funding_Type1=newForecastEntry.Capex_All_Other_Funding__c;
            }
            
            if(newForecastEntry.Forecast_Type__c=='Other Costs (Monthly)'  || newForecastEntry.Forecast_Type__c=='Software (Perpetual)' || newForecastEntry.Forecast_Type__c=='Labor (Lump Sum $)' || newForecastEntry.Forecast_Type__c=='Labor (By Effort)')
            {    
                date sDate=newForecastEntry.Start_Date__c; 
                date eDate=newForecastEntry.End_Date__c; 
        
                if(newForecastEntry.Start_Date__c<>null)
                {
                    sday=sDate.DAY();
                    smon=sDate.MONTH();
                    syear=sDate.YEAR();
                    cDate = newForecastEntry.Start_Date__c;
                   
                }
                if(newForecastEntry.End_Date__c<>null)
                {
                    eday=eDate.DAY();
                    emon=eDate.MONTH();
                }
            }
            else
            {
                smon=0;
                syear=0;
            }
            
            //Variable declaration for cDate,CYear for Other Costs (Single Payment) If Budget Category Type is Opex
            
            if(newForecastEntry.Forecast_Type__c=='Other Costs (Single Payment)' && newForecastEntry.Forecast_Category_Type__c=='capex')
            {
                 if(newForecastEntry.Invoice_Date__c<>null)
                 {
                        cDate = newForecastEntry.Invoice_Date__c;  
                 }
                 CYear=cDate.YEAR();
            }
            
            //Variable declaration for cDate,CYear for Other Costs (Single Payment) and Budget Category Type is Opex
            
            if(newForecastEntry.Forecast_Type__c=='Other Costs (Single Payment)' && newForecastEntry.Forecast_Category_Type__c=='opex')
            {
                 if(newForecastEntry.Delivery_Date__c<>null)
                 {
                        cDate = newForecastEntry.Delivery_Date__c;  
                 }
                 CYear=cDate.YEAR();
            }
            //Calculate Hrs =Total Hours/Month Difference
        if(newForecastEntry.Forecast_Type__c=='Labor (By Effort)' && newForecastEntry.Total_Effort_Hours__c>0 && newForecastEntry.Month_Difference__c>0)
        {
            Hrs=newForecastEntry.Total_Effort_Hours__c/newForecastEntry.Month_Difference__c;
        }
        
        //Calculate Month Projection for Other costs (Monthly)
        
        if(newForecastEntry.Forecast_Type__c=='Other Costs (Monthly)' && newForecastEntry.Total_Spent_Projection__c>=0 && newForecastEntry.Month_Difference__c>0)
        {
            if(newForecastEntry.Total_Spent_Projection__c>0)
            {
                mon_projection=newForecastEntry.Total_Spent_Projection__c/newForecastEntry.Month_Difference__c;
            }
            else
            {
                mon_projection=0;
            }
        }
       
        if(newForecastEntry.Forecast_Type__c=='Software (Perpetual)' && newForecastEntry.Month_Difference__c>0)
        {
            //List<Budget_Category__c> lstbud_chwo =[select ID,Capex_Opex__c from Budget_Category__c where Budget_Category_Prefix__c = 'CSWL'];        
        
            if(newForecastEntry.Payment_Date_1__c<>null)
            {
                
                 sr.add(new Forecasts__c(
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= lstbud_chwo[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c='capex',
                            Forecast_Funding_Type__c=newForecastEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newForecastEntry.Funding_Entity__c,
                            Budget_Amount__c = newForecastEntry.Payment_Amount_1__c,
                            Month__c=newForecastEntry.Payment_Date_1__c.MONTH(),
                            Year__c=string.valueof(newForecastEntry.Payment_Date_1__c.YEAR())
                            ));  
                 fr.add(new Financial_Report__c(
                            Type__c='Forecast',
                            Report_Sub_Type__c='Forecast',
                            RecordTypeId=rt.id,
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= lstbud_chwo[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c='capex',
                            Forecast_Funding_Type__c=newForecastEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newForecastEntry.Funding_Entity__c,
                            Budget_Amount__c = newForecastEntry.Payment_Amount_1__c,
                            Month__c=newForecastEntry.Payment_Date_1__c.MONTH(),
                            Year__c=string.valueof(newForecastEntry.Payment_Date_1__c.YEAR())
                            ));  
             }
             
             if(newForecastEntry.Payment_Date_2__c<>null)
            {
                
                 sr.add(new Forecasts__c(
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= lstbud_chwo[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c='capex',
                            Forecast_Funding_Type__c=newForecastEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newForecastEntry.Funding_Entity__c,
                            Budget_Amount__c = newForecastEntry.Payment_Amount_2__c,
                            Month__c=newForecastEntry.Payment_Date_2__c.MONTH(),
                            Year__c=string.valueof(newForecastEntry.Payment_Date_2__c.YEAR())
                            ));  
                 fr.add(new Financial_Report__c(
                            Type__c='Forecast',
                            Report_Sub_Type__c='Forecast',
                            RecordTypeId=rt.id,
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= lstbud_chwo[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c='capex',
                            Forecast_Funding_Type__c=newForecastEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newForecastEntry.Funding_Entity__c,
                            Budget_Amount__c = newForecastEntry.Payment_Amount_2__c,
                            Month__c=newForecastEntry.Payment_Date_2__c.MONTH(),
                            Year__c=string.valueof(newForecastEntry.Payment_Date_2__c.YEAR())
                            ));  
             }
             
              if(newForecastEntry.Payment_Date_3__c<>null)
            {
                
                 sr.add(new Forecasts__c(
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= lstbud_chwo[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c='capex',
                            Forecast_Funding_Type__c=newForecastEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newForecastEntry.Funding_Entity__c,
                            Budget_Amount__c = newForecastEntry.Payment_Amount_3__c,
                            Month__c=newForecastEntry.Payment_Date_3__c.MONTH(),
                            Year__c=string.valueof(newForecastEntry.Payment_Date_3__c.YEAR())
                            ));  
                  
                 fr.add(new Financial_Report__c(
                            Type__c='Forecast',
                            Report_Sub_Type__c='Forecast',
                            RecordTypeId=rt.id,
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= lstbud_chwo[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c='capex',
                            Forecast_Funding_Type__c=newForecastEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newForecastEntry.Funding_Entity__c,
                            Budget_Amount__c = newForecastEntry.Payment_Amount_3__c,
                            Month__c=newForecastEntry.Payment_Date_3__c.MONTH(),
                            Year__c=string.valueof(newForecastEntry.Payment_Date_3__c.YEAR())
                            ));  
             }
             
             if(newForecastEntry.Payment_Date_4__c<>null)
            {
                
                 sr.add(new Forecasts__c(
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= lstbud_chwo[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c='capex',
                            Forecast_Funding_Type__c=newForecastEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newForecastEntry.Funding_Entity__c,
                            Budget_Amount__c = newForecastEntry.Payment_Amount_4__c,
                            Month__c=newForecastEntry.Payment_Date_4__c.MONTH(),
                            Year__c=string.valueof(newForecastEntry.Payment_Date_4__c.YEAR())
                            ));  
                            
                 fr.add(new Financial_Report__c(
                            Type__c='Forecast',
                            Report_Sub_Type__c='Forecast',
                            RecordTypeId=rt.id,
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= lstbud_chwo[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c='capex',
                            Forecast_Funding_Type__c=newForecastEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newForecastEntry.Funding_Entity__c,
                            Budget_Amount__c = newForecastEntry.Payment_Amount_4__c,
                            Month__c=newForecastEntry.Payment_Date_4__c.MONTH(),
                            Year__c=string.valueof(newForecastEntry.Payment_Date_4__c.YEAR())
                            ));  
             }
             
              if(newForecastEntry.Payment_Date_5__c<>null)
            {
                
                 sr.add(new Forecasts__c(
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= lstbud_chwo[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c='capex',
                            Forecast_Funding_Type__c=newForecastEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newForecastEntry.Funding_Entity__c,
                            Budget_Amount__c = newForecastEntry.Payment_Amount_5__c,
                            Month__c=newForecastEntry.Payment_Date_5__c.MONTH(),
                            Year__c=string.valueof(newForecastEntry.Payment_Date_5__c.YEAR())
                            ));  
                            
                  fr.add(new Financial_Report__c(
                            Type__c='Forecast',
                            Report_Sub_Type__c='Forecast',
                            RecordTypeId=rt.id,
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= lstbud_chwo[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c='capex',
                            Forecast_Funding_Type__c=newForecastEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newForecastEntry.Funding_Entity__c,
                            Budget_Amount__c = newForecastEntry.Payment_Amount_5__c,
                            Month__c=newForecastEntry.Payment_Date_5__c.MONTH(),
                            Year__c=string.valueof(newForecastEntry.Payment_Date_5__c.YEAR())
                            ));  
                            
             }
             
         }
         
         
        if(newForecastEntry.Forecast_Type__c=='Other Costs (Single Payment)')
        {
            //cDate = newBudgetEntry.Delivery_Date__c;  
            
            //cDate = newBudgetEntry.Invoice_Date__c;  
            if(newForecastEntry.Apply_Net_Terms__c==1)
            {
                cDate = cDate.addDays(DAY_VAL); 
            }
            CYear=cDate.YEAR();
            k=cDate.MONTH();           
                         
             

                  sr.add(new Forecasts__c(
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= newForecastEntry.Account_Category__c,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c=newForecastEntry.Forecast_Category_Type__c,
                            Forecast_Funding_Type__c=Forecast_Funding_Type,
                            //Funding_Entity__c= newForecastEntry.Funding_Entity_Capex__c,
                            Budget_Amount__c = newForecastEntry.Total_Spent_Projection__c,
                            Month__c=k,
                            Year__c=string.valueof(CYear)
                            ));   
                  
                   fr.add(new Financial_Report__c(
                            Type__c='Forecast',
                            Report_Sub_Type__c='Forecast',
                            RecordTypeId=rt.id,
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= newForecastEntry.Account_Category__c,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c=newForecastEntry.Forecast_Category_Type__c,
                            Forecast_Funding_Type__c=Forecast_Funding_Type,
                            //Funding_Entity__c= newForecastEntry.Funding_Entity_Capex__c,
                            Budget_Amount__c = newForecastEntry.Total_Spent_Projection__c,
                            Month__c=k,
                            Year__c=string.valueof(CYear)
                            ));   
                
        }
            
        Integer j=0; 
        k=smon;  
        
         if(newForecastEntry.Forecast_Type__c=='Labor (Lump Sum $)' || newForecastEntry.Forecast_Type__c=='Labor (By Effort)')
         {
                   if(cDate<>null)
                    {
                        //cDate = cDate.addDays(DAY_VAL); 
                        mDate= cDate.addDays(DAY_VAL+31);
                        CYear =cDate.YEAR();
                        MYear =mDate.YEAR();
                        k=CDate.MONTH();
                        m=mDate.MONTH();
                    }
         }  
    
        
        if(newForecastEntry.Forecast_Type__c=='Other Costs (Monthly)'  || newForecastEntry.Forecast_Type__c=='Software (Perpetual)')
        {
            
                if(newForecastEntry.Apply_Net_Terms__c==1 && newForecastEntry.Forecast_Type__c== 'Other Costs (Monthly)')
                {
                    cDate = cDate.addDays(DAY_VAL); 
                  
                }
                
                if(newForecastEntry.Forecast_Type__c=='Software (Perpetual)')
                {
                    cDate = cDate.addDays(0); 
                }
                
                 CYear =cDate.YEAR();
                 k=CDate.MONTH();
                               
                if(newForecastEntry.Forecast_Type__c=='Other Costs (Monthly)')
                {
                    if(newForecastEntry.Apply_Net_Terms__c==1)
                    {
                        mDate= cDate.addDays(31);
                        MYear =mDate.YEAR();
                        m=mDate.MONTH();
                    }
                    else
                    {
                        MYear =cDate.YEAR();
                        m=cDate.MONTH();
                    }
                }
          
        }     
            
        
        //Declare List array for all different type of Budget Categories
        //////////////////////////
        
        //List<Budget_Category__c> lstbud_oswm =[select ID,Capex_Opex__c from Budget_Category__c where Budget_Category_Prefix__c = 'OSWM'];       
        //List<Budget_Category__c> lstbud_cicl =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'CICL'];
        //List<Budget_Category__c> lstbud_oil =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'OIL'];
        //List<Budget_Category__c> lstbud_cecl =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'CECL'];
        //List<Budget_Category__c> lstbud_oel =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'OEL'];
       
        /////////////////////////// 
        decimal loop_val;
        
        if(newForecastEntry.Forecast_Type__c=='Finance Adjustment' || newForecastEntry.Forecast_Type__c=='Multiple Month by Account')
        {
            loop_val=12;
        }
        else
        {
            loop_val=newForecastEntry.Month_Difference__c;
        }
        
        //////////////////////////
            
        if(newForecastEntry.Forecast_Type__c=='Finance Adjustment'  || newForecastEntry.Forecast_Type__c=='Multiple Month by Account' || newForecastEntry.Forecast_Type__c=='Software (Perpetual)'  || newForecastEntry.Forecast_Type__c=='Other Costs (Monthly)' || newForecastEntry.Forecast_Type__c=='Labor (Lump Sum $)' || newForecastEntry.Forecast_Type__c=='Labor (By Effort)')
        {       
           For(Integer i=1;i<=(loop_val); i++)
            {    

                    if(k>12 && CYear<>null)
                    {
                        k=1;
                        CYear=CYear+1;
                    } 
            
                    if(m>12 && MYear<>null)
                    {
                        m=1;
                        MYear=MYear+1;
                    }
            
            //Create Budgets for Software Perpetual
            
            if(newForecastEntry.Forecast_Type__c=='Finance Adjustment' || newForecastEntry.Forecast_Type__c=='Multiple Month by Account')
            {
                decimal FMA_Value;
                if(i==1)
                {
                    FMA_Value=newForecastEntry.Jan__c;
                }  
               else if(i==2)
                {
                    FMA_Value=newForecastEntry.Feb__c;
                }
                else if(i==3)
                {
                   FMA_Value=newForecastEntry.Mar__c;
                }
                else if(i==4)
                {
                    FMA_Value=newForecastEntry.Apr__c;
                }
                else if(i==5)
                {
                    FMA_Value=newForecastEntry.May__c;
                }
                else if(i==6)
                {
                    FMA_Value=newForecastEntry.Jun__c;
                }
                else if(i==7)
                {
                    FMA_Value=newForecastEntry.Jul__c;
                }
                else if(i==8)
                {
                    FMA_Value=newForecastEntry.Aug__c;
                }
                 else if(i==9)
                {
                    FMA_Value=newForecastEntry.Sep__c;
                }
                else if(i==10)
                {
                    FMA_Value=newForecastEntry.Oct__c;
                }
                else if(i==11)
                {
                    FMA_Value=newForecastEntry.Nov__c;
                }
                else
                {
                    FMA_Value=newForecastEntry.Dec__c;
                }
            
                if(FMA_Value!=NULL)
                {
                sr.add(new Forecasts__c(
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= newForecastEntry.Account_Category__c,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c=newForecastEntry.Forecast_Category_Type__c,
                            Forecast_Funding_Type__c=Forecast_Funding_Type1,
                            //Funding_Entity__c= newForecastEntry.Funding_Entity_Capex__c,
                            //Funding_Entity__c= newForecastEntry.Funding_Entity_Capex__c,
                            Budget_Amount__c = FMA_Value,
                            Month__c=i,
                            Year__c=newForecastEntry.Adjustment_Year__c
                            ));   
                            
                   fr.add(new Financial_Report__c(
                            Type__c='Forecast',
                            Report_Sub_Type__c='Forecast',
                            RecordTypeId=rt.id,
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= newForecastEntry.Account_Category__c,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c=newForecastEntry.Forecast_Category_Type__c,
                            Forecast_Funding_Type__c=Forecast_Funding_Type1,
                            //Funding_Entity__c= newForecastEntry.Funding_Entity_Capex__c,
                            //Funding_Entity__c= newForecastEntry.Funding_Entity_Capex__c,
                            Budget_Amount__c = FMA_Value,
                            Month__c=i,
                            Year__c=newForecastEntry.Adjustment_Year__c
                            ));   
                }
            
            
            }
            
            if(newForecastEntry.Forecast_Type__c=='Software (Perpetual)')
            {
                mon_projection=(newForecastEntry.Total_SW_Maintenance_Amount__c)/newForecastEntry.Month_Difference__c;   
                
                sr.add(new Forecasts__c(
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= lstbud_oswm[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c='Opex',
                            Forecast_Funding_Type__c=newForecastEntry.Opex_All_Other_Funding__c,
                            //Funding_Entity__c= newForecastEntry.Funding_Entity_Opex__c,
                            Budget_Amount__c = mon_projection,
                            Month__c=k,
                            Year__c=string.valueof(CYear)
                            ));  
                  fr.add(new Financial_Report__c(
                            Type__c='Forecast',
                            Report_Sub_Type__c='Forecast',
                            RecordTypeId=rt.id,
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= lstbud_oswm[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c='Opex',
                            Forecast_Funding_Type__c=newForecastEntry.Opex_All_Other_Funding__c,
                            //Funding_Entity__c= newForecastEntry.Funding_Entity_Opex__c,
                            Budget_Amount__c = mon_projection,
                            Month__c=k,
                            Year__c=string.valueof(CYear)
                            ));  
            }

            if(newForecastEntry.Forecast_Type__c=='Other Costs (Monthly)')
            {
                  sr.add(new Forecasts__c(
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= newForecastEntry.Account_Category__c,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c=newForecastEntry.Forecast_Category_Type__c,
                            Forecast_Funding_Type__c=Forecast_Funding_Type,
                            //Funding_Entity__c= newForecastEntry.Funding_Entity_Capex__c,
                            //Funding_Entity__c= newForecastEntry.Funding_Entity_Capex__c,
                            Budget_Amount__c = mon_projection,
                            Month__c=m,
                            Year__c=string.valueof(MYear)
                            ));   
                            
                     fr.add(new Financial_Report__c(
                            Type__c='Forecast',
                            Report_Sub_Type__c='Forecast',
                            RecordTypeId=rt.id,
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c= newForecastEntry.Account_Category__c,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c=newForecastEntry.Forecast_Category_Type__c,
                            Forecast_Funding_Type__c=Forecast_Funding_Type,
                            //Funding_Entity__c= newForecastEntry.Funding_Entity_Capex__c,
                            //Funding_Entity__c= newForecastEntry.Funding_Entity_Capex__c,
                            Budget_Amount__c = mon_projection,
                            Month__c=m,
                            Year__c=string.valueof(MYear)
                            ));   
            
            }
            
            ////////////////////////////////////////////////////////////////////////////////////////////////
            if(newForecastEntry.Forecast_Type__c=='Labor (Lump Sum $)' || newForecastEntry.Forecast_Type__c=='Labor (By Effort)')
            {             
                 //Rate Exception Error. Display Error message when there is no Rate declared for a director group in a specified Year
                 
                    Rate_Flag='True';
                 
                    if(CYear==2013)
                    {
                         EXTERNAL_LABOR_RATE_OFFSHORE   = newForecastEntry.EXTERNAL_LABOR_RATE_OFFSHORE_2013__c;
                         EXTERNAL_LABOR_RATE_ONSHORE    = newForecastEntry.EXTERNAL_LABOR_RATE_ONSHORE_2013__c;
                         INTERNAL_LABOR_RATE            = newForecastEntry.INTERNAL_LABOR_RATE_2013__c;
                    }
                    else if(CYear==2014)
                    {
                        EXTERNAL_LABOR_RATE_OFFSHORE    = newForecastEntry.EXTERNAL_LABOR_RATE_OFFSHORE_2014__c;
                        EXTERNAL_LABOR_RATE_ONSHORE     = newForecastEntry.EXTERNAL_LABOR_RATE_ONSHORE_2014__c;
                        INTERNAL_LABOR_RATE             = newForecastEntry.INTERNAL_LABOR_RATE_2014__c; 
                    }
                    else if(CYear==2015)
                    {
                        EXTERNAL_LABOR_RATE_OFFSHORE    = newForecastEntry.EXTERNAL_LABOR_RATE_OFFSHORE_2015__c;
                        EXTERNAL_LABOR_RATE_ONSHORE     = newForecastEntry.EXTERNAL_LABOR_RATE_ONSHORE_2015__c;
                        INTERNAL_LABOR_RATE             = newForecastEntry.INTERNAL_LABOR_RATE_2015__c; 
                    }
                    else
                    {
                        EXTERNAL_LABOR_RATE_OFFSHORE    = newForecastEntry.EXTERNAL_LABOR_RATE_OFFSHORE_2016__c;
                        EXTERNAL_LABOR_RATE_ONSHORE     = newForecastEntry.EXTERNAL_LABOR_RATE_ONSHORE_2016__c;
                        INTERNAL_LABOR_RATE             = newForecastEntry.INTERNAL_LABOR_RATE_2016__c; 
                    }
                
               
                
                //Refer Excel Macros for the below formula calcualtion       
                if(newForecastEntry.Forecast_Type__c=='Labor (Lump Sum $)')
                {
                     CAPEX_INTERNAL_CAP_LABOR=newForecastEntry.Monthly_Spend_Projection__c*((newForecastEntry.Capital_Expense__c/100)*(1-(newForecastEntry.External_Labor__c/100)));
                     OPEX_INTERNAL_LABOR=newForecastEntry.Monthly_Spend_Projection__c*((1-(newForecastEntry.Capital_Expense__c/100)))*(1-(newForecastEntry.External_Labor__c/100));
                     CAPEX_EXTERNAL_CAP_LABOR=newForecastEntry.Monthly_Spend_Projection__c*(((newForecastEntry.Capital_Expense__c/100))*((newForecastEntry.External_Labor__c/100)));
                     OPEX_EXTERNAL_LABOR=newForecastEntry.Monthly_Spend_Projection__c*((1-(newForecastEntry.Capital_Expense__c/100)))*(newForecastEntry.External_Labor__c/100);
                     //Rate_ID ='';
                }
                else
                {
                    if(smon>12 && syear<>null)
                    {
                        smon=1;
                        syear=syear+1;
                    }             

                 //Calculate Effort Projection
                 // Effort_Projection_CICL=Hrs*(Capex Internal Labor)
                 if(Hrs>0)
                 {
                     Effort_Projection_CICL=Hrs*(newForecastEntry.CAPEX_INTERNAL_CAP_LABOR_ON_SHORE__c/100);
                     Effort_Projection_OIL=Hrs*(newForecastEntry.OPEX_INTERNAL_LABOR_ON_SHORE__c/100);
                     Effort_Projection_CECL=Hrs*(newForecastEntry.CAPEX_EXTERNAL_CAP_LABOR_ON_SHORE__c/100);
                     Effort_Projection_OEL=Hrs*(newForecastEntry.OPEX_EXTERNAL_LABOR_ON_SHORE__c/100);   
                         
                     if(INTERNAL_LABOR_RATE>0)
                     {
                         CAPEX_INTERNAL_CAP_LABOR=(INTERNAL_LABOR_RATE)*Effort_Projection_CICL;
                         OPEX_INTERNAL_LABOR=(INTERNAL_LABOR_RATE)*Effort_Projection_OIL; 
                     }
                        
                         CAPEX_EXTERNAL_CAP_LABOR=((EXTERNAL_LABOR_RATE_ONSHORE *(1-(newForecastEntry.Off_Shore_Labor__c/100)))+ (EXTERNAL_LABOR_RATE_OFFSHORE *(newForecastEntry.Off_Shore_Labor__c/100)))*Effort_Projection_CECL;
                         OPEX_EXTERNAL_LABOR=((EXTERNAL_LABOR_RATE_ONSHORE *(1-(newForecastEntry.Off_Shore_Labor__c/100)))+ (EXTERNAL_LABOR_RATE_OFFSHORE *(newForecastEntry.Off_Shore_Labor__c/100)))*Effort_Projection_OEL;      
                  }
            }
            
            
            //////////////////////////////////////////////////////////////////
            if(newForecastEntry.CAPEX_INTERNAL_CAP_LABOR_ON_SHORE__c>0 && Rate_Flag=='True')
            {
                //List<Budget_Category__c> lstbud_cicl =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'CICL'];
                                             
                sr.add(new Forecasts__c(
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c=lstbud_cicl[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c='Capex',
                            //Total_Monthly_Hrs__c=Hrs,
                            //Effort_Projection__c = Effort_Projection_CICL,
                            //Funding_Entity__c= newBudgetEntry.Funding_Entity_CICL__c,
                            //Month_Factor__c=mon_projection,
                            Forecast_Funding_Type__c=newForecastEntry.CICL_Funding__c,
                            Budget_Amount__c = CAPEX_INTERNAL_CAP_LABOR,
                            //EXTERNAL_LABOR_RATE_OFFSHORE__c= EXTERNAL_LABOR_RATE_OFFSHORE,
                            //EXTERNAL_LABOR_RATE_ONSHORE__c= EXTERNAL_LABOR_RATE_ONSHORE,
                            //INTERNAL_LABOR_RATE__c= INTERNAL_LABOR_RATE,
                            Month__c=k,
                            //Rate_ID__c=Rate_ID,
                            Year__c=string.valueof(CYear)       
                            //Effort_Month__c=smon,
                            //Effort_Year__c=string.valueof(syear),
                            ));
                   
                    fr.add(new Financial_Report__c(
                            Type__c='Forecast',
                            Report_Sub_Type__c='Forecast',
                            RecordTypeId=rt.id,
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c=lstbud_cicl[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            Budget_Category_Type__c='Capex',
                            //Total_Monthly_Hrs__c=Hrs,
                            //Effort_Projection__c = Effort_Projection_CICL,
                            //Funding_Entity__c= newBudgetEntry.Funding_Entity_CICL__c,
                            //Month_Factor__c=mon_projection,
                            Forecast_Funding_Type__c=newForecastEntry.CICL_Funding__c,
                            Budget_Amount__c = CAPEX_INTERNAL_CAP_LABOR,
                            //EXTERNAL_LABOR_RATE_OFFSHORE__c= EXTERNAL_LABOR_RATE_OFFSHORE,
                            //EXTERNAL_LABOR_RATE_ONSHORE__c= EXTERNAL_LABOR_RATE_ONSHORE,
                            //INTERNAL_LABOR_RATE__c= INTERNAL_LABOR_RATE,
                            Month__c=k,
                            //Rate_ID__c=Rate_ID,
                            Year__c=string.valueof(CYear)       
                            //Effort_Month__c=smon,
                            //Effort_Year__c=string.valueof(syear),

                            ));
                            
            }
            
             //Create budgets for Opex Internal Cap Labor On Shore            
            if(newForecastEntry.OPEX_INTERNAL_LABOR_ON_SHORE__c>0  && Rate_Flag=='True')
            {
                //List<Budget_Category__c> lstbud_oil =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'OIL'];
                sr.add(new Forecasts__c(
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c=lstbud_oil[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            //Total_Monthly_Hrs__c=Hrs,
                            //Effort_Projection__c = Effort_Projection_CICL,
                            //Funding_Entity__c= newBudgetEntry.Funding_Entity_CICL__c,
                            //Month_Factor__c=mon_projection,
                            Budget_Category_Type__c='Opex',
                            Forecast_Funding_Type__c=newForecastEntry.OIL_Funding__c,
                            Budget_Amount__c = OPEX_INTERNAL_LABOR,
                            //EXTERNAL_LABOR_RATE_OFFSHORE__c= EXTERNAL_LABOR_RATE_OFFSHORE,
                            //EXTERNAL_LABOR_RATE_ONSHORE__c= EXTERNAL_LABOR_RATE_ONSHORE,
                            //INTERNAL_LABOR_RATE__c= INTERNAL_LABOR_RATE,
                            Month__c=k,
                            //Rate_ID__c=Rate_ID,
                            Year__c=string.valueof(CYear)       
                            //Effort_Month__c=smon,
                            //Effort_Year__c=string.valueof(syear),
                            ));
                            
                  fr.add(new Financial_Report__c(
                            Type__c='Forecast',
                            Report_Sub_Type__c='Forecast',
                            RecordTypeId=rt.id,
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c=lstbud_oil[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            //Total_Monthly_Hrs__c=Hrs,
                            //Effort_Projection__c = Effort_Projection_CICL,
                            //Funding_Entity__c= newBudgetEntry.Funding_Entity_CICL__c,
                            //Month_Factor__c=mon_projection,
                            Budget_Category_Type__c='Opex',
                            Forecast_Funding_Type__c=newForecastEntry.OIL_Funding__c,
                            Budget_Amount__c = OPEX_INTERNAL_LABOR,
                            //EXTERNAL_LABOR_RATE_OFFSHORE__c= EXTERNAL_LABOR_RATE_OFFSHORE,
                            //EXTERNAL_LABOR_RATE_ONSHORE__c= EXTERNAL_LABOR_RATE_ONSHORE,
                            //INTERNAL_LABOR_RATE__c= INTERNAL_LABOR_RATE,
                            Month__c=k,
                            //Rate_ID__c=Rate_ID,
                            Year__c=string.valueof(CYear)       
                            //Effort_Month__c=smon,
                            //Effort_Year__c=string.valueof(syear),
                            ));           
                 
                            
              }


              if(newForecastEntry.CAPEX_EXTERNAL_CAP_LABOR_ON_SHORE__c>0  && Rate_Flag=='True')
              {
                             sr.add(new Forecasts__c(
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c=lstbud_cecl[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            //Total_Monthly_Hrs__c=Hrs,
                            //Effort_Projection__c = Effort_Projection_CICL,
                            //Funding_Entity__c= newBudgetEntry.Funding_Entity_CICL__c,
                            //Month_Factor__c=mon_projection,
                            Budget_Category_Type__c='Capex',
                            Forecast_Funding_Type__c=newForecastEntry.CECL_Funding__c,
                            Budget_Amount__c = CAPEX_EXTERNAL_CAP_LABOR,
                            //EXTERNAL_LABOR_RATE_OFFSHORE__c= EXTERNAL_LABOR_RATE_OFFSHORE,
                            //EXTERNAL_LABOR_RATE_ONSHORE__c= EXTERNAL_LABOR_RATE_ONSHORE,
                            //INTERNAL_LABOR_RATE__c= INTERNAL_LABOR_RATE,
                            Month__c=m,
                            //Rate_ID__c=Rate_ID,
                            Year__c=string.valueof(MYear)       
                            //Effort_Month__c=smon,
                            //Effort_Year__c=string.valueof(syear),
                            ));
                            
                        
                         fr.add(new Financial_Report__c(
                            Type__c='Forecast',
                            Report_Sub_Type__c='Forecast',
                            RecordTypeId=rt.id,
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c=lstbud_cecl[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            //Total_Monthly_Hrs__c=Hrs,
                            //Effort_Projection__c = Effort_Projection_CICL,
                            //Funding_Entity__c= newBudgetEntry.Funding_Entity_CICL__c,
                            //Month_Factor__c=mon_projection,
                            Budget_Category_Type__c='Capex',
                            Forecast_Funding_Type__c=newForecastEntry.CECL_Funding__c,
                            Budget_Amount__c = CAPEX_EXTERNAL_CAP_LABOR,
                            //EXTERNAL_LABOR_RATE_OFFSHORE__c= EXTERNAL_LABOR_RATE_OFFSHORE,
                            //EXTERNAL_LABOR_RATE_ONSHORE__c= EXTERNAL_LABOR_RATE_ONSHORE,
                            //INTERNAL_LABOR_RATE__c= INTERNAL_LABOR_RATE,
                            Month__c=m,
                            //Rate_ID__c=Rate_ID,
                            Year__c=string.valueof(MYear)       
                            //Effort_Month__c=smon,
                            //Effort_Year__c=string.valueof(syear),
                            ));
               }
   
             
              if(newForecastEntry.OPEX_EXTERNAL_LABOR_ON_SHORE__c>0 && Rate_Flag=='True')
              {
                               
                           sr.add(new Forecasts__c(
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c=lstbud_oel[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            //Total_Monthly_Hrs__c=Hrs,
                            //Effort_Projection__c = Effort_Projection_CICL,
                            //Funding_Entity__c= newBudgetEntry.Funding_Entity_CICL__c,
                            //Month_Factor__c=mon_projection,
                            Budget_Category_Type__c='Opex',
                            Forecast_Funding_Type__c=newForecastEntry.OEL_Funding__c,
                            Budget_Amount__c = OPEX_EXTERNAL_LABOR,
                            //EXTERNAL_LABOR_RATE_OFFSHORE__c= EXTERNAL_LABOR_RATE_OFFSHORE,
                            //EXTERNAL_LABOR_RATE_ONSHORE__c= EXTERNAL_LABOR_RATE_ONSHORE,
                            //INTERNAL_LABOR_RATE__c= INTERNAL_LABOR_RATE,
                            Month__c=k,
                            //Rate_ID__c=Rate_ID,
                            Year__c=string.valueof(CYear)       
                            //Effort_Month__c=smon,
                            //Effort_Year__c=string.valueof(syear),
                            ));   
                            
                            
                          fr.add(new Financial_Report__c(
                            Type__c='Forecast',
                            Report_Sub_Type__c='Forecast',
                            RecordTypeId=rt.id,
                            Forecast_Entry_Form__c=newForecastEntry.ID,
                            Project_ID__c=newForecastEntry.Project_Name__c,
                            Forecast_Calendar__c= newForecastEntry.Forecast_Calendar__c,
                            Account_ID__c=lstbud_oel[0].ID,
                            Clarity_ID__c=newForecastEntry.Clarity_ID__c,
                            Effort_Entity__c=newForecastEntry.Effort_Entity__c,
                            //Total_Monthly_Hrs__c=Hrs,
                            //Effort_Projection__c = Effort_Projection_CICL,
                            //Funding_Entity__c= newBudgetEntry.Funding_Entity_CICL__c,
                            //Month_Factor__c=mon_projection,
                            Budget_Category_Type__c='Opex',
                            Forecast_Funding_Type__c=newForecastEntry.OEL_Funding__c,
                            Budget_Amount__c = OPEX_EXTERNAL_LABOR,
                            //EXTERNAL_LABOR_RATE_OFFSHORE__c= EXTERNAL_LABOR_RATE_OFFSHORE,
                            //EXTERNAL_LABOR_RATE_ONSHORE__c= EXTERNAL_LABOR_RATE_ONSHORE,
                            //INTERNAL_LABOR_RATE__c= INTERNAL_LABOR_RATE,
                            Month__c=k,
                            //Rate_ID__c=Rate_ID,
                            Year__c=string.valueof(CYear)       
                            //Effort_Month__c=smon,
                            //Effort_Year__c=string.valueof(syear),
                            ));                
                }

           /////////////////////////////////////////////////////////////////////
           } 
           j=j+1;
           k=k+1;
           m=m+1;
           smon=smon+1; 

           ////////////////////////////////////////////////////////////////////////////////////////////////////
        
            }
        }
    
    
     
     }
 
    }               
    //////////////////////////
     List<Forecasts__c> ForecastLst;
     List<Financial_Report__c> ForecastLst1;
        
        if(ForecastIds.size() > 0)
        {
            ForecastLst= [select id from Forecasts__c where Forecast_Entry_Form__c IN: ForecastIds];
            ForecastLst1= [select id from Financial_Report__c where Forecast_Entry_Form__c IN: ForecastIds];
            
             if(ForecastLst.size() > 0)
             {
                delete ForecastLst;
             }
        
            if(ForecastLst1.size() > 0)
            {
                 delete ForecastLst1;
            }
       
        
            if(sr.size() > 0)
                insert sr; 
            
            if(Forecast_Flag==TRUE)
            {
                if(fr.size() > 0)
                    insert fr;
            }    
                
        }
        
        
    
    /////////////////////////
             }
            finally
            {
                TriggerConstant.FORECAST_TRIGGER_FLAG = true;
            }
        }
             
}