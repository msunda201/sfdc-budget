trigger budget_setting_check on Budget_Setting__c (before insert,before update) {

if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
            try {
                   
                   TriggerConstant.FORECAST_TRIGGER_FLAG = false;
                  
                   
                   for (Budget_Setting__c Budget_Setting_Entry: Trigger.New) 
                   {
                        if(Budget_Setting_Entry.Active__c==True)
                        {
                            List<Budget_Setting__c> Projects_check=[select id from Budget_Setting__c where Active__c =True];
                        
                            if(Projects_check.size() > 0)
                            {
                                Budget_Setting_Entry.AddError('Please deactivate the Other Budget Settings to make current setting as Active.');
                            }

                          
                        }
                                  
                   }  
                    

            }
            finally
            {
                TriggerConstant.FORECAST_TRIGGER_FLAG = true;
            }
        }

}