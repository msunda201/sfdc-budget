/**
 *  @ Name               : DurableTeam_People_Entry
 *  @ Description        : The Trigger inserts/Updates the Junction object - DT_with_People__c 
 							whenever a project is associated with the durable team                
 *
 *  @ Author             : Hemalatha Manoharan on Mar 14,2014
 *  @ History            :
 *  @ Last Modified By   :  
 *  @ Last Modified Date : 
 *
 **/

trigger DurableTeam_People_Entry on DTwithProjects__c (after insert, after delete) {
	
	//List to hold the people to be added to the durableteam
	List<DT_with_People__c> addDTpeople = new List<DT_with_People__c>();
	
	List <DT_with_People__c> checkduplicateDTpeople = new List<DT_with_People__c>();
	//List to hold the people associated to the project of this durable team
	List<PeoplewithProjects__c> peoplewithprojectslist= new List<PeoplewithProjects__c>();
	
	if (Trigger.isInsert) 
	{
		for (DTwithProjects__c  newDTprojects: Trigger.New) 
		{      
			//get the list of people associated with the projects
		     peoplewithprojectslist =[select id,   PEOPLE__C from PeoplewithProjects__c where RESOURCE_PROGRAMS__C =: newDTprojects.Resource_Projects__c ];
		     system.debug('No of people' + peoplewithprojectslist.size() );
		     if (!peoplewithprojectslist.isEmpty()) 
		     {
		     	for (PeoplewithProjects__c toaddpeoplewithteam : peoplewithprojectslist)
				{
					system.debug('Inside for Insert People-->' + toaddpeoplewithteam.PEOPLE__C + 'Durable Team-->' + newDTprojects.Durable_Teams__c );
				    checkduplicateDTpeople =  [select id  from DT_with_People__c where People__c =: toaddpeoplewithteam.PEOPLE__C and Durable_Teams__c =: newDTprojects.Durable_Teams__c ];	
				    if (checkduplicateDTpeople.isEmpty()) {
				           	addDTpeople.add(new DT_with_People__c (
				        	 People__c = toaddpeoplewithteam.PEOPLE__C ,
				        	 Durable_Teams__c =newDTprojects.Durable_Teams__c
				        	 ));
				     }
				 }
				 system.debug('End of Insert if-->' );
				 //Insert Durable team with people.
				 if (!addDTpeople.isEmpty()) {
				           insert addDTpeople;     
				 }
		     }
		      	
	    }
    }
		  	
	
/*if (Trigger.isUpdate) {
		
		for (DTwithProjects__c  updateDTprojects: Trigger.New) 
		     	{
		     	//get the list of people associated with the projects
		        peoplewithprojectslist =[select id,   PEOPLE__C from PeoplewithProjects__c where RESOURCE_PROGRAMS__C =: updateDTprojects.Resource_Projects__c ];
		        system.debug('No of people' + peoplewithprojectslist.size() );
		     	
        	 if (!peoplewithprojectslist.isEmpty()) {
		        for (PeoplewithProjects__c toupdatepeoplewithteam : peoplewithprojectslist)
		        {
		          system.debug('Inside for Update People-->' + 'ID--' + toupdatepeoplewithteam.Id + 'people-->' + toupdatepeoplewithteam.PEOPLE__C  );
		              
		           checkduplicateDTpeople =  [select People__c, Durable_Teams__c  from DT_with_People__c where id =:  updateDTprojects.Id ];	
		           	
		           	  addDTpeople.add(new DT_with_People__c (
		        	 People__c = toupdatepeoplewithteam.PEOPLE__C ,
		        	 Durable_Teams__c =updateDTprojects.Durable_Teams__c
		        	 ));
		         
		        }
		        if ( !addDTpeople.isEmpty() ) {
		        update addDTpeople;
		        }
		         	if (!checkduplicateDTpeople.isEmpty()) {
		         		system.debug('Deleting in Update trigger-->');
		           		delete checkduplicateDTpeople;
		           	}
		         	 system.debug('End of Update if-->' );
		         //Update Durable team with people.
		           insert addDTpeople;     
		      } 
		      
		     }
	} */

   //Delete trigger
   if (Trigger.isDelete) 
   {
   	  for (DTwithProjects__c  oldDTprojects: Trigger.Old) 
   	  { 
   	  	system.debug('Inside for Delete People-->' + 'ID--' + oldDTprojects.Id + 'Durable Team-->' + oldDTprojects.Durable_Teams__c );
		checkduplicateDTpeople =  [select id, People__c, Durable_Teams__c  from DT_with_People__c where  Durable_Teams__c =:  oldDTprojects.Durable_Teams__c ];	
	     // addDTpeople.add(new DT_with_People__c (
		 // People__c = toupdatepeoplewithteam.PEOPLE__C ,
		 // Durable_Teams__c =newDTprojects.Durable_Teams__c
		 // ));
		         
	   }
	   if (!checkduplicateDTpeople.isEmpty()) 
	   {
		   delete checkduplicateDTpeople;
	   }
		         	
   }
}