trigger update_project_program on Program__c (after insert) {

if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
            try {
                   
                   TriggerConstant.FORECAST_TRIGGER_FLAG = false;
                   
                   List<Projects_2014__c>  pr = new List<Projects_2014__c>();
                   
                   for (Program__c newProgram: Trigger.New) 
                   {
                        List<Projects_2014__c> lstprjheader =[select id from Projects_2014__c where id =: newProgram.Project_Source__c];

                         if(lstprjheader.size()>0)
                         {
                             pr.add(new Projects_2014__c(
                                id=lstprjheader[0].id,
                                Program_Name__c= newProgram.id
                             ));   
                            
                             
                         }
                   }  
                   if(pr.size() > 0)
                   {
                                update pr; 
                   }
            }
            finally
            {
                TriggerConstant.FORECAST_TRIGGER_FLAG = true;
            }
        }

}