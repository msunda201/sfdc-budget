trigger update_funding_entity on Projects_2014__c (after update) 
{     
                  List<Budget_Entry_Form__c>  bef = new List<Budget_Entry_Form__c>();
                  string newfundingentity;
                   
                   for (Projects_2014__c newProject: Trigger.New) 
                   {                         
                           if(newProject.Project_Type__c=='POR')
                            {
                                newfundingentity=newProject.Funding_Entity_ID__c;
                            }
                            else
                            {
                                List<Funding_Entity__c> lstFunding =[select id from Funding_Entity__c where Name = :newProject.Funded_by__c];
                                if(lstFunding.size()>0)
                                {
                                  newfundingentity = lstFunding[0].id;             
                                }
                            }
                            
                             List<Budget_Entry_Form__c > budgetforms=[select id from Budget_Entry_Form__c where Project__c=:newProject.ID];
                        
                            if(budgetforms.size() > 0)
                            {
                                for(Budget_Entry_Form__c bdgt : budgetforms)
                                {
                                    bef.add(new Budget_Entry_Form__c(
                                        ID=bdgt.ID,
                                        Disable_Triggers__c=TRUE,
                                        Funding_Entity_ID__c= newfundingentity
                                     ));
                            
                                 }
                             }
                                    
                   }  
                   
                   if(bef.size() > 0)
                   {
                                update bef; 
                   }
   
 }