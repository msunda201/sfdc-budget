trigger create_intake on Intake_Entry_Form__c (after insert,after update)
{
    if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
            try {
                   TriggerConstant.FORECAST_TRIGGER_FLAG = false;
    //Variable Declaration
    Integer sdays;
    Integer edays;
    Integer sday;
    Integer eday;
    Integer smon;
    Integer syear;
    Integer emon;
    Decimal mon_projection;
    Decimal avg_mon_projection;
    Decimal smon_projection;
    Decimal emon_projection;
    String QTR_STR; 
    String M_QTR_STR;
    Decimal Effort_Projection_CICL;
    Decimal CAPEX_INTERNAL_CAP_LABOR;
    Decimal Effort_Projection_OIL;
    Decimal OPEX_INTERNAL_LABOR;
    Decimal Effort_Projection_CECL;
    Decimal CAPEX_EXTERNAL_CAP_LABOR;
    Decimal Effort_Projection_OEL;              
    Decimal OPEX_EXTERNAL_LABOR;
    Integer CYear;
    Integer CYear1;
    Integer CMonth;
    Integer DAY_VAL;
    Decimal Hrs;
    Integer k=0;
    Integer m=0;
    date cDate;
    date mDate;
    integer MYear;
    Decimal EXTERNAL_LABOR_RATE_OFFSHORE;
    Decimal EXTERNAL_LABOR_RATE_ONSHORE;
    Decimal INTERNAL_LABOR_RATE;
    String Rate_ID;     
    Decimal Purchase_Cost;        
    String HW_Budget_Category;
    String Category_Type;
    String Funding_Entity_Other;
    String Intake_Funding_Type;
    string Rate_Flag;
    Boolean Forecast_Flag;
    String Intake_Funding_Type1;
    
    Set<ID> IntakeIds = new Set<ID>();

    List<Intake__c> sr = new List<Intake__c>(); 
    
    List<Budget_Category__c> lstbud_oswm =[select ID,capex_opex__c from Budget_Category__c where Budget_Category_Prefix__c = 'OSWM'];       
    List<Budget_Category__c> lstbud_cicl =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'CICL'];
    List<Budget_Category__c> lstbud_oil =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'OIL'];
    List<Budget_Category__c> lstbud_cecl =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'CECL'];
    List<Budget_Category__c> lstbud_oel =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'OEL'];
    List<Budget_Category__c> lstbud_chwo =[select ID,capex_opex__c from Budget_Category__c where Budget_Category_Prefix__c = 'CSWL'];
    
    Map<Id, Intake_Entry_Form__c> external_internal_labor_Map 
                    = new Map<Id, Intake_Entry_Form__c>([select id, Director_Group__r.EXTERNAL_LABOR_RATE_OFFSHORE_2013__c, Director_Group__r.EXTERNAL_LABOR_RATE_OFFSHORE_2014__c,Director_Group__r.EXTERNAL_LABOR_RATE_OFFSHORE_2015__c,Director_Group__r.EXTERNAL_LABOR_RATE_OFFSHORE_2016__c, Director_Group__r.EXTERNAL_LABOR_RATE_ONSHORE_2013__c, Director_Group__r.EXTERNAL_LABOR_RATE_ONSHORE_2014__c,Director_Group__r.EXTERNAL_LABOR_RATE_ONSHORE_2015__c, Director_Group__r.EXTERNAL_LABOR_RATE_ONSHORE_2016__c, Director_Group__r.INTERNAL_LABOR_RATE_2013__c, Director_Group__r.INTERNAL_LABOR_RATE_2014__c, Director_Group__r.INTERNAL_LABOR_RATE_2015__c, Director_Group__r.INTERNAL_LABOR_RATE_2016__c 
                                                    FROM Intake_Entry_Form__c 
                                                WHERE Id IN :Trigger.New]);
   
     string intake_id;
     string category_val;
    for (Intake_Entry_Form__c newIntakeEntry:  Trigger.New) 
    {   

        intake_id=newIntakeEntry.ID;
        category_val=newIntakeEntry.Category_Type__c;
        Intake_Entry_Form__c queryExternal_internal_map = external_internal_labor_Map.get(intake_id);
        
           if(newIntakeEntry.Net_Terms__c=='Net 10')
            {
                DAY_VAL=10;
            }  
           else if(newIntakeEntry.Net_Terms__c=='Net 15')
            {
                DAY_VAL=15;
            }
            else if(newIntakeEntry.Net_Terms__c=='Net 20')
            {
                DAY_VAL=15;
            }
            else if(newIntakeEntry.Net_Terms__c=='Net 30')
            {
                DAY_VAL=30;
            }
            else if(newIntakeEntry.Net_Terms__c=='Net 45')
            {
                DAY_VAL=45;
            }
            else if(newIntakeEntry.Net_Terms__c=='Net 60')
            {
                DAY_VAL=60;
            }
            else if(newIntakeEntry.Net_Terms__c=='Net 90')
            {
                DAY_VAL=90;
            }
            else if(newIntakeEntry.Net_Terms__c=='Net 120')
            {
                DAY_VAL=120;
            }
            else
            {
                DAY_VAL=0;
            }
          
          
            
            if(newIntakeEntry.Intake_Type__c=='Other Costs (Monthly)'  || newIntakeEntry.Intake_Type__c=='Software (Perpetual)' || newIntakeEntry.Intake_Type__c=='Labor (Lump Sum $)' || newIntakeEntry.Intake_Type__c=='Labor (By Effort)')
            {    
                date sDate=newIntakeEntry.Start_Date__c; 
                date eDate=newIntakeEntry.End_Date__c; 
        
                if(newIntakeEntry.Start_Date__c<>null)
                {
                    sday=sDate.DAY();
                    smon=sDate.MONTH();
                    syear=sDate.YEAR();
                    cDate = newIntakeEntry.Start_Date__c;
                   
                }
                if(newIntakeEntry.End_Date__c<>null)
                {
                    eday=eDate.DAY();
                    emon=eDate.MONTH();
                }
            }
            else
            {
                smon=0;
                syear=0;
            }
            
            //Variable declaration for cDate,CYear for Other Costs (Single Payment) If Budget Category Type is Opex
            
            
           //System.debug('**** before Single paymnet if category type->' + newIntakeEntry.Category_Type__c);
            if((newIntakeEntry.Intake_Type__c=='Other Costs (Single Payment)') && (newIntakeEntry.Category_Type__c=='capex'))
            {
                 System.debug('**** before invoice date if ->' + newIntakeEntry.Invoice_Date__c);
                 if(newIntakeEntry.Invoice_Date__c<>null)
                 {
                        cDate = newIntakeEntry.Invoice_Date__c;  
                        //System.debug('inside invoice date if ->' + cDate );
                 }
                 System.debug('**setting cdate' + cDate ) ;
                 CYear=cDate.YEAR();
            }
            
            //Variable declaration for cDate,CYear for Other Costs (Single Payment) and Budget Category Type is Opex
            if((newIntakeEntry.Intake_Type__c=='Other Costs (Single Payment)') && (newIntakeEntry.Category_Type__c=='opex'))
            {
                 if(newIntakeEntry.Delivery_Date__c<>null)
                 {
                        cDate = newIntakeEntry.Delivery_Date__c;  
                 }
                 System.debug('**setting cdate in opex' + cDate ) ;
                 CYear=cDate.YEAR();
            }
        
        //Calculate Hrs =Total Hours/Month Difference
        if(newIntakeEntry.Intake_Type__c=='Labor (By Effort)' && newIntakeEntry.Total_Effort_Hours__c>0 && newIntakeEntry.Month_Difference__c>0)
        {
            Hrs=newIntakeEntry.Total_Effort_Hours__c/newIntakeEntry.Month_Difference__c;
        }
        
        //Calculate Month Projection for Other costs (Monthly)
        if(newIntakeEntry.Intake_Type__c=='Other Costs (Monthly)' && newIntakeEntry.Total_Spent_Projection__c>=0 && newIntakeEntry.Month_Difference__c>0)
        {
            if(newIntakeEntry.Total_Spent_Projection__c>0)
            {
                mon_projection=newIntakeEntry.Total_Spent_Projection__c/newIntakeEntry.Month_Difference__c;
            }
            else
            {
                mon_projection=0;
            }
        }
       
        if(newIntakeEntry.Intake_Type__c=='Software (Perpetual)' && newIntakeEntry.Month_Difference__c>0)
        {
            //List<Budget_Category__c> lstbud_chwo =[select ID,Capex_Opex__c from Budget_Category__c where Budget_Category_Prefix__c = 'CSWL'];        
        
            if(newIntakeEntry.Payment_Date_1__c<>null)
            {   
                 sr.add(new Intake__c(
                            Intake_Entry_Form__c=newIntakeEntry.ID,
                            Account_ID__c= lstbud_chwo[0].ID,
                            Capex_Opex__c='capex',
                            Intake_Amount__c = newIntakeEntry.Payment_Amount_1__c,
                            Month__c=newIntakeEntry.Payment_Date_1__c.MONTH(),
                            Intake_Year__c=string.valueof(newIntakeEntry.Payment_Date_1__c.YEAR())
                            ));     
            }
            
            if(newIntakeEntry.Payment_Date_2__c<>null)
            {   
                 sr.add(new Intake__c(
                            Intake_Entry_Form__c=newIntakeEntry.ID,
                            Account_ID__c= lstbud_chwo[0].ID,
                            Capex_Opex__c='capex',
                            Intake_Amount__c = newIntakeEntry.Payment_Amount_2__c,
                            Month__c=newIntakeEntry.Payment_Date_2__c.MONTH(),
                            Intake_Year__c=string.valueof(newIntakeEntry.Payment_Date_2__c.YEAR())
                            ));     
            }
            
            if(newIntakeEntry.Payment_Date_3__c<>null)
            {   
                 sr.add(new Intake__c(
                            Intake_Entry_Form__c=newIntakeEntry.ID,
                            Account_ID__c= lstbud_chwo[0].ID,
                            Capex_Opex__c='capex',
                            Intake_Amount__c = newIntakeEntry.Payment_Amount_3__c,
                            Month__c=newIntakeEntry.Payment_Date_3__c.MONTH(),
                            Intake_Year__c=string.valueof(newIntakeEntry.Payment_Date_3__c.YEAR())
                            ));     
             }
            
            if(newIntakeEntry.Payment_Date_4__c<>null)
            {   
                 sr.add(new Intake__c(
                            Intake_Entry_Form__c=newIntakeEntry.ID,
                            Account_ID__c= lstbud_chwo[0].ID,
                            Capex_Opex__c='capex',
                            Intake_Amount__c = newIntakeEntry.Payment_Amount_4__c,
                            Month__c=newIntakeEntry.Payment_Date_4__c.MONTH(),
                            Intake_Year__c=string.valueof(newIntakeEntry.Payment_Date_4__c.YEAR())
                            ));     
            }
            
            if(newIntakeEntry.Payment_Date_5__c<>null)
            {   
                 sr.add(new Intake__c(
                            Intake_Entry_Form__c=newIntakeEntry.ID,
                            Account_ID__c= lstbud_chwo[0].ID,
                            Capex_Opex__c='capex',
                            Intake_Amount__c = newIntakeEntry.Payment_Amount_5__c,
                            Month__c=newIntakeEntry.Payment_Date_5__c.MONTH(),
                            Intake_Year__c=string.valueof(newIntakeEntry.Payment_Date_5__c.YEAR())
                            ));     
             }

        }
        
        
        if(newIntakeEntry.Intake_Type__c=='Other Costs (Single Payment)')
        {
            System.debug('*******cDate ->' + cDate +'DAY_VAL->' + DAY_VAL + ' Apply_Net_Terms__c'+ newIntakeEntry.Apply_Net_Terms__c );
            if(newIntakeEntry.Apply_Net_Terms__c==1)
            {
                cDate = cDate.addDays(DAY_VAL); 
            }
            CYear=cDate.YEAR();
            k=cDate.MONTH();           

            sr.add(new Intake__c(
                            Intake_Entry_Form__c=newIntakeEntry.ID,
                            Intake_Amount__c = newIntakeEntry.Total_Spent_Projection__c,
                            Capex_Opex__c=newIntakeEntry.Category_Type__c,
                            Account_ID__c= newIntakeEntry.Account_Category__c,
                            Month__c=k,
                            Intake_Year__c=string.valueof(CYear)
            ));   
                                    
        }
        
        Integer j=0; 
        k=smon;  
        
         if(newIntakeEntry.Intake_Type__c=='Labor (Lump Sum $)' || newIntakeEntry.Intake_Type__c=='Labor (By Effort)')
         {
                   if(cDate<>null)
                    {
                        //cDate = cDate.addDays(DAY_VAL); 
                        mDate= cDate.addDays(DAY_VAL+31);
                        CYear =cDate.YEAR();
                        MYear =mDate.YEAR();
                        k=CDate.MONTH();
                        m=mDate.MONTH();
                    }
         }  
    
        
        if(newIntakeEntry.Intake_Type__c=='Other Costs (Monthly)'  || newIntakeEntry.Intake_Type__c=='Software (Perpetual)')
        {
            
                if(newIntakeEntry.Apply_Net_Terms__c==1 && newIntakeEntry.Intake_Type__c== 'Other Costs (Monthly)')
                {
                    cDate = cDate.addDays(DAY_VAL); 
                  
                }
                
                if(newIntakeEntry.Intake_Type__c=='Software (Perpetual)')
                {
                    cDate = cDate.addDays(0); 
                }
                
                 CYear =cDate.YEAR();
                 k=CDate.MONTH();
                               
                if(newIntakeEntry.Intake_Type__c=='Other Costs (Monthly)')
                {
                    if(newIntakeEntry.Apply_Net_Terms__c==1)
                    {
                        mDate= cDate.addDays(31);
                        MYear =mDate.YEAR();
                        m=mDate.MONTH();
                    }
                    else
                    {
                        MYear =cDate.YEAR();
                        m=cDate.MONTH();
                    }
                }
          
        }     
            
        
       /////////////////////////// 
        decimal loop_val;
        
        if( newIntakeEntry.Intake_Type__c=='Multiple Month by Account')
        {
            loop_val=12;
        }
        else
        {
            loop_val=newIntakeEntry.Month_Difference__c;
        }
        
        //////////////////////////
            
        if( newIntakeEntry.Intake_Type__c=='Multiple Month by Account' || newIntakeEntry.Intake_Type__c=='Software (Perpetual)'  || newIntakeEntry.Intake_Type__c=='Other Costs (Monthly)' || newIntakeEntry.Intake_Type__c=='Labor (Lump Sum $)' || newIntakeEntry.Intake_Type__c=='Labor (By Effort)')
        {       
           For(Integer i=1;i<=(loop_val); i++)
            {    

                    if(k>12 && CYear<>null)
                    {
                        k=1;
                        CYear=CYear+1;
                    } 
            
                    if(m>12 && MYear<>null)
                    {
                        m=1;
                        MYear=MYear+1;
                    }
            
            //Create Budgets for Software Perpetual
            
            if( newIntakeEntry.Intake_Type__c=='Multiple Month by Account')
            {
                decimal FMA_Value;
                if(i==1)
                {
                    FMA_Value=newIntakeEntry.Jan__c;
                }  
               else if(i==2)
                {
                    FMA_Value=newIntakeEntry.Feb__c;
                }
                else if(i==3)
                {
                   FMA_Value=newIntakeEntry.Mar__c;
                }
                else if(i==4)
                {
                    FMA_Value=newIntakeEntry.Apr__c;
                }
                else if(i==5)
                {
                    FMA_Value=newIntakeEntry.May__c;
                }
                else if(i==6)
                {
                    FMA_Value=newIntakeEntry.Jun__c;
                }
                else if(i==7)
                {
                    FMA_Value=newIntakeEntry.Jul__c;
                }
                else if(i==8)
                {
                    FMA_Value=newIntakeEntry.Aug__c;
                }
                 else if(i==9)
                {
                    FMA_Value=newIntakeEntry.Sep__c;
                }
                else if(i==10)
                {
                    FMA_Value=newIntakeEntry.Oct__c;
                }
                else if(i==11)
                {
                    FMA_Value=newIntakeEntry.Nov__c;
                }
                else
                {
                    FMA_Value=newIntakeEntry.Dec__c;
                }
            
                if(FMA_Value!=NULL)
                {
                  sr.add(new Intake__c(
                            Intake_Entry_Form__c=newIntakeEntry.ID,
                            Intake_Amount__c = FMA_Value,
                            Account_ID__c= newIntakeEntry.Account_Category__c,
                            Capex_Opex__c=category_val,
                            Month__c=i,
                            Intake_Year__c=newIntakeEntry.Adjustment_Year__c
                    ));              
                   
                }
            
              }
              ///////////
            if(newIntakeEntry.Intake_Type__c=='Software (Perpetual)')
            {
                mon_projection=(newIntakeEntry.Total_SW_Maintenance_Amount__c)/newIntakeEntry.Month_Difference__c; 
                
                sr.add(new Intake__c(
                            Intake_Entry_Form__c=newIntakeEntry.ID,
                            Account_ID__c= lstbud_oswm[0].ID,
                            Capex_Opex__c='opex',
                            Intake_Amount__c = mon_projection,
                            Month__c=k,
                            Intake_Year__c=string.valueof(CYear)
                    ));       
            }

            if(newIntakeEntry.Intake_Type__c=='Other Costs (Monthly)')
            {
            
                   sr.add(new Intake__c(
                            Intake_Entry_Form__c=newIntakeEntry.ID,
                            Account_ID__c= newIntakeEntry.Account_Category__c,
                            Capex_Opex__c=category_val,
                            Intake_Amount__c = mon_projection,
                            Month__c=m,
                            Intake_Year__c=string.valueof(CYear)
                    ));     

            }
            
             ////////////////////////////////////////////////////////////////////////////////////////////////
            if(newIntakeEntry.Intake_Type__c=='Labor (Lump Sum $)' || newIntakeEntry.Intake_Type__c=='Labor (By Effort)')
            {             
                 //Rate Exception Error. Display Error message when there is no Rate declared for a director group in a specified Year
                 
                    Rate_Flag='True';
                    
                 system.debug('###MAP EXTERNAL_LABOR_RATE_ONSHORE 2014 -->' + queryExternal_internal_map.Director_Group__r.EXTERNAL_LABOR_RATE_OFFSHORE_2014__c+  '2013' + queryExternal_internal_map.Director_Group__r.EXTERNAL_LABOR_RATE_OFFSHORE_2013__c);
                 
                    if(CYear==2013)
                    { 
                         EXTERNAL_LABOR_RATE_OFFSHORE   = queryExternal_internal_map.Director_Group__r.EXTERNAL_LABOR_RATE_OFFSHORE_2013__c;
                         EXTERNAL_LABOR_RATE_ONSHORE    = queryExternal_internal_map.Director_Group__r.EXTERNAL_LABOR_RATE_ONSHORE_2013__c;
                         INTERNAL_LABOR_RATE            = queryExternal_internal_map.Director_Group__r.INTERNAL_LABOR_RATE_2013__c;
                    }
                    else if(CYear==2014)
                    {
                        EXTERNAL_LABOR_RATE_OFFSHORE    = queryExternal_internal_map.Director_Group__r.EXTERNAL_LABOR_RATE_OFFSHORE_2014__c;
                        EXTERNAL_LABOR_RATE_ONSHORE     = queryExternal_internal_map.Director_Group__r.EXTERNAL_LABOR_RATE_ONSHORE_2014__c;
                        INTERNAL_LABOR_RATE             = queryExternal_internal_map.Director_Group__r.INTERNAL_LABOR_RATE_2014__c;
                    }
                    else if(CYear==2015)
                    {
                        EXTERNAL_LABOR_RATE_OFFSHORE    = queryExternal_internal_map.Director_Group__r.EXTERNAL_LABOR_RATE_OFFSHORE_2015__c;
                        EXTERNAL_LABOR_RATE_ONSHORE     = queryExternal_internal_map.Director_Group__r.EXTERNAL_LABOR_RATE_ONSHORE_2015__c;
                        INTERNAL_LABOR_RATE             = queryExternal_internal_map.Director_Group__r.INTERNAL_LABOR_RATE_2015__c;
                    }
                    else
                    {
                        EXTERNAL_LABOR_RATE_OFFSHORE    = queryExternal_internal_map.Director_Group__r.EXTERNAL_LABOR_RATE_OFFSHORE_2016__c;
                        EXTERNAL_LABOR_RATE_ONSHORE     = queryExternal_internal_map.Director_Group__r.EXTERNAL_LABOR_RATE_ONSHORE_2016__c;
                        INTERNAL_LABOR_RATE             = queryExternal_internal_map.Director_Group__r.INTERNAL_LABOR_RATE_2016__c;
                    }
               
              
                system.debug('###CYear-->'+CYear);
                
                //Refer Excel Macros for the below formula calcualtion       
                if(newIntakeEntry.Intake_Type__c=='Labor (Lump Sum $)')
                {
                     CAPEX_INTERNAL_CAP_LABOR=newIntakeEntry.Monthly_Spend_Projection__c*((newIntakeEntry.Capital_Expense__c/100)*(1-(newIntakeEntry.External_Labor__c/100)));
                     OPEX_INTERNAL_LABOR=newIntakeEntry.Monthly_Spend_Projection__c*((1-(newIntakeEntry.Capital_Expense__c/100)))*(1-(newIntakeEntry.External_Labor__c/100));
                     CAPEX_EXTERNAL_CAP_LABOR=newIntakeEntry.Monthly_Spend_Projection__c*(((newIntakeEntry.Capital_Expense__c/100))*((newIntakeEntry.External_Labor__c/100)));
                     OPEX_EXTERNAL_LABOR=newIntakeEntry.Monthly_Spend_Projection__c*((1-(newIntakeEntry.Capital_Expense__c/100)))*(newIntakeEntry.External_Labor__c/100);
                     //Rate_ID ='';
                }
                else
                {
                    if(smon>12 && syear<>null)
                    {
                        smon=1;
                        syear=syear+1;
                    }             

                 //Calculate Effort Projection
                 // Effort_Projection_CICL=Hrs*(Capex Internal Labor)
   
                 if(Hrs>0)
                 {
                     Effort_Projection_CICL=Hrs*(newIntakeEntry.CAPEX_INTERNAL_CAP_LABOR_ON_SHORE__c/100);
                     Effort_Projection_OIL=Hrs*(newIntakeEntry.OPEX_INTERNAL_LABOR_ON_SHORE__c/100);
                     Effort_Projection_CECL=Hrs*(newIntakeEntry.CAPEX_EXTERNAL_CAP_LABOR_ON_SHORE__c/100);
                     Effort_Projection_OEL=Hrs*(newIntakeEntry.OPEX_EXTERNAL_LABOR_ON_SHORE__c/100);   
                         
                        if(INTERNAL_LABOR_RATE>0)
                        {
                            CAPEX_INTERNAL_CAP_LABOR=(INTERNAL_LABOR_RATE)*Effort_Projection_CICL;
                            OPEX_INTERNAL_LABOR=(INTERNAL_LABOR_RATE)*Effort_Projection_OIL; 
                        }

                       system.debug('###Effort_Projection_CECL-->'+Effort_Projection_CECL);
                       system.debug('###Effort_Projection_OEL-->'+Effort_Projection_OEL);
                      
                       system.debug('###EXTERNAL_LABOR_RATE_ONSHORE-->'+EXTERNAL_LABOR_RATE_ONSHORE);
                     
                      CAPEX_EXTERNAL_CAP_LABOR=((EXTERNAL_LABOR_RATE_ONSHORE *(1-(newIntakeEntry.Off_Shore_Labor__c/100)))+ (EXTERNAL_LABOR_RATE_OFFSHORE *(newIntakeEntry.Off_Shore_Labor__c/100)))*Effort_Projection_CECL;  
                      OPEX_EXTERNAL_LABOR=((EXTERNAL_LABOR_RATE_ONSHORE *(1-(newIntakeEntry.Off_Shore_Labor__c/100)))+ (EXTERNAL_LABOR_RATE_OFFSHORE *(newIntakeEntry.Off_Shore_Labor__c/100)))*Effort_Projection_OEL;      
                      
                  }
            }
            
            
            //////////////////////////////////////////////////////////////////
            if(newIntakeEntry.CAPEX_INTERNAL_CAP_LABOR_ON_SHORE__c>0 && Rate_Flag=='True')
            {                 
                 sr.add(new Intake__c(
                            Intake_Entry_Form__c=newIntakeEntry.ID,
                            Account_ID__c=lstbud_cicl[0].ID,
                            Intake_Amount__c = CAPEX_INTERNAL_CAP_LABOR,
                            Capex_Opex__c='capex',
                            Month__c=k,
                            Intake_Year__c=string.valueof(CYear)
                    ));                                                    
            }
            
             //Create budgets for Opex Internal Cap Labor On Shore            
            if(newIntakeEntry.OPEX_INTERNAL_LABOR_ON_SHORE__c>0  && Rate_Flag=='True')
            {
                //List<Budget_Category__c> lstbud_oil =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'OIL'];
          sr.add(new Intake__c(
                            Intake_Entry_Form__c=newIntakeEntry.ID,
                            Account_ID__c=lstbud_oil[0].ID,
                            Intake_Amount__c = OPEX_INTERNAL_LABOR,
                            Capex_Opex__c='opex',
                            Month__c=k,
                            Intake_Year__c=string.valueof(CYear)
                    ));                   
              }


              if(newIntakeEntry.CAPEX_EXTERNAL_CAP_LABOR_ON_SHORE__c>0  && Rate_Flag=='True')
              {
                   sr.add(new Intake__c(
                            Intake_Entry_Form__c=newIntakeEntry.ID,
                            Account_ID__c=lstbud_cecl[0].ID,
                            Intake_Amount__c = CAPEX_EXTERNAL_CAP_LABOR,
                            Capex_Opex__c='capex',
                            Month__c=m,
                            Intake_Year__c=string.valueof(MYear)
                    ));                    
               }
   
              if(newIntakeEntry.OPEX_EXTERNAL_LABOR_ON_SHORE__c>0 && Rate_Flag=='True')
              {
                     sr.add(new Intake__c(
                            Intake_Entry_Form__c=newIntakeEntry.ID,
                            Account_ID__c=lstbud_oel[0].ID,
                            Intake_Amount__c = OPEX_EXTERNAL_LABOR,
                            Capex_Opex__c='opex',
                            Month__c=k,
                            Intake_Year__c=string.valueof(CYear)
                    ));    
               }

            } 

            j=j+1;
            k=k+1;
            m=m+1;
            smon=smon+1;

            }

        }

    }              
    
    
        //////////////////////////
        List<Intake__c> IntakeLst;
        
        if(intake_id!='')
        {
            IntakeLst= [select id from Intake__c where Intake_Entry_Form__c =:intake_id];
                      
            if(IntakeLst.size() > 0)
            {
                delete IntakeLst;
            }
        
            if(sr.size() > 0)
              insert sr; 
                   
        }
        ///////////////////////////  
        }
            finally
            {
                TriggerConstant.FORECAST_TRIGGER_FLAG = true;
            }
        }           
}