trigger Forecast_Entry_Form_Delete on Forecast_Entry_Form__c (before delete, after delete ) 
{    
    if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
            try 
            {
                   List<Financial_Report__c> forecastLst;
                  
                   Date cannotdeleteDate = system.today();
                   Date forecaststartdate = system.today();
                   List<Forecast_Calendar__c> listFC = new List<Forecast_Calendar__c>();
                   
                   User current_user=[SELECT Profile_Name__c,id,User_Role_ID__c FROM User WHERE Id= :UserInfo.getUserId()] ;
                         
                     string tmp;
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                   tmp='True';
                     
                   for(Forecast_Entry_Form__c bef:Trigger.old)
                   {  
                       if (Trigger.isBefore && Trigger.isDelete )
                       {
                           system.debug('###Inside before delete -->ID-->'+ bef.Id );
                           if ((current_user.Profile_Name__c=='System Administrator') || (current_user.Profile_Name__c=='Finance Owners') || (current_user.Profile_Name__c=='Finance Members' )) {
                           listFC = [select Forecast_Start_Date__c, id, Name from Forecast_Calendar__c where (Forecast_Start_Date__c <= :cannotdeleteDate and Finance_Lock_Date__c >= :cannotdeleteDate) order by Forecast_Start_Date__c asc];
                           } else {
                           listFC = [select Forecast_Start_Date__c, id, Name from Forecast_Calendar__c where (Forecast_Start_Date__c <= :cannotdeleteDate and Forecast_End_Date__c >= :cannotdeleteDate)];
                           }
                           if (listFC.size() > 0) {
                           forecaststartdate = listFC.get(0).Forecast_Start_Date__c;
                           }
                           
                           Forecast_Entry_Form__c fec = [select  createddate from Forecast_Entry_Form__c where ID =: bef.Id];
                                                       
                            if ((fec.CreatedDate < forecaststartdate) && (!(current_user.Profile_Name__c=='System Administrator') ) ) {
                                bef.AddError('Delete Access Denied. You cannot delete the Forecast Entry Forms created in the previous forecast calendar months.');
                            } 
                            else {
                                system.debug('### Inside before delete ID-->'+ bef.Id );
                                forecastLst= [select id from Financial_Report__c where Type__c='Forecast' and Forecast_Entry_Form__c =: bef.Id];  
                                system.debug('###SR-->'+forecastLst.size() );
                                if(forecastLst.size() > 0)
                                {
                                    delete forecastLst;   
                                }
                            }

                       }
                   }
                   
                   ///////////////
               // User current_user=[SELECT Profile_Name__c,id FROM User WHERE Id= :UserInfo.getUserId()] ;
              
                for(Forecast_Entry_Form__c fef:Trigger.Old)
                 {  
                   if(Trigger.isDelete && ((current_user.Profile_Name__c=='Contractor') || (current_user.Profile_Name__c=='Comcast Employee')))
                   {
                       if(fef.Created_by_Userid__c!=current_user.id)
                       {
                           fef.AddError('Delete Access Denied for the Forecast Entry Forms created by other Users.');
                       }
                   }
                   
                  
             
             }
                   
                   
                   ////////////////
             }
             finally
              {
                  TriggerConstant.FORECAST_TRIGGER_FLAG = true;
              }
        
        }   
}