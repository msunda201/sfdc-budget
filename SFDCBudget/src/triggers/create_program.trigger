trigger create_program on Projects_2014__c (after insert) 
{    
    List<Projects_2014__c> pr = new List<Projects_2014__c>();
    List<Program__c> sr = new List<Program__c>();
    for (Projects_2014__c newProject: Trigger.New)
    {
                  List<Budget_Setting__c> lstbudgetsetting =[select ID,Name,Budget_Planning_Year__c from Budget_Setting__c where Active__c=True];
                        if(lstbudgetsetting.size()>0)
                        {
                            pr.add (new Projects_2014__c(
                             id = newProject.ID,
                             Budget_Setting__c = lstbudgetsetting[0].id
                          ));   
                        }  
                    if(newProject.Program_Name__c==NULL)
                    {
                      sr.add (new Program__c(
                         Name = newProject.Name,
                         Project_Source__c = newProject.id
                      ));   
                     
                    }
   }   
    if (pr.size() > 0) {
                     upsert pr;
   } 
   
   if (sr.size() > 0) {
                     insert sr;
   } 
   
 }