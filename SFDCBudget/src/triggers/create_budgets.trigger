trigger create_budgets on Budget_Entry_Form__c (after insert,after update) {

if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
            try {
                     TriggerConstant.FORECAST_TRIGGER_FLAG = false;
    //Variable Declaration                
    Integer sdays;
    Integer edays;
    Integer sday;
    Integer eday;
    Integer smon;
    Integer syear;
    Integer emon;
    Decimal mon_projection;
    Decimal avg_mon_projection;
    Decimal smon_projection;
    Decimal emon_projection;
    String QTR_STR; 
    String M_QTR_STR;
    Decimal Effort_Projection_CICL;
    Decimal CAPEX_INTERNAL_CAP_LABOR;
    Decimal Effort_Projection_OIL;
    Decimal OPEX_INTERNAL_LABOR;
    Decimal Effort_Projection_CECL;
    Decimal CAPEX_EXTERNAL_CAP_LABOR;
    Decimal Effort_Projection_OEL;              
    Decimal OPEX_EXTERNAL_LABOR;
    Integer CYear;
    Integer CMonth;
    Integer DAY_VAL;
    Decimal Hrs;
    Integer k=0;
    Integer m=0;
    date cDate;
    date mDate;
    integer MYear;
    Decimal EXTERNAL_LABOR_RATE_OFFSHORE;
    Decimal EXTERNAL_LABOR_RATE_ONSHORE;
    Decimal INTERNAL_LABOR_RATE;
    String Rate_ID;     
    Decimal Purchase_Cost;        
    String HW_Budget_Category;
    String Category_Type;
    String Funding_Entity_Other;
    String Budget_Funding_Type;
    string Rate_Flag;
    
    Set<ID> projectIds = new Set<ID>();
    
    //Declare List Array for Budgets
    List<Budget_2014__c> sr = new List<Budget_2014__c>();
    
    //Declare List Array for Budget Entry History 
    List<Budget_Entry_Form_History__c> beh = new List<Budget_Entry_Form_History__c>();

    //Declare List Array for Budget Entry History Software finance data
    
     //Declare List array for all different type of Budget Categories
        //////////////////////////
        List<Budget_Category__c> lstbud_oswm =[select ID,Capex_Opex__c from Budget_Category__c where Budget_Category_Prefix__c = 'OSWM'];       
        List<Budget_Category__c> lstbud_cicl =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'CICL'];
        List<Budget_Category__c> lstbud_oil =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'OIL'];
        List<Budget_Category__c> lstbud_cecl =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'CECL'];
        List<Budget_Category__c> lstbud_oel =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'OEL'];
        
        List<Budget_Category__c> lstbud_chws =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'CHWS'];
        List<Budget_Category__c> lstbud_chst =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'CHST'];
        List<Budget_Category__c> lstbud_chwn =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'CHWN'];
        List<Budget_Category__c> lstbud_chwo =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'CHWO'];

        List<Budget_Category__c> lstbud_cswl =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'CSWL'];        
     string project_id;
        ////////////////////////
        //User current_user=[SELECT Profile_Name__c,id FROM User WHERE Id= :UserInfo.getUserId()] ;
    for (Budget_Entry_Form__c newBudgetEntry: Trigger.New) 
    {    
        
       if(newBudgetEntry.Disable_Triggers__c!=TRUE)
       {
               
        List<Budget_Entry_Form_History__c> lstbeh =[select id from Budget_Entry_Form_History__c where Budget_Entry_Form__c =: newBudgetEntry.ID];
        
        if(Trigger.isUpdate)
        {
            //Update Budget Entry History data for Audit Trial
            if(lstbeh.size()>0)
            {
            beh.add(new Budget_Entry_Form_History__c(
            id=lstbeh[0].id,
            Name=newBudgetEntry.Name,
            Budget_Entry_Form__c=newBudgetEntry.ID,
            Capital_Expense__c=newBudgetEntry.Capital_Expense__c,
            External_Labor__c=newBudgetEntry.External_Labor__c,
            Off_Shore_Labor__c=newBudgetEntry.Off_Shore_Labor__c,
            Total_Spent_Projection__c=newBudgetEntry.Total_Spent_Projection__c,
            Project_Name__c=newBudgetEntry.Project__c,
            Bdgt_Category__c=newBudgetEntry.Bdgt_Category__c,
            Budget_Description__c=newBudgetEntry.Budget_Description__c,
            Budget_Type__c=newBudgetEntry.Budget_Type__c,
            Effort_Entity__c=newBudgetEntry.Effort_Entity__c,
            Funding_Entity_ID__c=newBudgetEntry.Funding_Entity_ID__c,
            Invoice_Date__c=newBudgetEntry.Invoice_Date__c,
            Justification__c=newBudgetEntry.Justification__c,
            Start_Date__c=newBudgetEntry.Start_Date__c,
            End_Date__c=newBudgetEntry.End_Date__c,
            Total_Effort_Hours__c=newBudgetEntry.Total_Effort_Hours__c,
            Total_SW_License_Amount__c=newBudgetEntry.Total_SW_License_Amount__c,
            Total_SW_Maintenance_Amount__c=newBudgetEntry.Total_SW_Maintenance_Amount__c,
            Vendor2014__c=newBudgetEntry.Vendor2014__c,
            Finance_Adjustment_Category__c=newBudgetEntry.Finance_Adjustment_Category__c,
            Payment_Amount_1__c=newBudgetEntry.Payment_Amount_1__c,
            Payment_Date_1__c=newBudgetEntry.Payment_Date_1__c,
            Payment_Amount_2__c=newBudgetEntry.Payment_Amount_2__c,
            Payment_Date_2__c=newBudgetEntry.Payment_Date_2__c,
            Payment_Amount_3__c=newBudgetEntry.Payment_Amount_3__c,
            Payment_Date_3__c=newBudgetEntry.Payment_Date_3__c,
            Payment_Amount_4__c=newBudgetEntry.Payment_Amount_4__c,
            Payment_Date_4__c=newBudgetEntry.Payment_Date_4__c,
            Payment_Amount_5__c=newBudgetEntry.Payment_Amount_5__c,
            Payment_Date_5__c=newBudgetEntry.Payment_Date_5__c,
            Finance_Payment_Amount_1__c=newBudgetEntry.Finance_Payment_Amount_1__c,
            Finance_Payment_Date_1__c=newBudgetEntry.Finance_Payment_Date_1__c,
            Finance_Payment_Amount_2__c=newBudgetEntry.Finance_Payment_Amount_2__c,
            Finance_Payment_Date_2__c=newBudgetEntry.Finance_Payment_Date_2__c,
            Finance_Payment_Amount_3__c=newBudgetEntry.Finance_Payment_Amount_3__c,
            Finance_Payment_Date_3__c=newBudgetEntry.Finance_Payment_Date_3__c,
            Finance_Payment_Amount_4__c=newBudgetEntry.Finance_Payment_Amount_4__c,
            Finance_Payment_Date_4__c=newBudgetEntry.Finance_Payment_Date_4__c
          ));
              if(beh.size()>0)
                update beh;
            }  
          

        }
        else
        {
            beh.add(new Budget_Entry_Form_History__c(
            Name=newBudgetEntry.Name,
            Budget_Entry_Form__c=newBudgetEntry.ID,
            Capital_Expense__c=newBudgetEntry.Capital_Expense__c,
            External_Labor__c=newBudgetEntry.External_Labor__c,
            Off_Shore_Labor__c=newBudgetEntry.Off_Shore_Labor__c,
            Total_Spent_Projection__c=newBudgetEntry.Total_Spent_Projection__c,
            Project_Name__c=newBudgetEntry.Project__c,
            Bdgt_Category__c=newBudgetEntry.Bdgt_Category__c,
            Budget_Description__c=newBudgetEntry.Budget_Description__c,
            Budget_Type__c=newBudgetEntry.Budget_Type__c,
            Effort_Entity__c=newBudgetEntry.Effort_Entity__c,
            Funding_Entity_ID__c=newBudgetEntry.Funding_Entity_ID__c,
            Invoice_Date__c=newBudgetEntry.Invoice_Date__c,
            Justification__c=newBudgetEntry.Justification__c,
            Start_Date__c=newBudgetEntry.Start_Date__c,
            End_Date__c=newBudgetEntry.End_Date__c,
            Total_Effort_Hours__c=newBudgetEntry.Total_Effort_Hours__c,
            Total_SW_License_Amount__c=newBudgetEntry.Total_SW_License_Amount__c,
            Total_SW_Maintenance_Amount__c=newBudgetEntry.Total_SW_Maintenance_Amount__c,
            Vendor2014__c=newBudgetEntry.Vendor2014__c,
            Finance_Adjustment_Category__c=newBudgetEntry.Finance_Adjustment_Category__c,
            Payment_Amount_1__c=newBudgetEntry.Payment_Amount_1__c,
            Payment_Date_1__c=newBudgetEntry.Payment_Date_1__c,
            Payment_Amount_2__c=newBudgetEntry.Payment_Amount_2__c,
            Payment_Date_2__c=newBudgetEntry.Payment_Date_2__c,
            Payment_Amount_3__c=newBudgetEntry.Payment_Amount_3__c,
            Payment_Date_3__c=newBudgetEntry.Payment_Date_3__c,
            Payment_Amount_4__c=newBudgetEntry.Payment_Amount_4__c,
            Payment_Date_4__c=newBudgetEntry.Payment_Date_4__c,
            Payment_Amount_5__c=newBudgetEntry.Payment_Amount_5__c,
            Payment_Date_5__c=newBudgetEntry.Payment_Date_5__c,
            Finance_Payment_Amount_1__c=newBudgetEntry.Finance_Payment_Amount_1__c,
            Finance_Payment_Date_1__c=newBudgetEntry.Finance_Payment_Date_1__c,
            Finance_Payment_Amount_2__c=newBudgetEntry.Finance_Payment_Amount_2__c,
            Finance_Payment_Date_2__c=newBudgetEntry.Finance_Payment_Date_2__c,
            Finance_Payment_Amount_3__c=newBudgetEntry.Finance_Payment_Amount_3__c,
            Finance_Payment_Date_3__c=newBudgetEntry.Finance_Payment_Date_3__c,
            Finance_Payment_Amount_4__c=newBudgetEntry.Finance_Payment_Amount_4__c,
            Finance_Payment_Date_4__c=newBudgetEntry.Finance_Payment_Date_4__c
          ));  
         
            
            if(beh.size()>0)
               insert beh;
         
        }
        
    
        //projectIds.add(newBudgetEntry.ID);
        project_id=newBudgetEntry.ID;
           // Declare Net Terms Value for Vendor
           
           if(newBudgetEntry.Net_Terms__c=='Net 10')
            {
                DAY_VAL=10;
            }  
           else if(newBudgetEntry.Net_Terms__c=='Net 15')
            {
                DAY_VAL=15;
            }
            else if(newBudgetEntry.Net_Terms__c=='Net 20')
            {
                DAY_VAL=15;
            }
            else if(newBudgetEntry.Net_Terms__c=='Net 30')
            {
                DAY_VAL=30;
            }
            else if(newBudgetEntry.Net_Terms__c=='Net 45')
            {
                DAY_VAL=45;
            }
            else if(newBudgetEntry.Net_Terms__c=='Net 60')
            {
                DAY_VAL=60;
            }
            else if(newBudgetEntry.Net_Terms__c=='Net 90')
            {
                DAY_VAL=90;
            }
            else if(newBudgetEntry.Net_Terms__c=='Net 120')
            {
                DAY_VAL=120;
            }
            else
            {
                DAY_VAL=0;
            }
            
            if(newBudgetEntry.Budget_Category_Type__c=='capex') 
            {
                Budget_Funding_Type=newBudgetEntry.Capex_All_Other_Funding__c;
            }
            
            if(newBudgetEntry.Budget_Category_Type__c=='opex')
            {
                Budget_Funding_Type=newBudgetEntry.Opex_All_Other_Funding__c;
            }
            
           
          
            //Variable declaration for sDate, eDate, sday,smon,syear,cDate for Other Costs (monthly) and Software Perpetual, Labor Lumpsum, Labor By Effort
            
            if(newBudgetEntry.Budget_Type__c=='Other Costs (Monthly)'  || newBudgetEntry.Budget_Type__c=='Software (Perpetual)' || newBudgetEntry.Budget_Type__c=='Labor (Lump Sum $)' || newBudgetEntry.Budget_Type__c=='Labor (By Effort)')
            {    
                date sDate=newBudgetEntry.Start_Date__c; 
                date eDate=newBudgetEntry.End_Date__c; 
        
                if(newBudgetEntry.Start_Date__c<>null)
                {
                    sday=sDate.DAY();
                    smon=sDate.MONTH();
                    syear=sDate.YEAR();
                    cDate = newBudgetEntry.Start_Date__c;
                   
                }
                if(newBudgetEntry.End_Date__c<>null)
                {
                    eday=eDate.DAY();
                    emon=eDate.MONTH();
                }
            }
            else
            {
                smon=0;
                syear=0;
            }
            
            //Variable declaration for cDate,CYear for Other Costs (Single Payment) If Budget Category Type is Opex
            
            if(newBudgetEntry.Budget_Type__c=='Other Costs (Single Payment)' && newBudgetEntry.Budget_Category_Type__c=='capex')
            {
                 if(newBudgetEntry.Invoice_Date__c<>null)
                 {
                        cDate = newBudgetEntry.Invoice_Date__c;  
                 }
                 CYear=cDate.YEAR();
            }
            
            //Variable declaration for cDate,CYear for Other Costs (Single Payment) and Budget Category Type is Opex
            
            if(newBudgetEntry.Budget_Type__c=='Other Costs (Single Payment)' && newBudgetEntry.Budget_Category_Type__c=='opex')
            {
                 if(newBudgetEntry.Delivery_Date__c<>null)
                 {
                        cDate = newBudgetEntry.Delivery_Date__c;  
                 }
                 CYear=cDate.YEAR();
            }
      
              //Variable declaration for cDate,CYear for HW Assets
                    
            if(newBudgetEntry.Budget_Type__c=='HW Asset')
            {
                 if(newBudgetEntry.Invoice_Date__c<>null)
                 {
                        cDate = newBudgetEntry.Invoice_Date__c;  
                 }
                 CYear=CDate.YEAR();
            }
        
        //Calculate Hrs =Total Hours/Month Difference
        if(newBudgetEntry.Budget_Type__c=='Labor (By Effort)' && newBudgetEntry.Total_Effort_Hours__c>0 && newBudgetEntry.Month_Difference__c>0)
        {
            Hrs=newBudgetEntry.Total_Effort_Hours__c/newBudgetEntry.Month_Difference__c;
        }
        //Calculate Purchase cost,Month Projection for HW Asset and 
        if(newBudgetEntry.Budget_type__C=='HW Asset')
        {
             //cDate = newBudgetEntry.Start_Date__c;  
             cDate = newBudgetEntry.Invoice_Date__c;  
             CYear=CDate.YEAR(); 
             
             if(newBudgetEntry.Asset_Size__c!=null && newBudgetEntry.Asset_Type__c!='')
             {
                 List<Asset_Mapping__c> lstsp_prj =[select Purchase_Cost__c from Asset_Mapping__c where Asset_Size__c=:String.valueOf(newBudgetEntry.Asset_Size__c) AND Asset_Type__c = :String.valueOf(newBudgetEntry.Asset_Type__c)];
                   
                 if(lstsp_prj.size()>0)
                 {
                     Purchase_Cost    = lstsp_prj[0].Purchase_Cost__c;
                 }
                 
                 if(purchase_Cost>0 && newBudgetEntry.Asset_Quantity__c>0 && newBudgetEntry.Month_Difference__c>0)
                 {
                    mon_projection=(newBudgetEntry.Asset_Quantity__c*Purchase_Cost)/newBudgetEntry.Month_Difference__c;     
                 }
             }            
        }
        
        //Calculate Month Projection for Other costs (Monthly)
        
        if(newBudgetEntry.Budget_Type__c=='Other Costs (Monthly)' && newBudgetEntry.Total_Spent_Projection__c>=0 && newBudgetEntry.Month_Difference__c>0)
        {
            if(newBudgetEntry.Total_Spent_Projection__c>0)
            {
                mon_projection=newBudgetEntry.Total_Spent_Projection__c/newBudgetEntry.Month_Difference__c;
            }
            else
            {
                mon_projection=0;
            }
        }
        
        //Create Budgets for Finance Adjustments for 4 Quarters
        
        if(newBudgetEntry.Budget_Type__c=='Finance Adjustment')
        {
             String budvalue;
             String budtypevalue;
 
             budvalue=newBudgetEntry.Bdgt_Category__c;
             budtypevalue=newBudgetEntry.Budget_Category_Type__c;
                       
            if(newBudgetEntry.Finance_Payment_Amount_1__c<>null && newBudgetEntry.Finance_Payment_Date_1__c<>null)
            {
                 sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            Budget_Category__c=budvalue,
                            Budget_Category_Type__c=budtypevalue,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_Capex__c,
                            Budget_Funding_Type__c=newBudgetEntry.Budget_Category_Type_Formula__c,
                            Spent_Projection__c = newBudgetEntry.Finance_Payment_Amount_1__c,
                            Month__c=newBudgetEntry.Finance_Payment_Date_1__c.MONTH(),
                            Year__c=string.valueof(newBudgetEntry.Finance_Payment_Date_1__c.YEAR())
                            ));  
       
           }
            
           if(newBudgetEntry.Finance_Payment_Amount_2__c<>null && newBudgetEntry.Finance_Payment_Date_2__c<>null)
            {
   
                 sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            
                            Budget_Category__c=budvalue,
                            Budget_Category_Type__c=budtypevalue,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_Capex__c,
                            Budget_Funding_Type__c=newBudgetEntry.Budget_Category_Type_Formula__c,
                            Spent_Projection__c = newBudgetEntry.Finance_Payment_Amount_2__c,
                            Month__c=newBudgetEntry.Finance_Payment_Date_2__c.MONTH(),
                            Year__c=string.valueof(newBudgetEntry.Finance_Payment_Date_2__c.YEAR())
                            ));  
       
           }        
           
           if(newBudgetEntry.Finance_Payment_Amount_3__c<>null && newBudgetEntry.Finance_Payment_Date_3__c<>null)
            {
                 sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            Budget_Category__c=budvalue,
                            Budget_Category_Type__c=budtypevalue,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_Capex__c,
                            Budget_Funding_Type__c=newBudgetEntry.Budget_Category_Type_Formula__c,
                            Spent_Projection__c = newBudgetEntry.Finance_Payment_Amount_3__c,
                            Month__c=newBudgetEntry.Finance_Payment_Date_3__c.MONTH(),
                            Year__c=string.valueof(newBudgetEntry.Finance_Payment_Date_3__c.YEAR())
                            ));  
       
           }
           
           if(newBudgetEntry.Finance_Payment_Amount_4__c<>null && newBudgetEntry.Finance_Payment_Date_4__c<>null)
            {
                
                 sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            Budget_Category__c=budvalue,
                            Budget_Category_Type__c=budtypevalue,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_Capex__c,
                            Budget_Funding_Type__c=newBudgetEntry.Budget_Category_Type_Formula__c,
                            Spent_Projection__c = newBudgetEntry.Finance_Payment_Amount_4__c,
                            Month__c=newBudgetEntry.Finance_Payment_Date_4__c.MONTH(),
                            Year__c=string.valueof(newBudgetEntry.Finance_Payment_Date_4__c.YEAR())
                            ));  
       
           }        
        
        
        
        }
        
        //Create Budgets for Multiple Account by Months
        
        if(newBudgetEntry.Budget_Type__c=='Multiple Month by Account')
        {
                For(Integer i=1;i<=12; i++)
                {    
                    decimal FMA_Value;
                
                    if(i==1)
                    {
                        FMA_Value=newBudgetEntry.Jan__c;
                    }  
                    else if(i==2)
                    {
                        FMA_Value=newBudgetEntry.Feb__c;
                    }
                    else if(i==3)
                    {
                       FMA_Value=newBudgetEntry.Mar__c;
                    }
                    else if(i==4)
                    {
                        FMA_Value=newBudgetEntry.Apr__c;
                    }
                    else if(i==5)
                    {
                        FMA_Value=newBudgetEntry.May__c;
                    }
                    else if(i==6)
                    {
                        FMA_Value=newBudgetEntry.Jun__c;
                    }
                    else if(i==7)
                    {
                        FMA_Value=newBudgetEntry.Jul__c;
                    }
                    else if(i==8)
                    {
                        FMA_Value=newBudgetEntry.Aug__c;
                    }
                    else if(i==9)
                    {
                        FMA_Value=newBudgetEntry.Sep__c;
                    }
                    else if(i==10)
                    {
                        FMA_Value=newBudgetEntry.Oct__c;
                    }
                    else if(i==11)
                    {
                        FMA_Value=newBudgetEntry.Nov__c;
                    }
                    else
                    {
                        FMA_Value=newBudgetEntry.Dec__c;
                    }

                    if(FMA_Value!=NULL)
                    {   
                        sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            Budget_Category__c=newBudgetEntry.Bdgt_Category__c,
                            Budget_Category_Type__c=newBudgetEntry.Budget_Category_Type__c,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_Capex__c,
                            Budget_Funding_Type__c=newBudgetEntry.Budget_Category_Type_Formula__c,
                            Spent_Projection__c = FMA_Value,
                            Month__c=i,
                            Year__c=newBudgetEntry.Adjustment_Year__c
                           ));                
                    }
            }
        }
        
        //Create Budgets for Software Perpetual for 5 Entries
                
        if(newBudgetEntry.Budget_Type__c=='Software (Perpetual)' && newBudgetEntry.Month_Difference__c>0)
        {
                
        if(newBudgetEntry.Payment_Date_1__c<>null && lstbud_cswl.size() > 0)
        { 
                
                 sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            Budget_Category__c= lstbud_cswl[0].ID,
                            Budget_Category_Type__c='capex',
                            Budget_Funding_Type__c=newBudgetEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_Capex__c,
                            Spent_Projection__c = newBudgetEntry.Payment_Amount_1__c,
                            Month__c=newBudgetEntry.Payment_Date_1__c.MONTH(),
                            Year__c=string.valueof(newBudgetEntry.Payment_Date_1__c.YEAR())
                            ));  
       
       }
       
       if(newBudgetEntry.Payment_Date_2__c<>null  && lstbud_cswl.size() > 0)
       {                     
                 sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            Budget_Category__c= lstbud_cswl[0].ID,
                            Budget_Category_Type__c='capex',
                            Budget_Funding_Type__c=newBudgetEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_Capex__c,
                            Spent_Projection__c = newBudgetEntry.Payment_Amount_2__c,
                            Month__c=newBudgetEntry.Payment_Date_2__c.MONTH(),
                            Year__c=string.valueof(newBudgetEntry.Payment_Date_2__c.YEAR())
                            ));  
        }
       if(newBudgetEntry.Payment_Date_3__c<>null  && lstbud_cswl.size() > 0)
       {            
                 sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            Budget_Category__c= lstbud_cswl[0].ID,
                            Budget_Category_Type__c='capex',
                            Budget_Funding_Type__c=newBudgetEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_Capex__c,
                            Spent_Projection__c = newBudgetEntry.Payment_Amount_3__c,
                            Month__c=newBudgetEntry.Payment_Date_3__c.MONTH(),
                            Year__c=string.valueof(newBudgetEntry.Payment_Date_3__c.YEAR())
                            ));
        }
       if(newBudgetEntry.Payment_Date_4__c<>null  && lstbud_cswl.size() > 0)
       {                                    
                
                 sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            Budget_Category__c= lstbud_cswl[0].ID,
                            Budget_Category_Type__c='capex',
                            Budget_Funding_Type__c=newBudgetEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_Capex__c,
                            Spent_Projection__c = newBudgetEntry.Payment_Amount_4__c,
                            Month__c=newBudgetEntry.Payment_Date_4__c.MONTH(),
                            Year__c=string.valueof(newBudgetEntry.Payment_Date_4__c.YEAR())
                            ));
       }
       
       if(newBudgetEntry.Payment_Date_5__c<>null  && lstbud_cswl.size() > 0)
       {            
                                
                sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            Budget_Category__c= lstbud_cswl[0].ID,
                            Budget_Category_Type__c='capex',
                            Budget_Funding_Type__c=newBudgetEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_Capex__c,
                            Spent_Projection__c = newBudgetEntry.Payment_Amount_5__c,
                            Month__c=newBudgetEntry.Payment_Date_5__c.MONTH(),
                            Year__c=string.valueof(newBudgetEntry.Payment_Date_5__c.YEAR())
                            ));                  
             
        }
            
  }

        //Create Budgets for Single Payment
                        
        if(newBudgetEntry.Budget_Type__c=='Other Costs (Single Payment)')
        {
            //cDate = newBudgetEntry.Delivery_Date__c;  
            
            //cDate = newBudgetEntry.Invoice_Date__c;  
            if(newBudgetEntry.Apply_Net_Terms__c==1)
            {
                cDate = cDate.addDays(DAY_VAL); 
            }
            CYear=cDate.YEAR();
            k=cDate.MONTH();           
                         

                  sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            Budget_Category__c= newBudgetEntry.Bdgt_Category__c,
                            Budget_Category_Type__c=newBudgetEntry.Budget_Category_Type__c,
                            Budget_Funding_Type__c=Budget_Funding_Type,
                            Funding_Entity__c= newBudgetEntry.Category_Funding_Entity__c,
                            Spent_Projection__c = newBudgetEntry.Total_Spent_Projection__c,
                            Month__c=k,
                            Year__c=string.valueof(CYear)
                            ));   
                
        }
        
        Integer j=0; 
        k=smon;  
        
         if(newBudgetEntry.Budget_Type__c=='Labor (Lump Sum $)' || newBudgetEntry.Budget_Type__c=='Labor (By Effort)')
         {
                   if(cDate<>null)
                    {
                        //cDate = cDate.addDays(DAY_VAL); 
                        mDate= cDate.addDays(DAY_VAL+31);
                        CYear =cDate.YEAR();
                        MYear =mDate.YEAR();
                        k=CDate.MONTH();
                        m=mDate.MONTH();
                    }
         }  
        
        
        //Declare Month & Year variables for HW Asset & Software Perpetual
        
        if(newBudgetEntry.Budget_Type__c=='HW Asset' || newBudgetEntry.Budget_Type__c=='Other Costs (Monthly)' || newBudgetEntry.Budget_Type__c=='Software (Perpetual)')
        {
            
                if(newBudgetEntry.Apply_Net_Terms__c==1 && newBudgetEntry.Budget_Type__c == 'Other Costs (Monthly)')
                {
                    cDate = cDate.addDays(DAY_VAL); 
                }
                
                if(newBudgetEntry.Budget_Type__c=='HW Asset' || newBudgetEntry.Budget_Type__c=='Software (Perpetual)')
                {
                    cDate = cDate.addDays(DAY_VAL); 
                }
                CYear =cDate.YEAR();
                k=CDate.MONTH();
                               
                if(newBudgetEntry.Budget_Type__c=='Other Costs (Monthly)')
                {
                    if(newBudgetEntry.Apply_Net_Terms__c==1)
                    {
                        mDate= cDate.addDays(31);
                        MYear =mDate.YEAR();
                        m=mDate.MONTH();
                    }
                    else
                    {
                        MYear =cDate.YEAR();
                        m=cDate.MONTH();
                    }
                }
               
                
            
        }        
        
       
        
      
        
if(newBudgetEntry.Budget_Type__c=='Software (Perpetual)' || newBudgetEntry.Budget_type__C=='HW Asset' || newBudgetEntry.Budget_Type__c=='Other Costs (Monthly)' || newBudgetEntry.Budget_Type__c=='Labor (Lump Sum $)' || newBudgetEntry.Budget_Type__c=='Labor (By Effort)')
{       
       For(Integer i=1;i<=(newBudgetEntry.Month_Difference__c); i++)
        {
            if(k>12 && CYear<>null)
            {
                k=1;
                CYear=CYear+1;
            } 
            
            if(m>12 && MYear<>null)
            {
                m=1;
                MYear=MYear+1;
            }
            
            //Create Budgets for Software Perpetual
            
            if(newBudgetEntry.Budget_Type__c=='Software (Perpetual)' && lstbud_oswm.size() > 0)
            {
                mon_projection=(newBudgetEntry.Total_SW_Maintenance_Amount__c)/newBudgetEntry.Month_Difference__c;   
            
                  sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            Budget_Category__c=lstbud_oswm[0].ID,
                            Budget_Category_Type__c='opex',
                            Budget_Funding_Type__c=newBudgetEntry.Opex_All_Other_Funding__c,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_Opex__c,
                            Spent_Projection__c = mon_projection,
                            Month__c=k,
                            Year__c=string.valueof(CYear)
                            ));   
            
            }
           
            //Create Budgets for HW Asset  
            
            if(newBudgetEntry.Budget_type__C=='HW Asset')
            {

                
                if(newBudgetEntry.Asset_Type__c=='Servers')
                {
                    HW_Budget_Category=lstbud_chws[0].ID;
                }
                else if(newBudgetEntry.Asset_Type__c=='Storage')
                {
                    HW_Budget_Category=lstbud_chst[0].ID;
                }
                else if(newBudgetEntry.Asset_Type__c=='Network')
                {
                    HW_Budget_Category=lstbud_chwn[0].ID;
                }
                else
                {
                    HW_Budget_Category=lstbud_chwo[0].ID;
                }
            

                  sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            Budget_Category__c= HW_Budget_Category,
                            Budget_Category_Type__c='capex',
                            Budget_Funding_Type__c=newBudgetEntry.Capex_All_Other_Funding__c,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_Capex__c,
                            Month_Factor__c=mon_projection,
                            Spent_Projection__c = mon_projection,
                            Month__c=k,
                            Year__c=string.valueof(CYear)
                            ));   

            }
            
            //Create Budgets for Other Costs (Monthly)
            
            if(newBudgetEntry.Budget_Type__c=='Other Costs (Monthly)')
            {
            

                  sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            Budget_Category__c= newBudgetEntry.Bdgt_Category__c,
                            Budget_Category_Type__c=newBudgetEntry.Budget_Category_Type__c,
                            Budget_Funding_Type__c=Budget_Funding_Type,
                            Funding_Entity__c= newBudgetEntry.Category_Funding_Entity__c,
                            Month_Factor__c=mon_projection,
                            Spent_Projection__c = mon_projection,
                            Month__c=m,
                            Year__c=string.valueof(MYear)
                            ));   
            
            }
            
            //Create Budgets for Labor (Lump Sum) & Labor - By Effort
            
            if(newBudgetEntry.Budget_Type__c=='Labor (Lump Sum $)' || newBudgetEntry.Budget_Type__c=='Labor (By Effort)')
            {
            
                
                 //Rate Exception Error. Display Error message when there is no Rate declared for a director group in a specified Year
                 Rate_Flag='True';
                 if(newBudgetEntry.Director_Group__c !=null)
                 {
                    List<Rate__c> lstRate =[select ID,EXTERNAL_LABOR_RATE_OFFSHORE__c,EXTERNAL_LABOR_RATE_ONSHORE__c,INTERNAL_LABOR_RATE__c from Rate__c where Year__c=:String.valueOf(CYear) AND Director_Group__c = :newBudgetEntry.Director_Group__c];
                   
                    if(lstRate.size()>0)
                    {
                         EXTERNAL_LABOR_RATE_OFFSHORE    = lstRate[0].EXTERNAL_LABOR_RATE_OFFSHORE__c;
                         EXTERNAL_LABOR_RATE_ONSHORE     = lstRate[0].EXTERNAL_LABOR_RATE_ONSHORE__c;
                         INTERNAL_LABOR_RATE             = lstRate[0].INTERNAL_LABOR_RATE__c;
                         Rate_ID                         = lstRate[0].ID;
                    }
                    else
                    {
                         EXTERNAL_LABOR_RATE_OFFSHORE    = 0;
                         EXTERNAL_LABOR_RATE_ONSHORE     = 0;
                         INTERNAL_LABOR_RATE             = 0;
                         Rate_ID = '';
                    }
                       
                    if(EXTERNAL_LABOR_RATE_OFFSHORE==0)
                    {
                        newBudgetEntry.addError('External Labor - Offshore Rate cannot be Null for the Selected Director Group');
                        Rate_Flag='False';
                       
                    }
                    
                    if(EXTERNAL_LABOR_RATE_ONSHORE==0)
                    {
                       newBudgetEntry.addError('External Labor - OnShore Rate cannot be Null for the Selected Director Group');
                       Rate_Flag='False';
                    }
                    
                    if(INTERNAL_LABOR_RATE==0)
                    {
                        newBudgetEntry.addError('Internal Labor Rate cannot be Null for the Selected Director Group');
                        Rate_Flag='False';
                    }
                 
                 }
                
                
                //Refer Excel Macros for the below formula calcualtion       
                if(newBudgetEntry.Budget_Type__c=='Labor (Lump Sum $)')
                {
                     CAPEX_INTERNAL_CAP_LABOR=newBudgetEntry.Monthly_Spend_Projection__c*((newBudgetEntry.Capital_Expense__c/100)*(1-(newBudgetEntry.External_Labor__c/100)));
                     OPEX_INTERNAL_LABOR=newBudgetEntry.Monthly_Spend_Projection__c*((1-(newBudgetEntry.Capital_Expense__c/100)))*(1-(newBudgetEntry.External_Labor__c/100));
                     CAPEX_EXTERNAL_CAP_LABOR=newBudgetEntry.Monthly_Spend_Projection__c*(((newBudgetEntry.Capital_Expense__c/100))*((newBudgetEntry.External_Labor__c/100)));
                     OPEX_EXTERNAL_LABOR=newBudgetEntry.Monthly_Spend_Projection__c*((1-(newBudgetEntry.Capital_Expense__c/100)))*(newBudgetEntry.External_Labor__c/100);
                     //Rate_ID ='';
                }
                else
                {
                    if(smon>12 && syear<>null)
                    {
                        smon=1;
                        syear=syear+1;
                    }             

                 //Calculate Effort Projection
                 // Effort_Projection_CICL=Hrs*(Capex Internal Labor)
                 if(Hrs>0)
                 {
                     Effort_Projection_CICL=Hrs*(newBudgetEntry.CAPEX_INTERNAL_CAP_LABOR_ON_SHORE__c/100);
                     Effort_Projection_OIL=Hrs*(newBudgetEntry.OPEX_INTERNAL_LABOR_ON_SHORE__c/100);
                     Effort_Projection_CECL=Hrs*(newBudgetEntry.CAPEX_EXTERNAL_CAP_LABOR_ON_SHORE__c/100);
                     Effort_Projection_OEL=Hrs*(newBudgetEntry.OPEX_EXTERNAL_LABOR_ON_SHORE__c/100);   
                         
                     if(INTERNAL_LABOR_RATE>0)
                     {
                         CAPEX_INTERNAL_CAP_LABOR=(INTERNAL_LABOR_RATE)*Effort_Projection_CICL;
                         OPEX_INTERNAL_LABOR=(INTERNAL_LABOR_RATE)*Effort_Projection_OIL; 
                     }
                        
                         CAPEX_EXTERNAL_CAP_LABOR=((EXTERNAL_LABOR_RATE_ONSHORE *(1-(newBudgetEntry.Off_Shore_Labor__c/100)))+ (EXTERNAL_LABOR_RATE_OFFSHORE *(newBudgetEntry.Off_Shore_Labor__c/100)))*Effort_Projection_CECL;
                         OPEX_EXTERNAL_LABOR=((EXTERNAL_LABOR_RATE_ONSHORE *(1-(newBudgetEntry.Off_Shore_Labor__c/100)))+ (EXTERNAL_LABOR_RATE_OFFSHORE *(newBudgetEntry.Off_Shore_Labor__c/100)))*Effort_Projection_OEL;      
                  }
            }
          
            //Create budgets for Capex Internal Cap Labor On Shore
            if(newBudgetEntry.CAPEX_INTERNAL_CAP_LABOR_ON_SHORE__c>0 && Rate_Flag=='True')
            {
                                                         
                sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            Budget_Category__c=lstbud_cicl[0].ID,
                            Total_Monthly_Hrs__c=Hrs,
                            Effort_Projection__c = Effort_Projection_CICL,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_CICL__c,
                            Month_Factor__c=mon_projection,
                            Budget_Category_Type__c='capex',
                             Budget_Funding_Type__c=newBudgetEntry.CICL_Funding__c,
                            Spent_Projection__c = CAPEX_INTERNAL_CAP_LABOR,
                            EXTERNAL_LABOR_RATE_OFFSHORE__c= EXTERNAL_LABOR_RATE_OFFSHORE,
                            EXTERNAL_LABOR_RATE_ONSHORE__c= EXTERNAL_LABOR_RATE_ONSHORE,
                            INTERNAL_LABOR_RATE__c= INTERNAL_LABOR_RATE,
                            Month__c=k,
                            Rate_ID__c=Rate_ID,
                            Year__c=string.valueof(CYear),       
                            Effort_Month__c=smon,
                            Effort_Year__c=string.valueof(syear)
                            ));
            }

            //Create budgets for Opex Internal Cap Labor On Shore            
            if(newBudgetEntry.OPEX_INTERNAL_LABOR_ON_SHORE__c>0  && Rate_Flag=='True')
            {
             
                sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            //Project__c=newBudgetEntry.Project_ID__c,
                            Budget_Category__c=lstbud_oil[0].ID,
                            Total_Monthly_Hrs__c=Hrs,
                            Effort_Projection__c =  Effort_Projection_OIL,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_OIL__c,
                            Month_Factor__c=mon_projection,
                            Budget_Category_Type__c='opex',
                            Budget_Funding_Type__c=newBudgetEntry.OIL_Funding__c,
                            Spent_Projection__c = OPEX_INTERNAL_LABOR,
                            EXTERNAL_LABOR_RATE_OFFSHORE__c= EXTERNAL_LABOR_RATE_OFFSHORE,
                            EXTERNAL_LABOR_RATE_ONSHORE__c= EXTERNAL_LABOR_RATE_ONSHORE,
                            INTERNAL_LABOR_RATE__c= INTERNAL_LABOR_RATE,
                            Month__c=k,
                            Rate_ID__c=Rate_ID,
                            Effort_Month__c=smon,
                            Effort_Year__c=string.valueof(syear),
                            Year__c=string.valueof(CYear)
                            ));
                
            }
            
            //Create budgets for Capex External Cap Labor On Shore
            if(newBudgetEntry.CAPEX_EXTERNAL_CAP_LABOR_ON_SHORE__c>0  && Rate_Flag=='True')
            {        
                sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            //Project__c=newBudgetEntry.Project_ID__c,
                            Budget_Category__c=lstbud_cecl[0].ID,
                            Total_Monthly_Hrs__c=Hrs,
                            Effort_Projection__c = Effort_Projection_CECL,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_CECL__c,
                            Month_Factor__c=mon_projection,
                            Budget_Category_Type__c='capex',
                            Budget_Funding_Type__c=newBudgetEntry.CECL_Funding__c,
                            Spent_Projection__c = CAPEX_EXTERNAL_CAP_LABOR,
                            EXTERNAL_LABOR_RATE_OFFSHORE__c= EXTERNAL_LABOR_RATE_OFFSHORE,
                            EXTERNAL_LABOR_RATE_ONSHORE__c= EXTERNAL_LABOR_RATE_ONSHORE,
                            INTERNAL_LABOR_RATE__c= INTERNAL_LABOR_RATE,
                            Month__c=m,
                            Rate_ID__c=Rate_ID,
                            Year__c=string.valueof(MYear),
                            Effort_Month__c=smon,
                            Effort_Year__c=string.valueof(syear)
                            ));
                
            }
           
           //Create Budgets for Opex External Labor On Shore 
           if(newBudgetEntry.OPEX_EXTERNAL_LABOR_ON_SHORE__c>0 && Rate_Flag=='True')
           {
                
                //List<Budget_Category__c> lstbud_oel =[select ID from Budget_Category__c where Budget_Category_Prefix__c = 'OEL'];
                
                sr.add(new Budget_2014__c(
                            Budget__c= newBudgetEntry.ID,
                            Budget_Category__c=lstbud_oel[0].ID,
                            Total_Monthly_Hrs__c=Hrs,
                            Effort_Projection__c =  Effort_Projection_OEL,
                            Funding_Entity__c= newBudgetEntry.Funding_Entity_OEL__c,
                            Month_Factor__c=mon_projection,
                            Budget_Category_Type__c='opex',
                            Budget_Funding_Type__c=newBudgetEntry.OEL_Funding__c,
                            Spent_Projection__c = OPEX_EXTERNAL_LABOR,
                            EXTERNAL_LABOR_RATE_OFFSHORE__c= EXTERNAL_LABOR_RATE_OFFSHORE,
                            EXTERNAL_LABOR_RATE_ONSHORE__c= EXTERNAL_LABOR_RATE_ONSHORE,
                            INTERNAL_LABOR_RATE__c= INTERNAL_LABOR_RATE,
                            Month__c=k,
                            Rate_ID__c=Rate_ID,
                            Year__c=string.valueof(CYear),
                            Effort_Month__c=smon,
                            Effort_Year__c=string.valueof(syear)
                            ));
                            
                }
            }
             j=j+1;
             k=k+1;
             m=m+1;
             smon=smon+1;
             
        }
        
       
 }      
        
    
    
        //Delete the Budget Entries for the selected Project.
        List<Budget_2014__c> budgetLst;
        if(project_id!='')
            budgetLst= [select id from Budget_2014__c where Budget__c =:project_id];
        
            if(budgetLst.size() > 0)
            {
                delete budgetLst;
            }
            
        // Insert the Budget List
        if(sr.size() > 0)
            insert sr; 
       }      
            
      }  
          
    }
            finally
            {
                TriggerConstant.FORECAST_TRIGGER_FLAG = true;
            }
       
        }

}