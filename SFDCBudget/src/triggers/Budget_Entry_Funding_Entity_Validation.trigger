trigger Budget_Entry_Funding_Entity_Validation on Budget_Entry_Form__c (before insert,before update) 
{
       if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
          try {
             User current_user=[SELECT Profile_Name__c,id,User_Role_ID__c FROM User WHERE Id= :UserInfo.getUserId()] ;
                         
             for(Budget_Entry_Form__c bef:Trigger.New)
             {        
                   if(((current_user.Profile_Name__c=='Finance Owners') || (current_user.Profile_Name__c=='Finance Members')) && (bef.Funding_Entity_ID__c==null))
                   {                          
                       if(bef.Funding_Entity_ID__c==null)
                       {
                            bef.AddError('Please select Funding Entity.');
                       }
                   }

             }     
            
              }
             finally
              {
                  TriggerConstant.FORECAST_TRIGGER_FLAG = true;
              }
            
         }
}