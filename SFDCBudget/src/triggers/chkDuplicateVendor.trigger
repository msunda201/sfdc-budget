trigger chkDuplicateVendor  on Vendor2014__c (before insert, before update) {


/***************************************************************************

Trigger Name: chkDuplicateName
Object Name : Account
Created By  : Mahendran Sundarraj
Created Date: 02/18/2013
Description : This trigger will prevent the user from entering duplicate Name ids to the Vendor
              record.               
***************************************************************************/
    Set<String> vendorname = new Set<String>();
    
    //Get the phone numbers to the set
    for(Vendor2014__c venObj: Trigger.New){
        if(venObj.Name != null)
            vendorname.add(venObj.Name.toUpperCase());
        
    }
    
       //Query the related accounts based on the phone numbers colleced above
    List<Vendor2014__c> venList = [select Name from Vendor2014__c where Name in: vendorname];
    Map<String, Vendor2014__c> NameMap = new Map<String, Vendor2014__c>();
    
    for(Integer i = 0; i<venList.size(); i++){
        NameMap.put(venList.get(i).Name.toUpperCase(), venList.get(i));
    }
    
   
       
     for(Vendor2014__c ven: Trigger.New){
           
            //This will throw error on duplicate phone number entry on record creation
            if(NameMap.get(ven.Name.toUpperCase()) != null)
                ven.AddError('Duplicate Vendor Name');         
           
        }
    


      


}