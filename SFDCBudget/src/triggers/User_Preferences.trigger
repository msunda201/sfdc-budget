trigger User_Preferences on User_Preference__c (before insert,before update) 
{
       if(TriggerConstant.FORECAST_TRIGGER_FLAG)
        {
          try {
             User current_user=[SELECT  Profile_Name__c,id FROM User WHERE Id= :UserInfo.getUserId()] ;
              
                for(User_Preference__c bef:Trigger.new)
                 {       
                    if(current_user.Profile_Name__c!='System Administrator')
                    {                         
                       if(bef.User__c!=current_user.id)
                         {
                            bef.AddError('Access Denied to update other user preferences.');
                         }
                    }
                 }
            
             }
             finally
             {
                  TriggerConstant.FORECAST_TRIGGER_FLAG = true;
             }
            
         }
}