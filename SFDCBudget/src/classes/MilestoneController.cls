public class MilestoneController {
	
	public Milestone__c objmilestone {get; set;}
	
	public String ResourcePro {get;set;}
	
	public String DurableTeam {get;set;}
	
		// Constructor method with StandardController object
	public MilestoneController (Apexpages.Standardcontroller stdcontroller) {
	   objmilestone = new Milestone__c();
	   objmilestone = (Milestone__c)stdcontroller.getRecord();
	  
	}
	
	public MilestoneController(){
		ResourcePro = DurableTeam = null;
	}
	
	//Save Method for Milestone
	public Pagereference SaveMileStone(){
		
		try{
			  if(objmilestone.Name == null)
             {
             	Apexpages.Message errmsg = new Apexpages.Message(ApexPages.Severity.ERROR,'Please Enter a value for Milestone Name');
                ApexPages.addMessage(errmsg);
                return null;
             }
         
             if(objmilestone.Milestone_Date__c == null)
             {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter a value for Milestone Date');
                ApexPages.addMessage(myMsg);
                return null;
             }
             
                       
			upsert objmilestone;
			Pagereference pgref = new Pagereference('/'+objmilestone.id);
			return pgref;
		}
		catch(exception e){
			
			Apexpages.addMessages(e);
			return null;
		}
		
	}
	
	
	public List<Selectoption> getListDurableTeams(){
		
		system.debug('Inside getListDurableTeams');
		
		List<Selectoption> optionDT = new List<Selectoption>();
		optionDT.add(new Selectoption('','--Choose--'));
		
		List<DTwithProjects__c> getDTID = [select Durable_Teams__c  from DTwithProjects__c where  Resource_Projects__c = :objmilestone.Project__c];
		system.debug('Durable_Teams__c size ' + getDTID.size());
		for (Integer i = 0; i < getDTID.size(); i++ ) {
			Durable_Teams__c getDurableTeam = [select id, Name from Durable_Teams__c where id =: getDTID.get(i).Durable_Teams__c ];
			system.debug('Inside if  Durable_Teams__c Name ' + getDurableTeam.Name + 'ID-->' + getDurableTeam.id);
			if (getDurableTeam.id != null) {
			optionDT.add(new Selectoption(getDurableTeam.id, getDurableTeam.Name));
			}
        }
        
        return optionDT;
                
	}
	
	
	

}