global class Copy_Timesheet_Calendar implements Database.Batchable<sObject>
{
    
    String email;
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
          date dv = system.today()-1;
          string Timesheet_Calendar_Val;

          Timesheet_Calendar__c listTC = null;
          List<Timesheet_Calendar__c> resultTC = [select id,End_Date__c,Start_Date__c from Timesheet_Calendar__c where (Start_Date__c <= :dv and End_Date__c >= :dv)];
          if(resultTC.size() > 0)
          {
            Timesheet_Calendar_Val= resultTC[0].id;
          }
          
         date dv1 = system.today()+1;
         date dv2 = system.today()+7;
         //String Timesheet_Calendar__c;
         
         Timesheet_Calendar__c objTS_calendar = new Timesheet_Calendar__c();
         objTS_calendar.Name = 'Week Ending - '+dv2.month()+'/'+dv2.day()+'/'+dv2.year();
         objTS_calendar.Start_Date__c=dv1;
         objTS_calendar.End_Date__c=dv2;                                
         insert objTS_calendar;
         
          system.debug('%%%%%%%%%%%%%%Timesheet_Calendar_Val'+Timesheet_Calendar_Val);
          String Withdrawn='WithDrawn'; 
          String query = 'SELECT ID,Timesheet_Calendar__c,OwnerID,New_Owner__c,Approved_by__c,People__c,People__r.Name, Timesheet_Calendar__r.Name FROM Timesheet_Header__c where People__r.Timesheet_Required__c=true AND People__r.Empl_Status_Desc__c!=:Withdrawn  AND People__r.Time_Sheet_Override_Flag__c=false AND Timesheet_Calendar__c=:Timesheet_Calendar_Val';
          system.debug('%%%%%%%%%%%%%%query'+query);
          return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Timesheet_Header__c> scope)
    {
          date dv = system.today()+1;
          date dv2 = system.today()+7;
          string Timesheet_Calendar_Val;

          Timesheet_Calendar__c listTC = null;
          List<Timesheet_Calendar__c> resultTC = [select id,End_Date__c,Start_Date__c from Timesheet_Calendar__c where (Start_Date__c <= :dv and End_Date__c >= :dv)];
          if(resultTC.size() > 0)
          {
            Timesheet_Calendar_Val= resultTC[0].id;
          }

         //system.debug('%%%%%%%%%%%%%%objTS_calendar'+objTS_calendar.ID);    
         List<Timesheet_Header__c> newFEMs = new List<Timesheet_Header__c>();
         List<Timesheet__c> TDList   = new List<Timesheet__c>();
         Set<id> oldId = new set<id>();
         for(Timesheet_Header__c a : scope)
         {  
            Timesheet_Header__c newFEM = a.clone(false, true, false, false);
            newFEM.Timesheet_Calendar__c=Timesheet_Calendar_Val;   
            newFEM.Old_Record_ID__c=a.ID;
            newFEM.Name=String.valueOf(a.People__r.Name) + ' -' +dv2.month()+'/'+dv2.day()+'/'+dv2.year();
            newFEM.OwnerID = a.OwnerID;
            newFEM.Status__c='Save'; 
            oldId.add(newFEM.Old_Record_ID__c); 
            newFEMs.add(newFEM);
        }
        
        if(newFEMs.size() > 0)
         {
             system.debug('%%%%%%%%%%%%%%newFEMs'+newFEMs.size());    
             insert newFEMs;
         }
         
        //List<Timesheet__c> TDList   = new List<Timesheet__c>();
         
        map<String,id> mapThlist = new map<String,id>();
        
        for (timesheet_header__c a : newFEMs) 
        {
            mapThlist.put(a.Old_Record_ID__c,a.id);
        }
        
        List<Timesheet__c> timesheetList = [select id,Portfolio__c,Timesheet_Header__c, name,Mon__c,Tue__c,Wed__c,Thu__c,Fri__c,Sat__c,Sun__c,Task__c, Resource_Projects__c from Timesheet__c where Timesheet_Header__c in :mapThlist.keyset()];
        
        //iterate each old timesheet record. Create a new one and update with the new Timesheet_Header__c.id field
        if (timesheetList.size() > 0) 
        {
             List<Timesheet__c> newTDList = new  List<Timesheet__c>();
            for (Timesheet__c oldTD : timesheetList) 
            {
                system.debug('*** sfdc *** ' + oldTd);
                Timesheet__c newTS = oldTD.clone(false, true, false, false);
                newTS.Timesheet_Header__c = mapThlist.get(oldTD.Timesheet_Header__c);
                newTDList.add(newTS);
            }
            
            
            if(newTDList.size() > 0)
            {
             system.debug('%%%%%%%%%%%%%%TDList'+TDList);  

             insert newTDList;
            }
        }
   
         
 
         
    }   
    
    global void finish(Database.BatchableContext BC)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {'mahendran_sundarraj@cable.comcast.com','Venkat_Sakhamuri@cable.comcast.com'});
        mail.setReplyTo('mahendran_sundarraj@cable.comcast.com');
        mail.setSenderDisplayName('Batch Processing');
        mail.setSubject('Copy TS Header - Batch Process Completed');
        mail.setPlainTextBody('Batch Process has completed');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}