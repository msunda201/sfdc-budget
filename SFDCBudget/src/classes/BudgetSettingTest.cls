@isTest
private class BudgetSettingTest {

                

               
               static Testmethod void updateRateTest1() 
                {
                
                                Budget_Setting__c objBudSetting = new Budget_Setting__c();
                                objBudSetting.Name = 'Budget Setting 2014';
                                objBudSetting.Budget_Planning_Year__c = '2014';
                                objBudSetting.Active__c = TRUE;
                                insert objBudSetting;
                                
                                 Budget_Category__c objBudCat = new Budget_Category__c();
                                objBudCat.Name = 'Capex - External Cap Labor [A18674]';
                                objBudCat.Budget_Category_Prefix__c='CECL';
                                objBudCat.Apply_Net_Terms__c=True;                                
                                objBudCat.Capex_Opex__c='capex';
                                objBudCat.Category_Type__c='Capex Third Party Labor';
                                objBudCat.Display_Flag__c=False;
                                objBudCat.Finance_Adjustment__c=True;
                                insert objBudCat;
                                
                                Budget_Category__c objBudCat1 = new Budget_Category__c();
                                objBudCat1.Name = 'Capex - Internal Cap Labor [A18673]';
                                objBudCat1.Budget_Category_Prefix__c='CICL';
                                objBudCat1.Apply_Net_Terms__c=True;                                
                                objBudCat1.Capex_Opex__c='capex';
                                objBudCat1.Category_Type__c='Capex Internal Labor';
                                objBudCat1.Display_Flag__c=False;
                                objBudCat1.Finance_Adjustment__c=True;
                                insert objBudCat1;
                                
                                Budget_Category__c objBudCat2 = new Budget_Category__c();
                                objBudCat2.Name = 'Opex - Internal Labor [A41105]';
                                objBudCat2.Budget_Category_Prefix__c='OIL';
                                objBudCat2.Apply_Net_Terms__c=True;                                
                                objBudCat2.Capex_Opex__c='opex';
                                objBudCat2.Category_Type__c='Opex Internal Labor';
                                objBudCat2.Display_Flag__c=False;
                                objBudCat2.Finance_Adjustment__c=True;
                                insert objBudCat2;
                                
                                Budget_Category__c objBudCat3 = new Budget_Category__c();
                                objBudCat3.Name = 'Opex - External Labor [A18674]';
                                objBudCat3.Budget_Category_Prefix__c='OEL';
                                objBudCat3.Apply_Net_Terms__c=True;                                
                                objBudCat3.Capex_Opex__c='opex';
                                objBudCat3.Category_Type__c='Opex External Labor';
                                objBudCat3.Display_Flag__c=False;
                                objBudCat3.Finance_Adjustment__c=True;
                                insert objBudCat3;

                                
                                Funding_Entity__c objFundEntity = new Funding_Entity__c();
                                objFundEntity.Name = 'Octo Group';
                                objFundEntity.Funding_Entity_ID__c = 'FOCT100';
                                insert objFundEntity;
                
                                Entity__c objEntity = new Entity__c();
                                objEntity.Name = 'Test Entity';
                                objEntity.POR__c=TRUE;
                                objEntity.Capex_All_Other__c='Project Funded';
                                objEntity.Capex_Internal_Labor__c='Project Funded';
                                objEntity.Capex_Third_Party_Labor__c='Project Funded';
                                objEntity.Opex_All_Other__c='Project Funded';
                                objEntity.Opex_External_Labor__c='Project Funded';
                                objEntity.Opex_Internal_Labor__c='Project Funded';
                                insert objEntity;
                                
                                Director_Group__c objDirGroup = new Director_Group__c();
                                objDirGroup.Name = 'Test DirGroup';
                                objDirGroup.Entity__c = objEntity.id;
                                insert objDirGroup;
              
                                Clarity__c objClarity = new Clarity__c();
                                objClarity.Entity__c = objEntity.id;
                                insert objClarity;
                                
                                Rate__c objRate = new Rate__c();
                                objRate.Director_Group__c = objDirGroup.id;
                                objRate.EXTERNAL_LABOR_RATE_OFFSHORE__c  = 40;
                                objRate.EXTERNAL_LABOR_RATE_ONSHORE__c = 50;
                                objRate.INTERNAL_LABOR_RATE__c = 60;
                                objRate.Year__c = '2013';
                                insert objRate;
                                
                                 Rate__c objRate1 = new Rate__c();
                                objRate1.Director_Group__c = objDirGroup.id;
                                objRate1.EXTERNAL_LABOR_RATE_OFFSHORE__c  = 40;
                                objRate1.EXTERNAL_LABOR_RATE_ONSHORE__c = 50;
                                objRate1.INTERNAL_LABOR_RATE__c = 60;
                                objRate1.Year__c = '2014';
                                insert objRate1;
                                
                                Vendor2014__c vcObj = new Vendor2014__c();
                                vcObj.Name='Euler';
                                vcObj.Payment_Terms__c='Net 90';
                                insert vcObj;
 
                                                               
                                Projects_2014__c objProject =new Projects_2014__c();
                                objProject.Clarity__c = objClarity.id;
                                objProject.Budget_Setting__c=objBudSetting.id;
                                objProject.Lock_Project__c = FALSE;
                                objProject.Start_Date__c = system.today();
                                objProject.End_Date__c = system.today();
                                upsert objProject;
                                
                                Budget_Setting__c objBudSetting1 = new Budget_Setting__c();
                                objBudSetting1.ID = objBudSetting.ID ;
                                objBudSetting1.Active__c = FALSE;
                                objBudSetting1.Lock_All_Projects__c = FALSE;
                                update objBudSetting1;

               }

               
}