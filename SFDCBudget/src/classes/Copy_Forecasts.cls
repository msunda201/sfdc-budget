global class Copy_Forecasts implements Database.Batchable<sObject>
{
    
    String email;
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
          date dv = system.today();
          string Forecast_Calendar_Val;

          Forecast_Calendar__c listFC = null;
          List<Forecast_Calendar__c> resultFC = [select id,Name from Forecast_Calendar__c where (Forecast_Start_Date__c <= :dv and Forecast_End_Date__c >= :dv)];
      
          if(resultFC.size() > 0)
          {
            Forecast_Calendar_Val= resultFC[0].id;
          }
    
    
        String query = 'SELECT Name,Forecast_Entry_Form__c,Project_ID__c,Forecast_Calendar__c,Account_ID__c,Clarity_ID__c,Effort_Entity__c,Funding_Entity__c,Budget_Category_Type__c,Forecast_Funding_Type__c,Budget_Amount__c,Month__c,Year__c FROM Forecasts__c where Forecast_Copy__c=TRUE AND Forecast_Calendar__c=:Forecast_Calendar_Val';
        return Database.getQueryLocator(query);
    }
   

    global void execute(Database.BatchableContext BC, List<Forecasts__c> scope)
    {
     
         List<Financial_Report__c> newFEMs = new List<Financial_Report__c>();
         RecordType rt = [select id from RecordType where developername = 'Forecast'];
         
         system.debug('%%%%%%%%%%%%%%SCOPE'+scope);
         
         for(Forecasts__c a : scope)
         {  
            newFEMs.add(new Financial_Report__c(
                            Type__c='Forecast',
                            Report_Sub_Type__c='Forecast',
                            RecordTypeId=rt.id,
                            Forecast_Entry_Form__c=a.Forecast_Entry_Form__c,
                            Project_ID__c=a.Project_ID__c,
                            Forecast_Calendar__c= a.Forecast_Calendar__c,
                            Account_ID__c= a.Account_ID__c,
                            Clarity_ID__c=a.Clarity_ID__c,
                            Effort_Entity__c=a.Effort_Entity__c,
                            Budget_Category_Type__c=a.Budget_Category_Type__c,
                            Forecast_Funding_Type__c=a.Forecast_Funding_Type__c,
                            Funding_Entity__c= a.Funding_Entity__c,
                            Budget_Amount__c = a.Budget_Amount__c ,
                            Month__c=a.Month__c,
                            Year__c=a.Year__c
                            ));  
         }
          system.debug('%%%%%%%%%%%%%%SIZE'+newFEMs.size());
         if(newFEMs.size() > 0)
         {
        
         insert newFEMs;
         }
    }   
    
    
    global void finish(Database.BatchableContext BC)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {'mahendran_sundarraj@cable.comcast.com'});
        mail.setReplyTo('mahendran_sundarraj@cable.comcast.com');
        mail.setSenderDisplayName('Forecast Batch Processing');
        mail.setSubject('Copy Forecast Entry Forms - Batch Process Completed');
        mail.setPlainTextBody('Forecast Batch Process has completed');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}