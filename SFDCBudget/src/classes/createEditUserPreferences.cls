Public Class createEditUserPreferences{

    public User_Preference__c objBudEntry{get;set;}

    public createEditUserPreferences(ApexPages.StandardController controller) 
    {
        objBudEntry=new User_Preference__c();
        objBudEntry=(User_Preference__c)controller.getrecord();
    }
     
    public string currentEntity { get; set; }
    public string currentDirector {get;set;}    
    public createEditUserPreferences()
    {  
        currentEntity = currentDirector= null;
    }
    
    // saveBudgetEntry method for save BudgetEntry
    
    public PageReference saveBudgetEntry() 
    {
       try
       {
            upsert objBudEntry;
            PageReference p = new PageReference('/'+objBudEntry.id);
            return p;    
       }
       catch(Exception e)
       {
             //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
             //ApexPages.addMessage(myMsg);
             ApexPages.addMessages(e);
           
             return null;
       }
    }
    
    //  getListEntity method for get List of Entity from Entity__c object
    
    public List<SelectOption> getListEntity(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        
        string entitystring='Select ID,Name from Entity__c where Active__c=True order by Name';
        List<Entity__c> lstEnt = Database.query(entitystring);
        for(Integer i=0; i<lstEnt.size(); i++) 
        {
             options.add(new SelectOption(lstEnt.get(i).ID,lstEnt.get(i).Name));
        }
        return options;
    }
    
    // getListDirector method for get List of Director from Director_Group__c
    
    public List<SelectOption> getListDirector() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        
        //if(String.IsBlank(objBudEntry.Entity__c)==true)
          //  return options;
        for(Director_Group__c p2:[select ID,Name from Director_Group__c where Entity__c = :objBudEntry.Entity__c  order by Name]) {
            options.add(new SelectOption(p2.ID,p2.Name));
        }
        return options;
    }
    //////////////////////////////////////////////////
    static testMethod void testPageMethods() 
    {          
       Entity__c objEntity = new Entity__c();
       objEntity.Name = 'Test Entity';
       objEntity.POR__c=TRUE;
       objEntity.Capex_All_Other__c='Project Funded';
       objEntity.Capex_Internal_Labor__c='Project Funded';
       objEntity.Capex_Third_Party_Labor__c='Project Funded';
       objEntity.Opex_All_Other__c='Project Funded';
       objEntity.Opex_External_Labor__c='Project Funded';
       objEntity.Opex_Internal_Labor__c='Project Funded';
       insert objEntity;
                                
       Director_Group__c objDirGroup = new Director_Group__c();
       objDirGroup.Name = 'Test DirGroup';
       objDirGroup.Entity__c = objEntity.id;
       insert objDirGroup;
                
       User_Preference__c objBudEntry =new User_Preference__c();
       objBudEntry.User__c = UserInfo.getUserId();
       objBudEntry.Entity__c = objEntity.id;
       objBudEntry.Director_Group__c = objDirGroup.id;
       insert objBudEntry;                      
        
       ApexPages.StandardController stdCtrlBudEntry = new ApexPages.StandardController(objBudEntry); 
       createEditUserPreferences objCreateBudEntry = new createEditUserPreferences(stdCtrlBudEntry);
        
        objCreateBudEntry.currentEntity = objEntity.id;
        objCreateBudEntry.currentDirector =objDirGroup.id ;
        objCreateBudEntry.getListEntity();
        objCreateBudEntry.getListDirector();
        objCreateBudEntry.saveBudgetEntry();
   
              

     }
      
    //////////////////////////////////////////////////
}