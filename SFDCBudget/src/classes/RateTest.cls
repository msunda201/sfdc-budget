@isTest
private class RateTest {
 
                static Testmethod void updateRateTest() 
                {
                
                                Budget_Setting__c objBudSetting = new Budget_Setting__c();
                                objBudSetting.Name = 'Budget Setting 2015';
                                objBudSetting.Budget_Planning_Year__c = '2015';
                                objBudSetting.Active__c = TRUE;
                                insert objBudSetting;

                                Budget_Category__c objBudCat = new Budget_Category__c();
                                objBudCat.Name = 'Capex - External Cap Labor [A18674]';
                                objBudCat.Budget_Category_Prefix__c='CECL';
                                objBudCat.Apply_Net_Terms__c=True;                                
                                objBudCat.Capex_Opex__c='capex';
                                objBudCat.Category_Type__c='Capex Third Party Labor';
                                objBudCat.Display_Flag__c=False;
                                objBudCat.Finance_Adjustment__c=True;
                                insert objBudCat;
                                
                                Budget_Category__c objBudCat1 = new Budget_Category__c();
                                objBudCat1.Name = 'Capex - Internal Cap Labor [A18673]';
                                objBudCat1.Budget_Category_Prefix__c='CICL';
                                objBudCat1.Apply_Net_Terms__c=True;                                
                                objBudCat1.Capex_Opex__c='capex';
                                objBudCat1.Category_Type__c='Capex Internal Labor';
                                objBudCat1.Display_Flag__c=False;
                                objBudCat1.Finance_Adjustment__c=True;
                                insert objBudCat1;
                                
                                Budget_Category__c objBudCat2 = new Budget_Category__c();
                                objBudCat2.Name = 'Opex - Internal Labor [A41105]';
                                objBudCat2.Budget_Category_Prefix__c='OIL';
                                objBudCat2.Apply_Net_Terms__c=True;                                
                                objBudCat2.Capex_Opex__c='opex';
                                objBudCat2.Category_Type__c='Opex Internal Labor';
                                objBudCat2.Display_Flag__c=False;
                                objBudCat2.Finance_Adjustment__c=True;
                                insert objBudCat2;
                                
                            
                
                                Budget_Category__c objBudCat3 = new Budget_Category__c();
                                objBudCat3.Name = 'Opex - External Labor [A18674]';
                                objBudCat3.Budget_Category_Prefix__c='OEL';
                                objBudCat3.Apply_Net_Terms__c=True;                                
                                objBudCat3.Capex_Opex__c='opex';
                                objBudCat3.Category_Type__c='Opex External Labor';
                                objBudCat3.Display_Flag__c=False;
                                objBudCat3.Finance_Adjustment__c=True;
                                insert objBudCat3;

                
                                
                                Funding_Entity__c objFundEntity = new Funding_Entity__c();
                                objFundEntity.Name = 'Octo Group';
                                objFundEntity.Funding_Entity_ID__c = 'FOCT100';
                                insert objFundEntity;
                
                                Entity__c objEntity = new Entity__c();
                                objEntity.Name = 'Test Entity';
                                objEntity.POR__c=TRUE;
                                objEntity.Capex_All_Other__c='Project Funded';
                                objEntity.Capex_Internal_Labor__c='Project Funded';
                                objEntity.Capex_Third_Party_Labor__c='Project Funded';
                                objEntity.Opex_All_Other__c='Project Funded';
                                objEntity.Opex_External_Labor__c='Project Funded';
                                objEntity.Opex_Internal_Labor__c='Project Funded';
                                insert objEntity;
                                
                                Director_Group__c objDirGroup = new Director_Group__c();
                                objDirGroup.Name = 'Test DirGroup';
                                objDirGroup.Entity__c = objEntity.id;
                                insert objDirGroup;
              
                                Clarity__c objClarity = new Clarity__c();
                                objClarity.Entity__c = objEntity.id;
                                insert objClarity;
                                
                                Rate__c objRate = new Rate__c();
                                objRate.Director_Group__c = objDirGroup.id;
                                objRate.EXTERNAL_LABOR_RATE_OFFSHORE__c  = 40;
                                objRate.EXTERNAL_LABOR_RATE_ONSHORE__c = 50;
                                objRate.INTERNAL_LABOR_RATE__c = 60;
                                objRate.Year__c = '2014';
                                insert objRate;

                                Rate__c objRate2 = new Rate__c();    
                                objRate2.Director_Group__c = objDirGroup.id;
                                objRate2.EXTERNAL_LABOR_RATE_OFFSHORE__c  = 40;
                                objRate2.EXTERNAL_LABOR_RATE_ONSHORE__c = 50;
                                objRate2.INTERNAL_LABOR_RATE__c = 60;
                                objRate2.Year__c = '2015';
                                insert objRate2;
                                
                                
                                Vendor2014__c vcObj = new Vendor2014__c();
                                vcObj.Name='Euler';
                                vcObj.Payment_Terms__c='Net 90';
                                insert vcObj;
                
                                Projects_2014__c objProject =new Projects_2014__c();
                                objProject.Clarity__c = objClarity.id;
                                objProject.Budget_Setting__c=objBudSetting.id;
                                objProject.Lock_Project__c = FALSE;
                                objProject.Start_Date__c = system.today();
                                objProject.End_Date__c = system.today();
                                insert objProject;
                                
                                Capability__c cObj = new Capability__c();
                                cObj.Name='QA';
                                cObj.Project__c=objProject.id;
                                insert cObj;
                                 
                                Budget_Entry_Form__c objBudEntry = new Budget_Entry_Form__c();
                                objBudEntry.Project__c =objProject.id;
                                objBudEntry.Capability__c =cObj.id;
                                objBudEntry.Director_Group__c = objDirGroup.id;
                               // objBudEntry.Director_Group__r.INTERNAL_LABOR_RATE__c=objDirGroup.id;
                                objBudEntry.Effort_Entity__c = objEntity.id;
                                //objBudEntry.Funding_Entity__c = objFundEntity.id;
                                objBudEntry.Budget_Type__c='Labor (By Effort)';
                                objBudEntry.Total_Effort_Hours__c=4000;
                                objBudEntry.External_Labor__c =60;
                                objBudEntry.End_Date__c = system.today()+50;
                                objBudEntry.Start_Date__c = system.today()-100;
                                objBudEntry.Off_Shore_Labor__c =40;        
                                objBudEntry.Capital_Expense__c =50;
                                objBudEntry.Vendor2014__c=vcObj.id;
                                objBudEntry.Service_Type__c='Staff Augumentation';
                                objBudEntry.Estimated_head_count__c=5;
                                insert objBudEntry;
                                
                                 Budget_2014__c objbdgt= new Budget_2014__c();
                                 objbdgt.Budget__c=objBudEntry.Id;
                                 objbdgt.Rate_ID__c=objRate.id;
                                 objbdgt.Budget_Category__c=objBudCat.id;
                                 insert objbdgt;   
                                 
                                 
                                 Rate__c objRate4 = new Rate__c();
                                objRate4.id = objRate.id;
                                objRate4.Director_Group__c = objDirGroup.id;
                                objRate4.EXTERNAL_LABOR_RATE_OFFSHORE__c  = 40;
                                objRate4.EXTERNAL_LABOR_RATE_ONSHORE__c = 50;
                                objRate4.INTERNAL_LABOR_RATE__c = 60;
                                objRate4.Year__c = '2014';
                                update objRate4;
                                 /***
                                 List<Budget_2014__c> budgets =[select id,Budget_Category_Prefix__c,Budget_Setting_Status__c,Account_ID__c,Off_Shore_Labor__c,Effort_Projection__c,Spent_Projection__c,EXTERNAL_LABOR_RATE_OFFSHORE__c,EXTERNAL_LABOR_RATE_ONSHORE__c,INTERNAL_LABOR_RATE__c from Budget_2014__c where Rate_ID__c =:objRate.id];
                        
                        if(budgets.size() > 0)
                        {
                            for(Budget_2014__c bdgt : budgets)
                            {
                                if(bdgt.Effort_Projection__c>0)
                                {
                                    if(bdgt.Budget_Category_Prefix__c=='CICL' || bdgt.Account_ID__c=='OIL')
                                    {
                                        bdgt.Spent_Projection__c      = 100*bdgt.Effort_Projection__c;
                                    }
                                    else
                                    {
                                        if(bdgt.Off_Shore_Labor__c!=null)
                                        {
                                            bdgt.Spent_Projection__c     =((100*(1-(bdgt.Off_Shore_Labor__c/100)))+ (100*(bdgt.Off_Shore_Labor__c/100)))*bdgt.Effort_Projection__c;
                                        }
                                    }
                                }
                                
                                //system.debug('### inside get listing bdgt.Spent_Projection__c-->'+bdgt.Spent_Projection__c);
                                
                                //bdgt.EXTERNAL_LABOR_RATE_OFFSHORE__c    = newRateEntry.EXTERNAL_LABOR_RATE_OFFSHORE__c;
                                //bdgt.EXTERNAL_LABOR_RATE_ONSHORE__c     = newRateEntry.EXTERNAL_LABOR_RATE_ONSHORE__c;
                                //bdgt.INTERNAL_LABOR_RATE__c             = newRateEntry.INTERNAL_LABOR_RATE__c;
                            
                            }
                 update budgets; 
                          
                          }
                            ****/
                           
                }
                 
}