@isTest
public class TestSendEmailConroller{
    @isTest
    private static void test(){
        People__c people2 = new People__c(Name='Test People');
        insert people2;
        
        People__c people = new People__c(Name='Test People',Manager_c__c=people2.Id);
        insert people;
        
        Timesheet_Calendar__c tc = new Timesheet_Calendar__c(Start_Date__c=system.today(),End_Date__c=system.today().addDays(2));
        insert tc;
        
        Timesheet_Header__c th = new Timesheet_Header__c(Name='Test TH',Timesheet_Calendar__c=tc.Id,People__c=people.Id,Status__c='New');
        insert th;
        
        SendEmailConroller.sendEmail(new Set<Id>{people.Id,userinfo.getUserId()});
    }
}