Public Class createEditForecastEntryController{
    public Forecast_Entry_Form__c objBudEntry{get;set;}
    private String DateStr; //variable to hold the target date
     public CommonFunctions common = null;
   

    public createEditForecastEntryController(ApexPages.StandardController controller){
        objBudEntry=new Forecast_Entry_Form__c();
        objBudEntry=(Forecast_Entry_Form__c)controller.getrecord();
        common = CommonFunctions.getInstance(controller);
        //Prod
        String strProjectId = ApexPages.currentPage().getParameters().get('CF00NJ0000000x8dS_lkid');
        //Sandbox
        //String strProjectId = ApexPages.currentPage().getParameters().get('CF00NU0000003KtSw_lkid');
        
       
        
        string strProject = ApexPages.currentPage().getParameters().get('Project_Name__c');    
        string strId = ApexPages.currentPage().getParameters().get('id');    
        
        if(strId==null)
        {
            User current_user=[SELECT id FROM User WHERE Id= :UserInfo.getUserId()] ;
            List<User_Preference__c> lstUserPref =[select Entity_ID__c,Entity__c,Director_Group__c from User_Preference__c where User__c = :UserInfo.getUserId()];
            if(lstUserPref.size()>0)
            {
                objBudEntry.Effort_Entity__c=lstUserPref[0].Entity_ID__c;
                objBudEntry.Director_Group__c=lstUserPref[0].Director_Group__c;
            }
            
            //Set Default Vendor
        //if(objBudEntry.Vendor__c!=null)
        //{
             string strVendorName='TBD';
             List<Vendor2014__c> lstVendor =[select ID from Vendor2014__c where Name = :strVendorName];
            if(lstVendor.size()>0)
             {
                 objBudEntry.Vendor__c = lstVendor[0].ID;
             }
            system.debug('%%%%%%%%%%%%%%Vendor__c'+objBudEntry.Vendor__c);
        //}
        }
        
       
        
         
        objBudEntry.Clone__c=FALSE;
        
        if(ApexPages.currentPage().getParameters().get('clone')!=NULL || objBudEntry.Forecast_Job__c==TRUE)
        {
             objBudEntry.Clone__c=TRUE;
        }     
        //if(strProjectId !=null && strProjectId!='CF00NK0000000mtdc_lkid')
        
        date dv = system.today();

        Forecast_Calendar__c listFC = null;
        List<Forecast_Calendar__c> resultFC = [select id,Name from Forecast_Calendar__c where (Forecast_Start_Date__c <= :dv and Forecast_End_Date__c >= :dv)];
      
        //if(resultFC.size() > 0 )
        if((resultFC.size() > 0 ) && (objBudEntry.ID==NULL) && !(objBudEntry.Forecast_Type__c =='Finance Adjustment' ||  objBudEntry.Forecast_Type__c == 'Multiple Month by Account') )
        {
            //System.debug('Inside if condition -> Not Finance Adjustment or Multiplemonth by Account-> ' + objBudEntry.Forecast_Type__c);
            objBudEntry.Forecast_Calendar__c= resultFC[0].id;
        }
        
        if(strProjectId !=null)
        {
     
            List<Projects_2014__c> lstProject =[select id,Name,Project_Type__c,Funded_by__c,Clarity__c,Funding_Entity_ID__c,Start_Date__c,End_Date__c from Projects_2014__c where id = :strProjectId];
            system.debug(lstProject);
            if(lstProject.size()>0)
            {
                objBudEntry.Project_Name__c= lstProject[0].ID;
                 system.debug('%%%%%%%%%%%%%%LIST PROJECT'+lstProject[0].ID);
                objBudEntry.Start_Date__c = null;
               
                objBudEntry.End_Date__c = null;
                objBudEntry.Invoice_Date__c = lstProject[0].Start_Date__c;
                if(lstProject[0].Project_Type__c=='POR')
                {
                    objBudEntry.Funding_Entity__c = lstProject[0].Funding_Entity_ID__c;
                }
                else
                {
                    List<Funding_Entity__c> lstFunding =[select id from Funding_Entity__c where Name = :lstProject[0].Funded_by__c];
                    if(lstFunding.size()>0)
                    {
                      objBudEntry.Funding_Entity__c = lstFunding[0].id;             
                    }
                }
            }
           
            
        }
    }
    
    public string currentEntity { get; set; }
    public string currentDirector {get;set;}
    
    public Decimal EXTERNAL_LABOR_RATE_OFFSHORE {get;set;}
    public Decimal EXTERNAL_LABOR_RATE_ONSHORE {get;set;}
    public Decimal INTERNAL_LABOR_RATE {get;set;}
    public Date CDate {get;set;}
    public integer CYear {get;set;}
    public integer CYear1 {get;set;}
    public integer CYear2 {get;set;}
    /*// Constructor method
    
    public createEditBudgetEntryController(){
    
       
        currentEntity = currentDirector= null;
    }*/
    
    // saveBudgetEntry method for save BudgetEntry
    
    public PageReference saveBudgetEntry() 
    {


        try{
            
            if(objBudEntry.Forecast_Type__c =='Labor (By Effort)')
            {
                RecordType rt = [select id from RecordType where developername = 'Forecast_Labor_By_Effort'];
                objBudEntry.RecordTypeId=rt.id;
            }
            else if(objBudEntry.Forecast_Type__c =='Labor (Lump Sum $)')
            {
                RecordType rt = [select id from RecordType where developername = 'Forecast_Labor_Lump_Sum'];
                objBudEntry.RecordTypeId=rt.id;
            }
            else if(objBudEntry.Forecast_Type__c =='Other Costs (Monthly)')
            {
                RecordType rt = [select id from RecordType where developername = 'Forecast_Other_Costs_Monthly'];
                objBudEntry.RecordTypeId=rt.id;
            }
            else if(objBudEntry.Forecast_Type__c =='Other Costs (Single Payment)')
            {
                RecordType rt = [select id from RecordType where developername = 'Forecast_Other_Costs_Single_Payment'];
                objBudEntry.RecordTypeId=rt.id;
            }
            else if(objBudEntry.Forecast_Type__c =='Software (Perpetual)')
            {
                RecordType rt = [select id from RecordType where developername = 'Forecast_Software_Perpetual'];
                objBudEntry.RecordTypeId=rt.id;
            }
            else if(objBudEntry.Forecast_Type__c == 'Multiple Month by Account')
            {
                RecordType rt = [select id from RecordType where developername = 'Multiple_Month_by_Account'];
                objBudEntry.RecordTypeId=rt.id;
            }
            else
            {
            RecordType rt = [select id from RecordType where developername = 'Forecast_Finance_Adjustment'];
            objBudEntry.RecordTypeId=rt.id;
            }
         /////////////////////////////////////////////   
         
         /*if(objBudEntry.Forecast_Type__c !='Finance Adjustment')
         {
             if(objBudEntry.Effort_Entity__c==null)
             {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Choose Effort Entity');
                    ApexPages.addMessage(myMsg);
                    return null;
             }
          }*/
          
        
        
            
            
            
      //////////////////////////////////// 
      //if(objBudEntry.Forecast_Type__c != 'Finance Adjustment')
      //system.debug('###Forecast_Calendar__c Setting1-->'+objBudEntry.Forecast_Calendar__c);
      
      if(((objBudEntry.id==NULL)||(objBudEntry.Clone__c==TRUE))&&(objBudEntry.Forecast_Type__c =='Software (Perpetual)' || objBudEntry.Forecast_Type__c =='Other Costs (Single Payment)' || objBudEntry.Forecast_Type__c == 'Other Costs (Monthly)' || objBudEntry.Forecast_Type__c =='Labor (By Effort)' || objBudEntry.Forecast_Type__c =='Labor (Lump Sum $)'))        
      {
     
          date dv = system.today();

          Forecast_Calendar__c listFC = null;
          List<Forecast_Calendar__c> resultFC = [select id,Name from Forecast_Calendar__c where (Forecast_Start_Date__c <= :dv and Forecast_End_Date__c >= :dv)];
      
          if(resultFC.size() > 0)
          {
            objBudEntry.Forecast_Calendar__c= resultFC[0].id;
          }
         
      }
       //system.debug('###Forecast_Calendar__c Setting2-->'+objBudEntry.Forecast_Calendar__c);
      //////////////////////////////////////

        
        
        ////////////////////////////////////
        //Mahendran updated - 01/28/2014
        
        
                objBudEntry.Forecast_Flag__c=TRUE;
                //User current_user=[SELECT Profile_Name__c,id,User_Role_ID__c FROM User WHERE Id= :UserInfo.getUserId()] ;
                
                if(objBudEntry.Clone__c==TRUE)
                {
                    //objBudEntry.Forecast_Created_User__c=current_user.id;
                    objBudEntry.id=NULL;
                    objBudEntry.Clone__c=FALSE;
                    insert objBudEntry;
                }
                else
                {
                    upsert objBudEntry;
                }
        
        
        PageReference p = new PageReference('/'+objBudEntry.id);
        return p;
       
       }
       catch(Exception e)
       {
             //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'The Forcast Entry Form is Locked');
             //ApexPages.addMessage(myMsg);
             ApexPages.addMessages(e);
           
             return null;
       }
        
        
        
    }
    
    
    
    
    //  getListEntity method for get List of Entity from Entity__c object
    
    public List<SelectOption> getListEntity(){
        List<SelectOption> options = new List<SelectOption>();
        options= common.getCommonListEntity();
        return options;
        
    }
    
    // getListDirector method for get List of Director from Director_Group__c
    
    public List<SelectOption> getListDirector() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        if(String.IsBlank(objBudEntry.Effort_Entity__c)==true)
            return options;
        for(Director_Group__c p2:[select ID,Name from Director_Group__c where Entity__c = :objBudEntry.Effort_Entity__c  order by Name]) {
            options.add(new SelectOption(p2.ID,p2.Name));
        }
        return options;
    }
    
    /////////////////////////////////////////////
    public List<SelectOption> getListVendor() {
        
       List<SelectOption> options = new List<SelectOption>();
        options= common.getListVendor();
        return options;
        
    }
    
    /////////////////////////////////////////////
    public List<SelectOption> getListBudgetCategory() {
       
     /*  List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        system.debug('###objBudEntry.Forecast_Type__c-->'+objBudEntry.Forecast_Type__c);
         if((objBudEntry.id==NULL)&&( objBudEntry.Forecast_Type__c =='Other Costs (Single Payment)' || objBudEntry.Forecast_Type__c == 'Other Costs (Monthly)'))   {
                        system.debug('### inside other costs ->'+objBudEntry.Forecast_Type__c);
                        for(Budget_Category__c p2:[select ID,Name from Budget_Category__c where Display_Flag__c=True  and account_id__c not in ('A18673','A18674','A75025' ) order by Name]) {
                                options.add(new SelectOption(p2.ID,p2.Name));
                        }
         } else {
                system.debug('### inside else ->'+objBudEntry.Forecast_Type__c);
                for(Budget_Category__c p2:[select ID,Name from Budget_Category__c where Display_Flag__c=True order by Name]) {
                                options.add(new SelectOption(p2.ID,p2.Name));
                        }               
         }
        return options;*/
        List<SelectOption> options = new List<SelectOption>();
        options= common.getListAccountCategory();
        return options; 
    }
    ///////////////////////////////////////////////////////////////////
    public List<SelectOption> getListFunding() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        
       
       system.debug('###objBudEntry.Project_Name__c-->'+objBudEntry.Project_Name__c);
      if(objBudEntry.Funding_Entity__c==null)
      {
        if(String.IsBlank(objBudEntry.Project_Name__c)==true)
            return options;
                
            List<Projects_2014__c> lstProject =[select id,Project_Type__c,Funded_by__c,Clarity__c,Funding_Entity_ID__c,Start_Date__c,End_Date__c from Projects_2014__c where id = :objBudEntry.Project_Name__c];
            
            if(lstProject.size()>0)
            {
               if(lstProject[0].Project_Type__c=='POR')
                {
                    objBudEntry.Funding_Entity__c = lstProject[0].Funding_Entity_ID__c;
                }
                else
                {
                    List<Funding_Entity__c> lstFunding =[select id from Funding_Entity__c where Name = :lstProject[0].Funded_by__c];
                    if(lstFunding.size()>0)
                    {
                      objBudEntry.Funding_Entity__c = lstFunding[0].id;             
                    }
                }
            }    
         }     
     
        for(Funding_Entity__c p2:[select ID,Name from Funding_Entity__c  order by Name]) {
            options.add(new SelectOption(p2.ID,p2.Name));
        }
        return options;
        
      
    }

    
    public List<SelectOption> getListCapability() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        
        
        if(String.IsBlank(objBudEntry.Project_Name__c )==true)
            return options;
        for(Capability__c p2:[select ID,Name from Capability__c where Project__c = :objBudEntry.Project_Name__c  order by Name]) {
            String nameStr = p2.Name;
            if (nameStr.length() > 50) {
            nameStr = p2.Name.substring(0,50);
            } else {
                nameStr = p2.Name;
            }
            options.add(new SelectOption(p2.ID,nameStr));
        }
        return options;
    }
   
    
  
       
    //////////////////////////////////////////////////
}