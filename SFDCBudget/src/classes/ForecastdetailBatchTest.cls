@isTest
public class ForecastdetailBatchTest {
    static testMethod void test() {
        Database.QueryLocator QL;
        List<Forecasts__c >  ff =  [Select ID,Forecast_Entry_Form__c,Project_ID__c,Forecast_Calendar__c,Account_ID__c,Clarity_ID__c,Effort_Entity__c,Funding_Entity__c,Budget_Category_Type__c,Forecast_Funding_Type__c,Budget_Amount__c,Month__c,Year__c,Name From Forecasts__c LIMIT 2];
  
        Database.BatchableContext BC;
        Copy_Forecasts  cc = New Copy_Forecasts ();
        cc.Start(BC);
        cc.execute(BC, ff );
        cc.Finish(BC); 
    }
   
}