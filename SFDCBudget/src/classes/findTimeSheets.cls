public class findTimeSheets {
    public String userId{set;get;}
    
    public List<PeopleModel> getPeoples(){
        List<PeopleModel> listMod = new List<PeopleModel>();
       List<People__c> listPeople = [select id,Name,Manager_User_ID__c,(select Id,Name,Status__c,Timesheet_Calendar__r.Name,Time_sheet_Status__c from Timesheet_Header__r where Status__c != 'Submit' AND Timesheet_Calendar__r.End_Date__c <= :system.today()) from People__c where Manager_User_ID__c =: userId];
    //     List<People__c> listPeople = [select id,Name,Manager_User_ID__c,(select Id,Name,Status__c from Timesheet_Header__r where Status__c != 'Submit') from People__c where Manager_User_ID__c =: userId];
        if(listPeople != null && listPeople.size() > 0){
            for(People__c people : listPeople){
                if(people.Timesheet_Header__r != null && people.Timesheet_Header__r.size() > 0){
                    for(Timesheet_Header__c th : people.Timesheet_Header__r){
                        PeopleModel mod = new PeopleModel();
                        mod.peopleName = people.Name;
                        mod.timesheetName = th.Timesheet_Calendar__r.Name;
                        mod.Status = th.Time_sheet_Status__c ;
                        
                        listMod.add(mod);
                    }
                }
            }
        }
        return listMod;
    }
    
    public class PeopleModel{
        public String peopleName{set;get;}
        public String timesheetName{set;get;}
        public String status{set;get;}
        
        public PeopleModel(){
            peopleName = '';
            timesheetName = '';
        }
    }
    /*
    private List<Timesheet_Header__c> Timesheets;
    
    public map<id,Timesheet__c> childMap;
    public map<string,string> mapDayToDate{get;set;}
    
    public findTimeSheets(){
        string Timesheet_Calendar_Val;
        Timesheets =  [SELECT Name, Timesheet_Calendar__c, Timesheet_Period__c, People__c, Manager__c,Status__c, Timesheet_Total_Hours__c,Ownerid FROM Timesheet_Header__c where Status__c!='Submit' OR OwnerID=:UserInfo.getUserId()];
        
    }

    public List<Timesheet_Header__c> getTimeSheets() {
        return TimeSheets;
    }
    */
}