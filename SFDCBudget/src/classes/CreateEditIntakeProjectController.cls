public class CreateEditIntakeProjectController {
        public Intake_Project__c objIntakeProj{get;set;}
        public CommonFunctions common = null;
        public CreateEditIntakeProjectController (Apexpages.Standardcontroller controller) {
                    //common = CommonFunctions.getInstance(controller);
                objIntakeProj = new Intake_Project__c();
                objIntakeProj = (Intake_Project__c) controller.getRecord();
                objIntakeProj.Clone__c=FALSE;
        
                        if(ApexPages.currentPage().getParameters().get('clone')!=NULL)
                        {
                        objIntakeProj.Clone__c=TRUE;
                        }
        }
        
        public Pagereference saveIntakeProjectEntry(){
                
         try{
                        if(objIntakeProj.Clone__c==TRUE)
                {
                    objIntakeProj.id=NULL;
                    objIntakeProj.Clone__c=FALSE;
                    insert objIntakeProj;
                }
                else
                {
                    upsert objIntakeProj;
                }
               
                        PageReference pgRef = new PageReference('/'+objIntakeProj.id);
                        system.debug('Page ref'+pgRef);
                        return pgRef;
                }
                catch(Exception e)
                {
                ApexPages.addMessages(e);
                return null;
             }
        }
        
        
            
        public List<SelectOption> getListFunding() {
       
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        for(Funding_Entity__c p2:[select ID,Name from Funding_Entity__c  order by Name]) {
            options.add(new SelectOption(p2.ID,p2.Name));
        }
        return options;
    }

}