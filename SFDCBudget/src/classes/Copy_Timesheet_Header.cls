global class Copy_Timesheet_Header implements Database.Batchable<sObject>
{
    
    String email;
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
          date dv = system.today()+1-3;
          string Timesheet_Calendar_Val;

          Timesheet_Calendar__c listTC = null;
          List<Timesheet_Calendar__c> resultTC = [select id,End_Date__c,Start_Date__c from Timesheet_Calendar__c where (Start_Date__c <= :dv and End_Date__c >= :dv)];
          if(resultTC.size() > 0)
          {
            Timesheet_Calendar_Val= resultTC[0].id;
          }
          
          String Withdrawn='WithDrawn'; 

          String query = 'SELECT ID,Old_Record_ID__c,Timesheet_Calendar__c,OwnerID,Approved_by__c,People__c,People__r.Name, Timesheet_Calendar__r.Name,Submitted_by__c FROM Timesheet_Header__c where People__r.Timesheet_Required__c=true AND People__r.Empl_Status_Desc__c!=:Withdrawn  AND People__r.Time_Sheet_Override_Flag__c=false AND Timesheet_Calendar__c=:Timesheet_Calendar_Val';

          system.debug('%%%%%%%%%%%%%%Timesheet_Calendar_Val'+Timesheet_Calendar_Val);
                    
          system.debug('%%%%%%%%%%%%%%query'+query);
          
          return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Timesheet_Header__c> scope)
    {

        List<Timesheet__c> TDList   = new List<Timesheet__c>();
         
        map<String,id> mapThlist = new map<String,id>();
        
        for (timesheet_header__c a : scope) 
        {
            mapThlist.put(a.Old_Record_ID__c,a.id);
        }
        
        List<Timesheet__c> timesheetList = [select id,Portfolio__c,Timesheet_Header__c, name,Mon__c,Tue__c,Wed__c,Thu__c,Fri__c,Sat__c,Sun__c,Time_sheet_Hours__c,Task__c, Resource_Projects__c from Timesheet__c where Timesheet_Header__c in :mapThlist.keyset()];
        
        //iterate each old timesheet record. Create a new one and update with the new Timesheet_Header__c.id field
        if (timesheetList.size() > 0) 
        {
             List<Timesheet__c> newTDList = new  List<Timesheet__c>();
            for (Timesheet__c oldTD : timesheetList) 
            {
                system.debug('*** sfdc *** ' + oldTd);
                Timesheet__c newTS = oldTD.clone(false, true, false, false);
                newTS.Timesheet_Header__c = mapThlist.get(oldTD.Timesheet_Header__c);
                newTDList.add(newTS);
            }
            
            
            if(newTDList.size() > 0)
            {
             system.debug('%%%%%%%%%%%%%%TDList'+TDList);  

             insert newTDList;
            }
        }

        
         
         
         
         
         /******************
         
         for(Timesheet_Header__c a : scope)
         {  

             Timesheet_Header__c newTH = [SELECT ID,(select id,Portfolio__c,Timesheet_Header__c, name,Mon__c,Tue__c,Wed__c,Thu__c,Fri__c,Sat__c,Sun__c,Time_sheet_Hours__c,Task__c, Resource_Projects__c from Timesheets__r) FROM Timesheet_Header__c where id =:a.Old_Record_ID__c];
             if(newTH != null)
             {
                 for(Timesheet__c oldTD : newTH.Timesheets__r)
                 {  
                       Timesheet__c newTS = oldTD.clone(false, true, false, false);
                       newTS.Timesheet_Header__c = a.Id;
                      
                       TDList.add(newTS);
                 }
                 
             }
            
            
         } 
         
         if(TDList.size() > 0)
         {
             //system.debug('%%%%%%%%%%%%%%TDList'+newTS.size());    
             insert TDList;
         }
         
         **///////////////
         
         
    }   
    
    global void finish(Database.BatchableContext BC)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {'mahendran_sundarraj@cable.comcast.com'});
        mail.setReplyTo('mahendran_sundarraj@cable.comcast.com');
        mail.setSenderDisplayName('Batch Processing');
        mail.setSubject('Copy TS Header - Batch Process Completed');
        mail.setPlainTextBody('Batch Process has completed');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}