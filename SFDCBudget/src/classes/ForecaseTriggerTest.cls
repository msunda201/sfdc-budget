@isTest (SeeAllData = true)
private class ForecaseTriggerTest {
 
                static Testmethod void insertForeCastTest() {
                
                                Project__c pcObj = new Project__c();
                                pcObj.Name='Conrep';   
                                pcObj.Justification__c='Test Test';
                                pcObj.New_or_Continued_Support__c='1.New spend to Comcast';
                                insert pcObj;
                                
                                Project_Budget__c pbObj = new Project_Budget__c();
                                pbObj.Name='Conrep Test';
                                pbObj.Project_Name__c=pcObj.id;
                                pbObj.Category__c='Capex - External Offshore';
                                insert pbObj;
                                
                                           
                                Vendor__c vcObj = new Vendor__c();
                                vcObj.Name='Euler';
                                vcObj.Payment_Terms__c='Net 90';
                                insert vcObj;
                                
                                Forecast__c fcObj = new Forecast__c();
                                fcObj.Name = 'Mahi123 - 01/08/2013';
                                fcObj.Recurring__c='Yes';
                                fcObj.Project_Budget__c=pbObj.id;
                                fcObj.Start_Date__c=system.today();
                                fcObj.Category__c='Capex - External Offshore';
                                fcObj.Frequency__c='Weekly';
                                fcObj.No_of_Occurences__c=3;
                                fcObj.Vendor__c=vcObj.id;
                                fcObj.Forecast_Amount__c=1000;
                                insert fcObj;
 
                }
                
                static Testmethod void insertForeCastTest3() {
                                Vendor__c vcObj = new Vendor__c();
                                vcObj.Name='Euler';
                                vcObj.Payment_Terms__c='Net 90';
                                insert vcObj;
                                
                                Forecast__c fcObj = new Forecast__c();
                                fcObj.Name = 'Mahi123 - 01/08/2013';
                                fcObj.Recurring__c='Yes';
                                fcObj.Project_Budget__c='a0GU00000058kCBMAY';
                                fcObj.Start_Date__c=system.today();
                                fcObj.Category__c='Opex - External Offshore';
                                fcObj.Frequency__c='Weekly';
                                fcObj.No_of_Occurences__c=3;
                                fcObj.Vendor__c=vcObj.id;
                                fcObj.Forecast_Amount__c=1000;
                                insert fcObj;
 
                }
                
                 static Testmethod void insertForeCastTest5() {
                                Vendor__c vcObj = new Vendor__c();
                                vcObj.Name='Euler';
                                vcObj.Payment_Terms__c='Net 90';
                                insert vcObj;
                                
                                Forecast__c fcObj = new Forecast__c();
                                fcObj.Name = 'Mahi123 - 01/08/2013';
                                fcObj.Recurring__c='No';
                                fcObj.Project_Budget__c='a0GU00000058kCBMAY';
                                fcObj.Delivery_Date__c=system.today();
                                fcObj.Category__c='Opex - External Offshore';
                                fcObj.Vendor__c=vcObj.id;
                                fcObj.Forecast_Amount__c=1000;
                                insert fcObj;
 
                }
                
               
                
                  static Testmethod void insertForeCastTest1() {
                                Vendor__c vcObj = new Vendor__c();
                                vcObj.Name='Euler';
                                vcObj.Payment_Terms__c='Net 90';
                                insert vcObj;
                                
                                Forecast__c fcObj = new Forecast__c();
                                fcObj.Name = 'Mahi123 - 01/08/2013';
                                fcObj.Recurring__c='No';
                                fcObj.Project_Budget__c='a0GU00000058kCBMAY';
                                fcObj.Delivery_Date__c=system.today();
                                fcObj.Category__c='Capex - External Offshore';
                                fcObj.Vendor__c=vcObj.id;
                                fcObj.Forecast_Amount__c=1000;
                                insert fcObj;
 
                }
 
 
}