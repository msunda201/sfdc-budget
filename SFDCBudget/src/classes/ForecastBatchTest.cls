@isTest(SeeAllData=True)
public class ForecastBatchTest {
    static testMethod void test() {
        Database.QueryLocator QL;
        List<Forecast_Entry_Form__c>  ff =  [Select Id,Name,RecordTypeId,Account_Category__c,Apr__c,Aug__c,Capital_Expense__c,Clone__c,Dec__c,Delivery_Date__c,Director_Group__c,Effort_Entity__c,End_Date__c,External_Labor__c,Feb__c,Forecast_Calendar__c,Forecast_Description__c,Forecast_Type__c,Funding_Entity__c,Invoice_Date__c,Jan__c,Jul__c,Jun__c,Justification__c,Mar__c,May__c,Nov__c,Oct__c,Off_Shore_Labor__c,Payment_Amount_1__c,Payment_Amount_2__c,Payment_Amount_3__c,Payment_Amount_4__c,Payment_Amount_5__c,Payment_Date_1__c,Payment_Date_2__c,Payment_Date_3__c,Payment_Date_4__c,Payment_Date_5__c,Project_Name__c,Sep__c,Start_Date__c,Total_Effort_Hours__c,Total_Payment_Amount__c,Total_Spent_Projection__c,Total_SW_License_Amount__c,Total_SW_Maintenance_Amount__c,Vendor__c,Forecast_Job__c,Duplicate_ID__c From Forecast_Entry_Form__c LIMIT 2];
  
        Database.BatchableContext BC;
        Copy_Forecast_Entry_Form  cc = New Copy_Forecast_Entry_Form ();
        cc.Start(BC);
        cc.execute(BC, ff );
        cc.Finish(BC); 
    }
   
}