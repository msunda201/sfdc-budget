public class sample1{
    public List<Timesheet_Header__c> mem {get;set;}
    public List<Timesheet_Header__c> mem1 {get;set;}
    public Integer count {get;set;}
    public Integer count1 {get;set;}
    public String Resource_Type_Val{get;set;}
    
    public List<Timesheet_Header__c> getTimesheets(){
       return memlist;
    }
   
    // public String Resource_TypeVal{get;set;}
    /*  public list<Timesheet_Header__c> WrappedList {get;set;}
    public boolean selectAll {get;set;}*/
    public List<Timesheet_Header__c> memList = new List<Timesheet_Header__c>();
    public List<Timesheet_Header__c> memList1 = new List<Timesheet_Header__c>();
    
    public Integer index = 20;
    public Integer start = 0;
    public Boolean nextBool {get;set;}
    public Boolean prevBool {get;set;}
    public List<People__c> conts {get; set;}
    
    private void Resource_Type_Val(){
        conts=[select Resource_Type__c from People__c where User_ID__c=:UserInfo.getUserId()];
    }
    public sample1(){
          date dv = system.today();

          
          Timesheet_Calendar__c listTC = null;
          List<Timesheet_Calendar__c> resultTC = [select id,End_Date__c,Start_Date__c from Timesheet_Calendar__c where (Start_Date__c <= :dv and End_Date__c >= :dv)];
          if(resultTC.size() > 0){
            Timesheet_Calendar_Val= resultTC[0].id;
          }
          
          mtSortDirection = strSortDirection = 'ASC';
          mtSortExpression = strSortExpression = 'Name';
          //String Resource_Type_Val;
          List<People__c> resultPC = [select Resource_Type__c from People__c where User_ID__c=:UserInfo.getUserId()];
          if(resultPC.size() > 0){
            Resource_Type_Val= resultPC[0].Resource_Type__c;
          }
          memList = [SELECT Name, Timesheet_Calendar__c, Timesheet_Period__c,Timesheet_Period_Display__c,People__c, Manager__c,Time_sheet_Status__c,Status__c, Timesheet_Total_Hours__c,Ownerid FROM Timesheet_Header__c where (Timesheet_Calendar__c=:Timesheet_Calendar_Val OR Status__c='Submit' OR Status__c='New' OR Status__c='Save') AND (OwnerID=:UserInfo.getUserId() OR Timesheet_Delegate_User__c=:UserInfo.getUserId() OR New_Owner__c=:UserInfo.getUserId()) limit 1000];
          count = [SELECT Count() FROM Timesheet_Header__c  where (Timesheet_Calendar__c=:Timesheet_Calendar_Val OR Status__c='Submit') AND (OwnerID=:UserInfo.getUserId() OR Timesheet_Delegate_User__c=:UserInfo.getUserId() OR New_Owner__c=:UserInfo.getUserId())];
        
          Timesheet_Header__c[] temp = new List<Timesheet_Header__c>();
          Timesheet_Header__c[] temp1 = new List<Timesheet_Header__c>();
        
          //Resource_TypeVal= [select Resource_Type__c from People__c where User_ID__c=:UserInfo.getUserId()];
        
          memList1 = [SELECT Name, Timesheet_Calendar__c, Timesheet_Period__c,Timesheet_Period_Display__c,People__c, Manager__c,Time_sheet_Status__c,Status__c, Timesheet_Total_Hours__c,Ownerid FROM Timesheet_Header__c where  (Status__c='New' OR Status__c='Save' OR Timesheet_Calendar__c=:Timesheet_Calendar_Val) AND (People_User_ID__c=:UserInfo.getUserId().SubString(0,15)) limit 1000];
          count1 = [SELECT Count() FROM Timesheet_Header__c  where  (Status__c='New' OR Status__c='Save' OR Timesheet_Calendar__c=:Timesheet_Calendar_Val) AND (People_User_ID__c=:UserInfo.getUserId().SubString(0,15))];
        
          //   WrappedList = new list<Timesheet_Header__c>();

        for(Integer i = start; i<memlist.size(); i++){   
             if(memList.size()>i && memList.get(i)!=null){
                temp.add(memList.get(i));
             //  WrappedList.add(memList.get(i));
            }
        }
        mem = temp;
         for(Integer j = start; j<memlist1.size(); j++){   
             if(memList1.size()>j && memList1.get(j)!=null){
                temp1.add(memList1.get(j));
                //  WrappedList.add(memList.get(i));
            }
        }
        mem1 = temp1;
        //  mem = WrappedList;
        prevBool = true;
        nextBool = (count <= index)? true : false;
        Resource_Type_Val();
        system.debug('*** sfdc constuctir *** index: ' + index + ' and start: ' + start);
    }
    public void next(){
        system.debug('*** sfdc Next start *** index: ' + index + ' and start: ' + start);
        index = index + 20;
        start = start + 20;
        system.debug('*** sfdc Next start > 20 *** index: ' + index + ' and start: ' + start);
        mem.clear();
        if(index > count){
            index = Math.Mod(count,20) + start;
            system.debug('Index is ' + index);
             //   nextBool = false;
             nextBool = (count <= index)? true : false;
            prevBool = false;
            List<Timesheet_Header__c> temp = new List<Timesheet_Header__c>();
            for(Integer i = start; i<index; i++){   
                if(memList.size()>i && memList.get(i)!=null){
                        temp.add(memList.get(i));
                }
            }
            mem = temp;  
           index = start + 20;    
        }
        else{
            List<Timesheet_Header__c> temp = new List<Timesheet_Header__c>();
            for(Integer i = start; i<index; i++){   
                if(memList.size()>i && memList.get(i)!=null){   
                    temp.add(memList.get(i));
                }
            }
            mem = temp;     
            prevBool = false;
        }   
    }
    public void previous(){
        if(start > 20){    
            index = index - 20;
            start = start - 20;
            system.debug('*** sfdc Previous start > 20 *** index: ' + index + ' and start: ' + start);
            List<Timesheet_Header__c> temp = new List<Timesheet_Header__c>();
            for(Integer i = start; i<index; i++){   
                if(memList.size()>i && memList.get(i)!=null){
                     temp.add(memList.get(i));
                }   
            }
            mem = temp; 
            prevBool = false;
            nextBool = false;
        }    
        else{
            index = index - 20;
            start = start - 20;
            system.debug('*** sfdc Previous *** index: ' + index + ' and start: ' + start);
            List<Timesheet_Header__c> temp = new List<Timesheet_Header__c>();
            for(Integer i = start; i<index; i++){   
                 if(memList.size()>i && memList.get(i)!=null){   
                    temp.add(memList.get(i));
                }
            }
            mem = temp; 
            prevBool = true;
            nextBool = false;        
        }
    }
    //### Team's Timesheet sorting
    public String strSortExpression{get;set;}
    public String strSortDirection{get;set;}
    public String Timesheet_Calendar_Val;
    public void ViewData(){
       //build the full sort expression
       String sortFullExp = strSortExpression+' '+strSortDirection;
       String strUserId = UserInfo.getUserId();
       //query the database based on the sort expression
       String strSOQL = 'SELECT Name,Timesheet_Calendar__c,Timesheet_Period__c,Timesheet_Period_Display__c,People__c, Manager__c,Time_sheet_Status__c,Status__c,Timesheet_Total_Hours__c,OwnerId FROM Timesheet_Header__c WHERE (Timesheet_Calendar__c = :Timesheet_Calendar_Val OR Status__c=\'Submit\' OR Status__c=\'New\' OR Status__c=\'Save\') AND (OwnerID = :strUserId OR Timesheet_Delegate_User__c = :strUserId OR New_Owner__c = :strUserId) ORDER BY '+sortFullExp+' LIMIT 1000';

       mem = Database.query(strSOQL);
       strSortDirection = (strSortDirection == 'ASC')? 'DESC' : 'ASC';
       //return null;
    }
    //### My Timesheets
    public String mtSortExpression{get;set;}
    public String mtSortDirection{get;set;}
    public void mtViewData(){
       //build the full sort expression
       String sortFullExp = mtSortExpression+' '+mtSortDirection;
       String strProfId = UserInfo.getUserId().SubString(0,15);
       //query the database based on the sort expression
       String strSOQL = 'SELECT Name,Timesheet_Calendar__c,Timesheet_Period__c,Timesheet_Period_Display__c,People__c, Manager__c,Time_sheet_Status__c,Status__c,Timesheet_Total_Hours__c,OwnerId FROM Timesheet_Header__c WHERE (Status__c = \'New\' OR Status__c = \'Save\' OR Timesheet_Calendar__c = :Timesheet_Calendar_Val) AND (People_User_ID__c = :strProfId) ORDER BY '+sortFullExp+' limit 1000';
       system.debug('### strSOQL: '+strSOQL);
       mem1 = Database.query(strSOQL);
       mtSortDirection = (mtSortDirection == 'ASC')? 'DESC' : 'ASC';
       //return null;
    }
}