@isTest
public class Testsample1{
    @isTest
    private static void test(){
    
        People__c peopleCal = new People__c(Name='Test',Resource_Type__c='Employee',User_ID__c=UserInfo.getUserId());
        insert peopleCal;
        
        Timesheet_Calendar__c timeCal = new Timesheet_Calendar__c(Name='Test Calendar',Start_Date__c=system.today(),End_Date__c=system.today().addDays(7));
        insert timeCal;
        
        Timesheet_Header__c timeHeader = new Timesheet_Header__c(Name='Test Header',People__c=peopleCal.id,Timesheet_Calendar__c=timeCal.Id);
        insert timeHeader;
        
        People__c peopleCal1 = new People__c(id=peopleCal.id,Delegate_Time_sheet_User__c=UserInfo.getUserId());
        
         update peopleCal1;
        
        sample1 sam = new sample1();
              
        sam.next();
        sam.previous();
        sam.start = 21;
        sam.previous();
        
        ApexPAges.StandardController sc = new ApexPages.StandardController(peopleCal1);
        DelegateTimesheetUsers del = new DelegateTimesheetUsers(sc);
        del.savePeopleEntry();
        
         ApexPAges.StandardController sc1 = new ApexPages.StandardController(peopleCal1);
         UnDelegateTimesheetUsers undel = new UnDelegateTimesheetUsers(sc);
         undel.savePeopleEntry();
        
        
    }
   
}