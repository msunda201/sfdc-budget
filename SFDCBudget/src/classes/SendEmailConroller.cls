public class SendEmailConroller{
    public static void sendEmail(Set<Id> userId){
        List<User> listUser = [select id,Name,email from User where id in: userId];
   //     List<People__c> lstPeople = [select id,(select id from People3__r) from People__c where Manager_User_ID__c =: userIds];
        
   //     if(lstPeople != null && lstPeople.size() > 0){
  //          if(lstPeople[0].People3__r != null && lstPeople[0].People3__r.size() > 0){
    //            Set<Id> setPeopleIds = new Set<Id>();
    //            for(People__c ppl : lstPeople[0].People3__r){
    //                setPeopleIds.add(ppl.Id);
    //            }
             
        //        List<People__c> listPeople = [select id,Name,user_Id__c,(select Id,Name from Timesheet_Header__r where Status__c != 'Submit') from People__c where id in: setPeopleIds];
                String Withdrawn='WithDrawn';   
                List<People__c> listPeople2 = [select id,Name,Manager_User_ID__c,(select Id,Name,Status__c from Timesheet_Header__r where Status__c = 'Submit' AND Timesheet_Reminder_Sent_Date__c != null) from People__c where Timesheet_Required__c=true AND Empl_Status_Desc__c!=:Withdrawn AND Time_Sheet_Override_Flag__c=false AND Manager_User_ID__c in: userId];
                List<People__c> listPeople = [select id,Name,Manager_User_ID__c,(select Id,Name,Status__c from Timesheet_Header__r where Status__c != 'Submit' AND Timesheet_Calendar__r.End_Date__c <= :system.today()) from People__c where Timesheet_Required__c=true AND Empl_Status_Desc__c!=:Withdrawn AND Time_Sheet_Override_Flag__c=false AND Manager_User_ID__c in: userId];
                Map<Id,People__c> mapBoolean = new Map<Id,People__c>();
                List<Timesheet_Header__c> listUpdateTimesheetHeader = new List<Timesheet_Header__c>();
                if(listPeople != null && listPeople.size() > 0){
                    for(People__c people : listPeople){
                        if(people.Timesheet_Header__r != null && people.Timesheet_Header__r.size() > 0){
                            mapBoolean.put(people.Manager_User_ID__c,people);
                            for(Timesheet_Header__c th : people.Timesheet_Header__r){
                                th.Timesheet_Reminder_Sent_Date__c = system.today();
                            }
                            listUpdateTimesheetHeader.addAll(people.Timesheet_Header__r);
                        }
                    }
                }
                if(listPeople2 != null && listPeople2.size() > 0){
                    for(People__c people : listPeople2){
                        if(people.Timesheet_Header__r != null && people.Timesheet_Header__r.size() > 0){
                            for(Timesheet_Header__c th : people.Timesheet_Header__r){
                                th.Timesheet_Reminder_Sent_Date__c = null;
                            }
                            listUpdateTimesheetHeader.addAll(people.Timesheet_Header__r);
                        }
                    }
                }
                if(listUpdateTimesheetHeader.size() > 0){
                    update listUpdateTimesheetHeader;
                }
                List<EmailTemplate> listTemp = [select id from EmailTemplate where name = 'Time sheet not Submitted'];
                if(listTemp != null && listTemp.size() > 0){
                    List<Messaging.SingleEmailMessage> emails=new List<Messaging.SingleEmailMessage>();
                
                    for(User usr : listUser){
                        if(mapBoolean.containsKey(usr.id)){
                            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                            List<String> toAddresses = new List<String>{usr.Email};
                            email.setTargetObjectId(usr.Id); 
                            email.setTemplateId(listTemp[0].Id);     
                            email.setWhatId(mapBoolean.get(usr.id).Id); 
                            email.setSaveAsActivity(false); 
                            email.setToAddresses(toAddresses);
                            String[] BccAddresses = new String[] {'Venkat_Sakhamuri@cable.comcast.com'};
                            email.setBccaddresses(Bccaddresses);

                            emails.add(email); 
                        }
                    }
                    if(emails.size() > 0){
                        Messaging.sendEmail(emails);
                    }
                }
            }
     //   }
   // }
}