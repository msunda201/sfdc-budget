@isTest
public class TestApexEditableTimesheetListExtension{
    @isTest
    private static void test(){
        Timesheet_Calendar__c timeCal = new Timesheet_Calendar__c(Name='Test Calendar',Start_Date__c=system.today(),End_Date__c=system.today().addDays(7));
        insert timeCal;
        
        Timesheet_Header__c timeHeader = new Timesheet_Header__c(Name='Test Header',Timesheet_Calendar__c=timeCal.Id);
        insert timeHeader;
        
        Portfolio__c port = new Portfolio__c(Name='Test Port', Active__c=TRUE);
        insert port;
        
        Projects_NIR__c proj = new Projects_NIR__c(Name='Test Proj',Portfolio_ID__c=port.Id);
        insert proj;
        
        Task__c tsk = new Task__c(Active__c=true,Name='My Task');
        insert tsk;
        
        Task__c tsk1 = new Task__c(Active__c=true,Name='My Task2');
        insert tsk1;
        
        Timesheet__c timesheet = new Timesheet__c(Task__c=tsk.Id,Timesheet_Header__c=timeHeader.Id,Portfolio__c=port.Id,Resource_Projects__c=proj.Id,Mon__c=10,Tue__c=20,Wed__c=20,Thu__c=20,Fri__c=20,Sat__c=20,Sun__c=20);
        insert timesheet;
        
        ApexEditableTimesheetListExtension venkat = new ApexEditableTimesheetListExtension(new ApexPages.StandardController(timeHeader));
        venkat.loadchildWrapperlist();
        venkat.selectedListItem = '0';
        venkat.loadProjectonProt();
        venkat.calculateTotalTimeshHr();
        venkat.setingProjandtaskvalue();
        venkat.savetimesheetdetail();
        venkat.addToList();
        venkat.removeFromList();
        venkat.getSuccessURL();
        
    }
}