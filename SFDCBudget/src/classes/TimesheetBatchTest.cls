@isTest(SeeAllData=True)
public class TimesheetBatchTest {
    static testMethod void test() {
        Database.QueryLocator QL;
        List<Timesheet_Header__c>  ff =  [SELECT ID,Timesheet_Calendar__c,OwnerID,Approved_by__c,People__c,People__r.Name, Timesheet_Calendar__r.Name FROM Timesheet_Header__c LIMIT 2];
  
        Database.BatchableContext BC;
        Copy_Timesheet_Calendar  cc = New Copy_Timesheet_Calendar();
        cc.Start(BC);
        cc.execute(BC,ff);
        cc.Finish(BC); 
    }
    
     static testMethod void test5() {
        Database.QueryLocator QL;
        String Withdrawn='WithDrawn'; 
        List<People__c>  ff =  [select Id, Manager_c__c,Manager_User_ID__c,Name,Resource_Type__c,Manager_Email_ID__c,Work_Email__c,User_ID__c from People__c where Timesheet_Required__c=true AND Empl_Status_Desc__c!=:Withdrawn AND Time_Sheet_Override_Flag__c=false  LIMIT 2];
  
        Database.BatchableContext BC;
        New_People_Timesheet cc = New New_People_Timesheet();
        cc.Start(BC);
        cc.execute(BC,ff);
        cc.Finish(BC); 
    }
    
  
    
    static testMethod void test1() {
    
        date dv = system.today()+1-2;
          string Timesheet_Calendar_Val;

          Timesheet_Calendar__c listTC = null;
          List<Timesheet_Calendar__c> resultTC = [select id,End_Date__c,Start_Date__c from Timesheet_Calendar__c where (Start_Date__c <= :dv and End_Date__c >= :dv)];
          if(resultTC.size() > 0)
          {
            Timesheet_Calendar_Val= resultTC[0].id;
          }
          
          String Withdrawn='WithDrawn'; 
    
        Database.QueryLocator QL;
        List<Timesheet_Header__c>  ff =  [SELECT ID,Old_Record_ID__c,Timesheet_Calendar__c,OwnerID,Approved_by__c,People__c,People__r.Name, Timesheet_Calendar__r.Name FROM Timesheet_Header__c where People__r.Timesheet_Required__c=true AND People__r.Empl_Status_Desc__c!=:Withdrawn  AND People__r.Time_Sheet_Override_Flag__c=false AND Timesheet_Calendar__c=:Timesheet_Calendar_Val];
  
  
        List<People__c>  ff1 =  [select Id, Manager_c__c,Manager_User_ID__c,Name,Resource_Type__c,Manager_Email_ID__c,Work_Email__c,User_ID__c from People__c where Timesheet_Required__c=true AND Empl_Status_Desc__c!=:Withdrawn AND Time_Sheet_Override_Flag__c=false AND Id not In (select People__c from Timesheet_Header__c where Timesheet_Calendar__c=:Timesheet_Calendar_Val)];

  
       
        Database.BatchableContext BC1;
        New_People_Timesheet  cc1 = New New_People_Timesheet();
        cc1.Start(BC1);
        cc1.execute(BC1,ff1);
        cc1.Finish(BC1); 
        
        
        
        
        
    }
   
}