@isTest
public class TestManageDailyTimeSheet{
    @isTest
    private static void test(){
        Timesheet_Calendar__c timeCal = new Timesheet_Calendar__c(Name='Test Calendar',Start_Date__c=system.today(),End_Date__c=system.today().addDays(7));
        insert timeCal;
        
        Timesheet_Header__c timeHeader = new Timesheet_Header__c(Name='Test Header',Timesheet_Calendar__c=timeCal.Id);
        insert timeHeader;
        
        Portfolio__c port = new Portfolio__c(Name='Test Port');
        insert port;
        
        Projects_NIR__c proj = new Projects_NIR__c(Name='Test Proj',Portfolio_ID__c=port.Id);
        insert proj;
        
        Task__c tsk = new Task__c(Active__c=true,Name='My Task');
        insert tsk;
        
        Task__c tsk1 = new Task__c(Active__c=true,Name='My Task2');
        insert tsk1;
        
        Timesheet__c timesheet = new Timesheet__c(Time_sheet_Hours__c=20,Task__c=tsk.Id,Timesheet_Header__c=timeHeader.Id,Portfolio__c=port.Id,Resource_Projects__c=proj.Id);
        insert timesheet;
        
        timesheet.Task__c = tsk1.Id;
        update timesheet;
    }
}