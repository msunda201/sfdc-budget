public class CommonFunctions {
    
    private static CommonFunctions instance = null; 
   
    private CommonFunctions () {
    }
	
	public static CommonFunctions getInstance(ApexPages.StandardController controller){
		if (instance==null) {
			instance = new CommonFunctions();
		}
		Schema.Sobjecttype sObjtype =   controller.getRecord().getSObjectType();
		system.debug('Sobject in getInstance' + sObjtype.newSObject());
		return instance;
	}
	
		
	public List<SelectOption> getCommonListEntity(){
		System.debug('Inside get List Entity in Commons');
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        
        string entitystring='Select ID,Name from Entity__c where Active__c=True order by Name';
        List<Entity__c> lstEnt = Database.query(entitystring);
        for(Integer i=0; i<lstEnt.size(); i++) 
        {
             options.add(new SelectOption(lstEnt.get(i).ID,lstEnt.get(i).Name));
        }
        return options; 
	}
        
            
    
    
    // getListDirector method for get List of Director from Director_Group__c
    
   
    /////////////////////////////////////////////
    public List<SelectOption> getListVendor() {
    	
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        
        for(Vendor2014__c p2:[select ID,Name from Vendor2014__c order by Name]) {
            options.add(new SelectOption(p2.ID,p2.Name.toLowerCase()));
        }
        return options;
    }
    
    
    	

	    
    /////////////////////////////////////////////
    public List<SelectOption> getListAccountCategory() {
       
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        for(Budget_Category__c p2:[select ID,Name from Budget_Category__c where Display_Flag__c=True order by Name]) {
                options.add(new SelectOption(p2.ID,p2.Name));
        }

        return options;
    }
	

}