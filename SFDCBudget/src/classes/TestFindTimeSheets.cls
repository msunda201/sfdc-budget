@isTest
public class TestFindTimeSheets{
    @isTest
    private static void test(){
        People__c people = new People__c(Name='Test People',Manager_User_ID__c=userinfo.getUserId());
        insert people;
        
        Timesheet_Calendar__c tc = new Timesheet_Calendar__c(Start_Date__c=system.today(),End_Date__c=system.today().addDays(2));
        insert tc;
        
        Timesheet_Header__c th = new Timesheet_Header__c(Name='Test TH',Timesheet_Calendar__c=tc.Id,People__c=people.Id,Status__c='New');
        insert th;
        
        findTimeSheets ft = new findTimeSheets();
        ft.userId = userinfo.getUserId();
        ft.getPeoples();
    }
}