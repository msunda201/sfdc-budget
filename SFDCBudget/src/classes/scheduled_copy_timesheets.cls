global class scheduled_copy_timesheets  implements Schedulable {
   global void execute(SchedulableContext sc) {

      Copy_Timesheet_Calendar  b = new Copy_Timesheet_Calendar (); 
      database.executebatch(b);
   }
}