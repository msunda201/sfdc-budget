@isTest
public class TestSelectedSizeWorkaroundBK{
    @isTest
    private static void test(){
        Timesheet_Calendar__c timeCal = new Timesheet_Calendar__c(Name='Test Calendar',Start_Date__c=system.today(),End_Date__c=system.today().addDays(7));
        insert timeCal;
        
        Timesheet_Header__c timeHeader = new Timesheet_Header__c(Name='Test Header',Timesheet_Calendar__c=timeCal.Id);
        insert timeHeader;
        
        ApexPages.currentPage().getParameters().put('selids',timeHeader.id);
        selectedSizeWorkaroundBK  sel = new selectedSizeWorkaroundBK(new ApexPages.StandardSetController(new List<Timesheet_Header__c>{timeHeader}));
        sel.getMySelectedSize();
        sel.getMyRecordsSize();
        sel.saveRec();
        
        
        ApexPages.currentPage().getParameters().put('selids',timeHeader.id);
        selectedSizeSubmit  sel1 = new selectedSizeSubmit(new ApexPages.StandardSetController(new List<Timesheet_Header__c>{timeHeader}));
        sel1.getMySelectedSize();
        sel1.getMyRecordsSize();
        sel1.saveRec();
        
        
    }
}