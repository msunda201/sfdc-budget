global class SchedulePeopleController implements Schedulable {
   global void execute(SchedulableContext SC) {
      if(!Test.isRunningTest())database.executeBatch(new BatchPeopleController(),10);
   }
}