/*
 * EditableContactListExtension.cls
 *
 * Copyright (c)2013, Michael Welburn.
 * License: MITinsert
 *
 * Usage:
 *   This is the implementation class where Object specific references
 *   need to be defined. Please see EditableList.cls for more information
 *   on what each method is responsible for.
 *
 *   Required methods:
 *     - EditableContactListExtension(ApexPages.StandardController)
 *     - getChildren()
 *     - initChildRecord()
 *
 */
public with sharing class ApexEditableTimesheetListExtension
{
  // Read the explanation in EditableContactListExtension(ApexPages.StandardController)
  // to see when to uncomment the following line.

  // public Account myAccount {get; private set;}
     //public Timesheet_Header__c objTimesheetDetail{get;private set;}
    // public List<Timesheet__c> Timesheet{get; set;}
     public Timesheet_Header__c timeheader{get;set;}        
     public map<id,Timesheet__c> childMap;
     public decimal results{get;set;}
     public map<string,string> mapDayToDate{get;set;}
     public list<TimeSheetDetailWrapper> removeWrapperlist{set;get;}
     public list<TimeSheetDetailWrapper> childWrapperlist
     {
        get{
            if(childWrapperlist==null)
            {
                childWrapperlist=new list<TimeSheetDetailWrapper>(); 
                loadchildWrapperlist();
               
            }
            return childWrapperlist;
        }set;
    }
    
     public string selectedListItem{
        get{
            if(selectedListItem==null)
            {
                selectedListItem='';
               
            }
            return selectedListItem;
        }set;
    }
  public  map<id,List<Projects_NIR__c>> projectwithProMap=new map<id,List<Projects_NIR__c>>();
  
  public  List<SelectOption> taskOption{
        get{
            if(taskOption==null)
            {
                taskOption= new List<SelectOption>();
               
            }
            return taskOption;
        }set;
    }
  
    
  public ApexEditableTimesheetListExtension(ApexPages.StandardController stdController) 
  {
    removeWrapperlist = new list<TimeSheetDetailWrapper>();
    string buttonId = ApexPages.currentPage().getParameters().get('button');        
    Timesheet_Header__c objTH = (Timesheet_Header__c)stdController.getRecord();
    String strTP = objTH.Timesheet_Period__c;
    if(Test.isRunningTest()){
        strTP = '10-23-2014 - 10-30-2014';
    }
    list<string> lstDtRange = strTP.split(' - '); 
    
    String strStartDt = lstDtRange[0];
    list<string> lstStartDtValues = strStartDt.split('-');
    system.debug(lstStartDtValues[0]+'##'+lstStartDtValues[1]+'##'+lstStartDtValues[2]);
    integer iStMM = integer.valueof(lstStartDtValues[1]);
    integer iStDD = integer.valueof(lstStartDtValues[2]);
    integer iStYY = integer.valueof(lstStartDtValues[0]);
    Datetime dt = DateTime.newInstance(iStYY,iStMM,iStDD);
    //Datetime dt = Datetime.valueof(stdt);
    //integer yy = dt.year();
    String dayOfWeek = dt.format('E');
    list<Datetime> lstDT = new list<Datetime>();
    if(dayOfWeek=='Sun')
        dt.addDays(-6);
    else if(dayOfWeek=='Sat')
        dt.addDays(-5);
    else if(dayOfWeek=='Fri')
        dt.addDays(-4);
    else if(dayOfWeek=='Thu')
        dt.addDays(-4);
    else if(dayOfWeek=='Wed') 
        dt.addDays(-4);
       // dt.addDays(2);
    else if(dayOfWeek=='Tue')
        dt.addDays(-4);
      //dt.addDays(4);
    mapDayToDate = new map<string,string>();    
    for(integer iindex=0;iindex<7;iindex++){
        //dt = System.now();
        system.debug('dt'+iindex+'::'+dt);
        integer yy = dt.year();
        integer mm = dt.month();
        integer dd = dt.day();
        String dow = dt.format('E');
       // mapDayToDate.put(dow,mm+'/'+dd+'/'+yy); 
       system.debug(dow+'@@@@@'+mm+'/'+dd);
       mapDayToDate.put(dow,mm+'/'+dd);
       
        dt = dt.addDays(1);
    }
    system.debug('mapDayToDate::'+mapDayToDate);
    timeheader=(Timesheet_Header__c)stdController.getRecord();
    //objTSEntry=new Timesheet__c();
    //objTSEntry=(Timesheet__c)controller.getrecord();
        
    // If necessary, explicitly query for additional metadata on parent record
    // if you are looking to display things that don't come back with the
    // StandardController. In that case, you will need to replace the "Account.X"
    // references in the Visualforce Page with references to an Account variable
    // that you will need to declare in this class (myAccount.X). I have commented out
    // an example.

    // this.myAccount = [SELECT Id,
    //                            Name,
    //                            Custom_Relationship__r.Name
    //                        FROM Account
    //                        WHERE Id =: stdController.getRecord().Id];
    
  }
  
  public void loadchildWrapperlist(){
     map<id,Portfolio__c> protfoMap;
     list<Task__c> taskList=new list<Task__c>();
     list<Projects_NIR__c> projectList=new list<Projects_NIR__c>();
     list<SelectOption> projecttempList=new list<SelectOption>();
     
     List<SelectOption> portfoOption= new List<SelectOption>();
    
     projecttempList.add(new SelectOption('--Choose--','--Choose--'));
     portfoOption.add(new SelectOption('--Choose--','--Choose--'));
     
     protfoMap = new map<id,Portfolio__c>();
     List<Portfolio__c> listPortfolios = [select ID,Name from Portfolio__c where Active__c=TRUE order by Name asc];
     for(Portfolio__c loopPortfolio : listPortfolios){
        protfoMap.put(loopPortfolio.Id,loopPortfolio);
     }
   
     projectList=[select ID,Name,Porfolio_Name__c,Portfolio_ID__c from Projects_NIR__c where Portfolio_ID__c=:protfoMap.keySet() order by Name asc];
     taskList=[select id,Name,Capex_Opex__c,Task_Code__c,Task_Description__c from Task__c where Active__c=true order by Name asc];
     
     for(Portfolio__c proFvar : listPortfolios){
   //  for(Portfolio__c proFvar : protfoMap.values()){
         String nameStr = proFvar.Name;
         nameStr = (nameStr.length() > 50)? proFvar.Name.substring(0,50) : proFvar.Name;
         portfoOption.add(new SelectOption(proFvar.id,nameStr.trim())); 
      }
       if(projectwithProMap.isEmpty()){
         for(Projects_NIR__c projectFVar :projectList){
          if(projectwithProMap.get(projectFVar.Portfolio_ID__c)==null){
              projectwithProMap.put(projectFVar.Portfolio_ID__c,new List<Projects_NIR__c>());
          }
             projectwithProMap.get(projectFVar.Portfolio_ID__c).add(projectFVar);
         }
      }
      if(taskOption.isEmpty()){
          taskOption.add(new SelectOption('--Choose--','--Choose--'));
          for(Task__c taskFVar : taskList){
              taskOption.add(new SelectOption(taskFVar.id,taskFVar.Name));
          }
      }
     childMap=new map<id,Timesheet__c>([SELECT Id,Timesheet_Calendar__c,Timesheet_Period__c,Portfolio__c,People__c,Manager__c,
                          Resource_Projects__c,Status__c,Task__c,Portfolio__r.Name,Resource_Projects__r.Name,Task__r.Name,Mon__c,Tue__c,Wed__c,Thu__c,Fri__c,Sat__c,Sun__c
                      FROM Timesheet__c
                      WHERE Timesheet_Header__c =: timeheader.Id order by createddate asc]);   
         
     if(childWrapperlist.isEmpty()& !childMap.isEmpty()){
        for(Timesheet__c timesheetFvar :childMap.values()){
             List<SelectOption> projectOption= new List<SelectOption>();
             projectOption.add(new SelectOption('--Choose--','--Choose--'));
             if(projectwithProMap.get(timesheetFvar.Portfolio__c)!=null){
                 for(Projects_NIR__c projectFVar: projectwithProMap.get(timesheetFvar.Portfolio__c)){
                      String nameStr=projectFVar.Name;
                     if (nameStr.length() > 50){
                       nameStr = projectFVar.Name.substring(0,50);
                     }
                     else{
                       nameStr = projectFVar.Name;
                     }
                      projectOption.add(new SelectOption(projectFVar.id,nameStr.trim()));
                  }
              }
              childWrapperlist.add(new TimeSheetDetailWrapper(portfoOption,projectOption,taskOption,
                                      String.ValueOF(timesheetFvar.Portfolio__c),String.ValueOF(timesheetFvar.Resource_Projects__c),
                                      String.ValueOF(timesheetFvar.Task__c),
                                      String.ValueOF(timesheetFvar.Mon__c),
                                      String.ValueOF(timesheetFvar.Tue__c),
                                      String.ValueOF(timesheetFvar.Wed__c),
                                      String.ValueOF(timesheetFvar.Thu__c),
                                      String.ValueOF(timesheetFvar.Fri__c),
                                      String.ValueOF(timesheetFvar.Sat__c),
                                      String.ValueOF(timesheetFvar.Sun__c),                                        
                                      //String.ValueOF(timesheetFvar.Time_sheet_Hours__c),
                                      String.ValueOF(Integer.ValueOF(timesheetFvar.Mon__c)+Integer.ValueOF(timesheetFvar.Tue__c)+Integer.ValueOF(timesheetFvar.Wed__c)+Integer.ValueOF(timesheetFvar.Thu__c)+Integer.ValueOF(timesheetFvar.Fri__c)+Integer.ValueOF(timesheetFvar.Sat__c)+Integer.ValueOF(timesheetFvar.Sun__c)),
                                      timesheetFvar.id));                        
          }
     }
     else{
          childWrapperlist.add(new TimeSheetDetailWrapper(portfoOption,projecttempList,taskOption));
     }
   }
   
   public void loadProjectonProt(){
      List<SelectOption> projectOption= new List<SelectOption>();
      projectOption.add(new SelectOption('--Choose--','--Choose--'));
      
      Integer temp=Integer.ValueOf(selectedListItem);
      if(childWrapperlist.get(temp).protfStr != '--Choose--' && projectwithProMap.get(childWrapperlist.get(temp).protfStr)!=null){
          for(Projects_NIR__c projectFVar: projectwithProMap.get(childWrapperlist.get(temp).protfStr)){
              String nameStr=projectFVar.Name;
             if (nameStr.length() > 50){
               nameStr = projectFVar.Name.substring(0,50);
             }
             else{
               nameStr = projectFVar.Name;
             }
              projectOption.add(new SelectOption(projectFVar.id,nameStr.trim()));
          }
      }
      childWrapperlist.get(temp).projectWIns=projectOption;
   }
    public void addToList(){
      loadchildWrapperlist(); 
    }
    
    public void removeFromList()  {
         Integer temp=Integer.ValueOf(selectedListItem);
         if(childWrapperlist.get(temp)!=null)
         {
              removeWrapperlist.add(childWrapperlist.get(temp));
              childWrapperlist.remove(temp);
         }
        
    }
 
 
 /* public void removeFromList()
    {
         Integer temp=Integer.ValueOf(selectedListItem);
         if(childWrapperlist.get(temp)!=null)
         {
            if(childWrapperlist.get(temp).timesheetId!=null)
            {
                Timesheet__c tempSh=childMap.get(childWrapperlist.get(temp).timesheetId);
                delete tempSh;
                //Uncomment this below code if its didn't remove user view but it has removed in the
                //backend after replace this method in existing call.
                
                childWrapperlist.remove(temp);
                System.debug('**********removelist***'+childWrapperlist);
            }
            else
            {
              childWrapperlist.remove(temp);
            }
         }
        
    }*/
    
    public pagereference getSuccessURL()
    {
        //pagereference page=new pagereference('/a0u/o');
        pagereference page=new pagereference('/apex/My_Timesheets');
        page.setRedirect(true);
        return page;
    }
    
   public void calculateTotalTimeshHr(){
        Integer temp;
        system.debug('#### selectedListItem: '+selectedListItem);
        //onclick="EnableSave('Calculate');"
        if(selectedListItem != ''){
            temp=Integer.ValueOf(selectedListItem);
            Integer Total=0;
            if(childWrapperlist.get(temp)!=null){
                Total = Integer.valueOf(childWrapperlist.get(temp).prjHoursMon) + Integer.valueOf(childWrapperlist.get(temp).prjHoursTue);
                Total += Integer.valueOf(childWrapperlist.get(temp).prjHourswed) + Integer.valueOf(childWrapperlist.get(temp).prjHoursThr);
                Total += Integer.valueOf(childWrapperlist.get(temp).prjHoursFri) + Integer.valueOf(childWrapperlist.get(temp).prjHoursSat);
                Total += Integer.valueOf(childWrapperlist.get(temp).prjHoursSun);
                /*
                if(childWrapperlist.get(temp).prjHoursMon!=null && 
                  Integer.valueOf(childWrapperlist.get(temp).prjHoursMon)>0){
                    Total+=Integer.valueOf(childWrapperlist.get(temp).prjHoursMon);
                    System.debug('*******hr1**************'+Total);
                }
                if(childWrapperlist.get(temp).prjHoursTue!=null && 
                  Integer.valueOf(childWrapperlist.get(temp).prjHoursTue)>0){
                    Total+=Integer.valueOf(childWrapperlist.get(temp).prjHoursTue);
                    System.debug('*******hr2**************'+Total);
                }
                if(childWrapperlist.get(temp).prjHourswed!=null && 
                  Integer.valueOf(childWrapperlist.get(temp).prjHourswed)>0){
                    Total+=Integer.valueOf(childWrapperlist.get(temp).prjHourswed);
                    System.debug('*******hr3**************'+Total);
                }
                if(childWrapperlist.get(temp).prjHoursThr!=null && 
                  Integer.valueOf(childWrapperlist.get(temp).prjHoursThr)>0){
                    Total+=Integer.valueOf(childWrapperlist.get(temp).prjHoursThr);
                    System.debug('*******hr4**************'+Total);
                }
                if(childWrapperlist.get(temp).prjHoursFri!=null && 
                  Integer.valueOf(childWrapperlist.get(temp).prjHoursFri)>0){
                    Total+=Integer.valueOf(childWrapperlist.get(temp).prjHoursFri);
                    System.debug('*******hr5**************'+Total);
                }
                if(childWrapperlist.get(temp).prjHoursSat!=null && 
                  Integer.valueOf(childWrapperlist.get(temp).prjHoursSat)>0){
                    Total+=Integer.valueOf(childWrapperlist.get(temp).prjHoursSat);
                    System.debug('*******hr6**************'+Total);
                }
                if(childWrapperlist.get(temp).prjHoursSun!=null && 
                  Integer.valueOf(childWrapperlist.get(temp).prjHoursSun)>0){
                    Total+=Integer.valueOf(childWrapperlist.get(temp).prjHoursSun);
                    System.debug('*******hr7**************'+Total);
                }
                */
                childWrapperlist.get(temp).prjHours=String.Valueof(Total);
            }
        }
    }
    public pagereference setingProjandtaskvalue(){
        return null;
    }
    public PageReference savetimesheetdetail(){
        list<Timesheet__c> finaltimesheetInsertList=new list<Timesheet__c>();
         Savepoint sp = Database.setSavepoint();
         try
         {
             List<Timesheet_Header__c> listHead = [select id,Name,Timesheet_Period__c,Timesheet_Total_Hours__c from Timesheet_Header__c where id =: apexpages.currentPage().getParameters().get('id')];
         
       //      List<Daily_Timesheets__c> dailyTimeSheet = new List<Daily_Timesheets__c>();
             for(TimeSheetDetailWrapper wrFvar : childWrapperlist)
             {
                 if(wrFvar.timesheetId!=null)
                 {
                     childMap.get(wrFvar.timesheetId).Portfolio__c=wrFvar.protfStr;
                     childMap.get(wrFvar.timesheetId).Resource_Projects__c=wrFvar.prodjectStr;
                     childMap.get(wrFvar.timesheetId).Task__c=wrFvar.taskStr;
                     childMap.get(wrFvar.timesheetId).Mon__c=Decimal.Valueof(wrFvar.prjHoursMon);
                     childMap.get(wrFvar.timesheetId).Tue__c=Decimal.Valueof(wrFvar.prjHoursTue);
                     childMap.get(wrFvar.timesheetId).Wed__c=Decimal.Valueof(wrFvar.prjHourswed);
                     childMap.get(wrFvar.timesheetId).Thu__c=Decimal.Valueof(wrFvar.prjHoursThr);
                     childMap.get(wrFvar.timesheetId).Fri__c=Decimal.Valueof(wrFvar.prjHoursFri);
                     childMap.get(wrFvar.timesheetId).Sat__c=Decimal.Valueof(wrFvar.prjHoursSat);
                     childMap.get(wrFvar.timesheetId).Sun__c=Decimal.Valueof(wrFvar.prjHoursSun);
                     //childMap.get(wrFvar.timesheetId).Time_sheet_Hours__c=Integer.Valueof(wrFvar.prjHours);
                    
                 //    childMap.get(wrFvar.timesheetId).newtotalhours=Integer.Valueof(wrFvar.prjHours);
                     finaltimesheetInsertList.add(childMap.get(wrFvar.timesheetId));
                     
                     /*
                     Daily_Timesheets__c daily = new Daily_Timesheets__c();
                     daily.Resource_Projects__c = childMap.get(wrFvar.timesheetId).Resource_Projects__c;
                     daily.Task__c = childMap.get(wrFvar.timesheetId).Task__c;
                     daily.Timesheet_Date__c = listHead[0].Timesheet_Period__c;
                     daily.Timesheet_Header__c = listHead[0].Id;
                     daily.Timesheet_Hours__c = listHead[0].Timesheet_Total_Hours__c;
                     dailyTimeSheet.add(daily); 
                     */
                 }
                 else
                 {
                   finaltimesheetInsertList.add(new Timesheet__c(Portfolio__c=wrFvar.protfStr,Resource_Projects__c=wrFvar.prodjectStr,
                                           Task__c=wrFvar.taskStr,
                                           Mon__c=Decimal.Valueof(wrFvar.prjHoursMon),
                                           Tue__c=Decimal.Valueof(wrFvar.prjHoursTue),
                                           Wed__c=Decimal.Valueof(wrFvar.prjHourswed),
                                           Thu__c=Decimal.Valueof(wrFvar.prjHoursThr),
                                           Fri__c=Decimal.Valueof(wrFvar.prjHoursFri),
                                           Sat__c=Decimal.Valueof(wrFvar.prjHoursSat),
                                           Sun__c=Decimal.Valueof(wrFvar.prjHoursSun),
                                           //Time_sheet_Hours__c=Integer.Valueof(wrFvar.prjHours),
                                           Timesheet_Header__c=timeheader.id));  
                     /*                      
                     Daily_Timesheets__c daily = new Daily_Timesheets__c();
                     daily.Resource_Projects__c = wrFvar.prodjectStr;
                     daily.Task__c = wrFvar.taskStr;
                     daily.Timesheet_Date__c = listHead[0].Timesheet_Period__c;
                     daily.Timesheet_Header__c = listHead[0].Id;
                     daily.Timesheet_Hours__c = Integer.Valueof(wrFvar.prjHours);
                     dailyTimeSheet.add(daily);
                     */
                 }
             }
             
             if(removeWrapperlist != null && removeWrapperlist.size() > 0){
                 Set<String> tmstids = new Set<String>();
                 for(TimeSheetDetailWrapper wrFvar : removeWrapperlist){
                     if(wrFvar.timesheetId != null){
                         tmstids.add(wrFvar.timesheetId);
                     }
                 }
                 if(tmstids.size() > 0){
                     delete [select id from Timesheet__c where id in: tmstids];
                 }
             }
             
             if(!finaltimesheetInsertList.isEmpty())
             {
              
                upsert finaltimesheetInsertList;
                 timeheader.Old_Record_ID__c=timeheader.ID;
                 update timeheader;
                 
               //  if(dailyTimeSheet.size() > 0){
                //     insert dailyTimeSheet;
                // }
                 //pagereference page=new pagereference('/a0u/o');
                 pagereference page=new pagereference('/apex/My_Timesheets');
                 page.setRedirect(true);
                 return page;
             }
         }
         catch(Exception e)
         {
              Database.rollback(sp);
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
              ApexPages.addMessages(e);        
              return null;
         }
         return null;
    }
    
    public class TimeSheetDetailWrapper
    {
       public List<SelectOption> protfoWIns{get;set;}
       public List<SelectOption> projectWIns{get;set;}
       public List<SelectOption> taskWIns{get;set;}
       public string protfStr{get;set;}
       public string prodjectStr{
        get{
            if(prodjectStr==null)
            {
                prodjectStr='';
               
            }
            return prodjectStr;
        }set;
    }
       public string taskStr{
        get{
            if(taskStr==null)
            {
                taskStr='';
               
            }
            return taskStr;
        }set;
    }
       public String prjHours{
        get{
            if(prjHours==null)
            {
                prjHours='0';
               
            }
            return prjHours;
        }set;
    }
    
     public String prjHoursMon{
        get{
            if(prjHoursMon==null)
            {
                prjHoursMon='0';
               
            }
            return prjHoursMon;
        }set;
    }
    public String prjHoursTue{
        get{
            if(prjHoursTue==null)
            {
                prjHoursTue='0';
               
            }
            return prjHoursTue;
        }set;
    }
    public String prjHourswed{
        get{
            if(prjHourswed==null)
            {
                prjHourswed='0';
               
            }
            return prjHourswed;
        }set;
    }
    public String prjHoursThr{
        get{
            if(prjHoursThr==null)
            {
                prjHoursThr='0';
               
            }
            return prjHoursThr;
        }set;
    }
    public String prjHoursFri{
        get{
            if(prjHoursFri==null)
            {
                prjHoursFri='0';
               
            }
            return prjHoursFri;
        }set;
    }
    public String prjHoursSat{
        get{
            if(prjHoursSat==null)
            {
                prjHoursSat='0';
               
            }
            return prjHoursSat;
        }set;
    }
     public String prjHoursSun{
        get{
            if(prjHoursSun==null)
            {
                prjHoursSun='0';
               
            }
            return prjHoursSun;
        }set;
    }
       public string timesheetId{get;set;}
       public TimeSheetDetailWrapper(List<SelectOption> profPram,List<SelectOption> projectParam,List<SelectOption> tasklistParam)
       {
           protfoWIns=profPram;
           projectWIns=projectParam;
           taskWIns=tasklistParam;
           
       }
       
        public TimeSheetDetailWrapper(List<SelectOption> profPram,List<SelectOption> projectParam,List<SelectOption> tasklistParam
                                    ,String protfStrParam,String projectStrm,String taskStrParam,
                                    String monHrParam,String tueHrParam,String wedHrParam,String thuHrParam,
                                    String friHrParam,String satHrParam,String sunHrParam,
                                    String hoursStrParam,String timeSheetParam)
       {
           protfoWIns=profPram;
           projectWIns=projectParam;
           taskWIns=tasklistParam;
           protfStr=protfStrParam;
           prodjectStr=projectStrm;
           taskStr=taskStrParam;
           prjHoursMon=monHrParam;
           prjHoursTue=tueHrParam;
           prjHourswed=wedHrParam;
           prjHoursThr=thuHrParam;
           prjHoursFri=friHrParam;
           prjHoursSat=satHrParam;
           prjHoursSun=sunHrParam;
           prjHours=hoursStrParam;
           timesheetId=timeSheetParam;
           
       }
    }
    
   
  
}