Public Class DelegateTimesheetUsers{

    public People__c objPeopleEntry{get;set;}

    public DelegateTimesheetUsers(ApexPages.StandardController controller) 
    {
        objPeopleEntry=new People__c();
        objPeopleEntry=(People__c)controller.getrecord();
        List<People__c> lstPeople =[select Delegate_Time_sheet_User__c from People__c WHERE User_ID__c= :UserInfo.getUserId()];
        if(lstPeople.size()>0)
        { 
             objPeopleEntry.Delegate_Time_sheet_User__c=lstPeople[0].Delegate_Time_sheet_User__c;
        }
        
    }
     
   
    
    public PageReference savePeopleEntry() 
    {
    
      String People_ID;
      List<People__c > lstPeople =[select ID from People__c WHERE User_ID__c= :UserInfo.getUserId()];
      if(lstPeople.size()>0)
      { 
        
         People_ID=lstPeople[0].ID;
      }
      objPeopleEntry.ID=People_ID;
    
      update objPeopleEntry;   
      return null;

    }
      
}