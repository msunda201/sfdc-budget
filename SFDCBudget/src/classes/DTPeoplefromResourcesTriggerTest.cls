@isTest
public class DTPeoplefromResourcesTriggerTest {
    
    static Testmethod void insertResources()
    {
        Projects_NIR__c resourceprgm = new Projects_NIR__c();
        resourceprgm.Name = 'SFDC2';
        resourceprgm.Project_Description__c = 'SFDC2';
        resourceprgm.Scope__c='SFDC2';
        resourceprgm.Justification__c='SFDC2';
        insert resourceprgm;
        
        People__c ppl = new People__c();
        ppl.Name = 'Hemalatha Sukumaran';
        ppl.Resource_Type__c ='Contractor';
        ppl.Position_Description__c ='Mobile Tester';
        insert ppl;
               
        Durable_Teams__c dteam = new Durable_Teams__c();
        dteam.Name = 'team1';
        dteam.Condensed_DT__c='Applications';
        insert dteam;
        
        
        
        PeoplewithProjects__c  newpeoplewithProjects = new PeoplewithProjects__c();
        newpeoplewithProjects.People__c = ppl.id;//hema
        newpeoplewithProjects.Resource_Programs__c = resourceprgm.id ; //sfdc
        insert newpeoplewithProjects;
        
        DTwithProjects__c dtwithProjectdetail  = new DTwithProjects__c();
        dtwithProjectdetail.Resource_Projects__c = resourceprgm.id ; //sfdc
        dtwithProjectdetail.Durable_Teams__c = dteam.id; //team1
        insert dtwithProjectdetail;
        
        List<DT_with_People__c> testDTwithpeople = [select id, People__c, Durable_Teams__c  from DT_with_People__c where People__c =: ppl.id  and Durable_Teams__c =: dteam.id];
        
        System.assertEquals(testDTwithpeople.size(), 1 );
        
        }
    
    static Testmethod void deleteResources()
    {
        Projects_NIR__c resourceprgm = new Projects_NIR__c();
        resourceprgm.Name = 'SFDC2';
        resourceprgm.Project_Description__c = 'SFDC2';
        resourceprgm.Scope__c='SFDC2';
        resourceprgm.Justification__c='SFDC2';
        insert resourceprgm;
        
        People__c ppl = new People__c();
        ppl.Name = 'Hemalatha Sukumaran';
        ppl.Resource_Type__c ='Contractor';
        ppl.Position_Description__c ='Mobile Tester';
        insert ppl;
               
        
        PeoplewithProjects__c  newpeoplewithProjects = new PeoplewithProjects__c();
        newpeoplewithProjects.People__c = ppl.id;//hema
        newpeoplewithProjects.Resource_Programs__c = resourceprgm.id; //sfdc
        insert newpeoplewithProjects;
        
        List<PeoplewithProjects__c> pplprojectlist = new List <PeoplewithProjects__c>();
        pplprojectlist = [select id from PeoplewithProjects__c where People__c =: ppl.id  ];
        delete pplprojectlist;
        
    }

}