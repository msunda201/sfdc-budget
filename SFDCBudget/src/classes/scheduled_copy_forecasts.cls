global class scheduled_copy_forecasts  implements Schedulable {
   global void execute(SchedulableContext sc) {
      Copy_Forecast_Entry_Form b = new Copy_Forecast_Entry_Form (); 
      database.executebatch(b);
   }
}