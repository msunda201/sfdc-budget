@isTest
Private class ForecastEntryTest2 
{
/*
static Testmethod void insertForecastTestsfdc() {
                
                                 Budget_Category__c objBudCat = new Budget_Category__c();
                                objBudCat.Name = 'Capex - SW License';
                                objBudCat.Budget_Category_Prefix__c='CSWP';
                                objBudCat.Apply_Net_Terms__c=True;                                
                                objBudCat.Capex_Opex__c='capex';
                                objBudCat.Category_Type__c='Capex All Other';
                                objBudCat.Display_Flag__c=True;
                                objBudCat.Finance_Adjustment__c=True;
                                insert objBudCat;
                                
                                Budget_Category__c objBudCat1 = new Budget_Category__c();
                                objBudCat1.Name = 'Opex - SW Maintenance';
                                objBudCat1.Budget_Category_Prefix__c='OSWW';
                                objBudCat1.Apply_Net_Terms__c=True;                                
                                objBudCat1.Capex_Opex__c='opex';
                                objBudCat1.Category_Type__c='Opex All Other';
                                objBudCat1.Display_Flag__c=True;
                                objBudCat1.Finance_Adjustment__c=True;
                                insert objBudCat1;
                                
                                Budget_Category__c objBudCat2 = new Budget_Category__c();
                                objBudCat2.Name = 'Opex - Internal Cap Labor';
                                objBudCat2.Budget_Category_Prefix__c='OILL';
                                objBudCat2.Apply_Net_Terms__c=True;                                
                                objBudCat2.Capex_Opex__c='opex';
                                objBudCat2.Category_Type__c='Opex Internal Labor';
                                objBudCat2.Display_Flag__c=False;
                                objBudCat2.Finance_Adjustment__c=True;
                                insert objBudCat2;
                                
                                Budget_Category__c objBudCat3 = new Budget_Category__c();
                                objBudCat3.Name = 'Opex - External Labor';
                                objBudCat3.Budget_Category_Prefix__c='OELL';
                                objBudCat3.Apply_Net_Terms__c=True;                                
                                objBudCat3.Capex_Opex__c='opex';
                                objBudCat3.Category_Type__c='Opex External Labor';
                                objBudCat3.Display_Flag__c=False;
                                objBudCat3.Finance_Adjustment__c=True;
                                insert objBudCat3;

                                
                                Funding_Entity__c objFundEntity = new Funding_Entity__c();
                                objFundEntity.Name = 'Octo Group';
                                objFundEntity.Funding_Entity_ID__c = 'FOCT100';
                                insert objFundEntity;
                
                                Entity__c objEntity = new Entity__c();
                                objEntity.Name = 'Test Entity';
                                 objEntity.POR__c=TRUE;
                                objEntity.Capex_All_Other__c='Project Funded';
                                objEntity.Capex_Internal_Labor__c='Project Funded';
                                objEntity.Capex_Third_Party_Labor__c='Project Funded';
                                objEntity.Opex_All_Other__c='Project Funded';
                                objEntity.Opex_External_Labor__c='Project Funded';
                                objEntity.Opex_Internal_Labor__c='Project Funded';
                                insert objEntity;
                                
                                Director_Group__c objDirGroup = new Director_Group__c();
                                objDirGroup.Name = 'Test DirGroup';
                                objDirGroup.Entity__c = objEntity.id;
                                insert objDirGroup;
              
                                Clarity__c objClarity = new Clarity__c();
                                objClarity.Entity__c = objEntity.id;
                                insert objClarity;
                                
                                Rate__c objRate = new Rate__c();
                                objRate.Director_Group__c = objDirGroup.id;
                                objRate.EXTERNAL_LABOR_RATE_OFFSHORE__c  = 40;
                                objRate.EXTERNAL_LABOR_RATE_ONSHORE__c = 50;
                                objRate.INTERNAL_LABOR_RATE__c = 60;
                                objRate.Year__c = '2013';
                                insert objRate;
                                
                                 Rate__c objRate1 = new Rate__c();
                                objRate1.Director_Group__c = objDirGroup.id;
                                objRate1.EXTERNAL_LABOR_RATE_OFFSHORE__c  = 40;
                                objRate1.EXTERNAL_LABOR_RATE_ONSHORE__c = 50;
                                objRate1.INTERNAL_LABOR_RATE__c = 60;
                                objRate1.Year__c = '2014';
                                insert objRate1;
                                
                                Vendor2014__c vcObj = new Vendor2014__c();
                                 vcObj.Name='Euler24';                                
                                vcObj.Payment_Terms__c='Net 90';
                                insert vcObj;
                
                                Projects_2014__c objProject =new Projects_2014__c();
                                objProject.Clarity__c = objClarity.id;
                                
                                objProject.Start_Date__c = system.today();
                                objProject.End_Date__c = system.today();
                                insert objProject;
                                
                                Capability__c cObj = new Capability__c();
                                cObj.Name='QA';
                                cObj.Project__c=objProject.id;
                                insert cObj;
                                                              
                                User u1 = [SELECT Id FROM User WHERE Id='005U0000001pdqOIAQ'];
                System.RunAs(u1){
                
               System.debug('##### '+ u1);
                
                                Forecast_Entry_Form__c objForEntry3 = new Forecast_Entry_Form__c();
                                objForEntry3.Project_Name__c =objProject.id;
                               // objForEntry3.Capability__c =cObj.id;
                                objForEntry3.Director_Group__c = objDirGroup.id;
                                objForEntry3.Effort_Entity__c = objEntity.id;
                                objForEntry3.Forecast_Type__c='Software (Perpetual)';
                                objForEntry3.Total_SW_License_Amount__c = 10000;
                                objForEntry3.Total_SW_Maintenance_Amount__c=10000;
                                objForEntry3.Payment_Amount_1__c=5000;
                                objForEntry3.Payment_Date_1__c=system.today();
                                objForEntry3.Payment_Amount_2__c=5000;
                                objForEntry3.Payment_Date_2__c=system.today();
                                objForEntry3.Payment_Amount_3__c=null;
                                objForEntry3.Payment_Date_3__c=system.today();
                                objForEntry3.End_Date__c = system.today()+100;
                                objForEntry3.Start_Date__c = system.today();
                                objForEntry3.Invoice_Date__c= system.today()+10;
                                objForEntry3.Vendor__c=vcObj.id;
                                insert objForEntry3;

                               
                ApexPages.currentPage().getParameters().Put('CF00NK0000000mtdc_lkid',objProject.id);
        ApexPages.StandardController stdCtrlForEntry = new ApexPages.StandardController(objForEntry3); 
        createEditForecastEntryController objCreateEditFor = new createEditForecastEntryController(stdCtrlForEntry);
        
        objCreateEditFor.currentEntity = objEntity.id;
        objCreateEditFor.currentDirector =objDirGroup.id ;
        objCreateEditFor.getListEntity();
        objCreateEditFor.getListVendor();
        objCreateEditFor.currentEntity = objEntity.id;
        objCreateEditFor.getListDirector();
        objCreateEditFor.currentEntity = null;
        objCreateEditFor.getListDirector();
        objCreateEditFor.getListBudgetCategory();
        objCreateEditFor.saveBudgetEntry();    
        
         //  User_Preference__c objuserpref = [select Entity_ID__c,Entity__c,Director_Group__c from User_Preference__c where User__c=:u1.id ];
        
    //    System.debug('@@@@#### '+objuserpref + '');
                }
                
                     } */ 
                     }