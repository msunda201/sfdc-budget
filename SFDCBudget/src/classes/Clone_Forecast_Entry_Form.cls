global class Clone_Forecast_Entry_Form implements Database.Batchable<sObject>
{
    
    String email;
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
          date dv = system.today()-1;
          string Forecast_Calendar_Val;

          Forecast_Calendar__c listFC = null;
          //List<Forecast_Calendar__c> resultFC = [select id,Name from Forecast_Calendar__c where (Forecast_Start_Date__c <= :dv and Forecast_End_Date__c >= :dv)];
          List<Forecast_Calendar__c> resultFC = [select id,Name from Forecast_Calendar__c where Status__c='Inprogress'];
          if(resultFC.size() > 0)
          {
            Forecast_Calendar_Val= resultFC[0].id;
          }
    
    
        String query = 'SELECT Id,Name,CREATEDBYID,RecordTypeId,Capability__c,Account_Category__c,Apr__c,Adjustment_Year__c,Aug__c,Capital_Expense__c,Clone__c,Dec__c,Delivery_Date__c,Director_Group__c,Effort_Entity__c,End_Date__c,External_Labor__c,Feb__c,Forecast_Calendar__c,Forecast_Description__c,Forecast_Type__c,Funding_Entity__c,Invoice_Date__c,Jan__c,Jul__c,Jun__c,Justification__c,Mar__c,May__c,Nov__c,Oct__c,Off_Shore_Labor__c,Payment_Amount_1__c,Payment_Amount_2__c,Payment_Amount_3__c,Payment_Amount_4__c,Payment_Amount_5__c,Payment_Date_1__c,Payment_Date_2__c,Payment_Date_3__c,Payment_Date_4__c,Payment_Date_5__c,Project_Name__c,Sep__c,Start_Date__c,Total_Effort_Hours__c,Total_Payment_Amount__c,Total_Spent_Projection__c,Total_SW_License_Amount__c,Total_SW_Maintenance_Amount__c,Vendor__c,Forecast_Job__c,Duplicate_ID__c FROM Forecast_Entry_Form__c where Forecast_Calendar__c=:Forecast_Calendar_Val limit1';
        return Database.getQueryLocator(query);
    }
   

    global void execute(Database.BatchableContext BC, List<Forecast_Entry_Form__c> scope)
    {
    
         date dv = system.today();
         String Forecast_Calendar;

          Forecast_Calendar__c listFC = null;
          List<Forecast_Calendar__c> resultFC = [select id,Name from Forecast_Calendar__c where (Forecast_Start_Date__c <= :dv and Forecast_End_Date__c >= :dv)];
          
          if(resultFC.size() > 0)
          {
            Forecast_Calendar= resultFC[0].id;
          }
     
         List<Forecast_Entry_Form__c> newFEMs = new List<Forecast_Entry_Form__c>();
         //RecordType rt = [select id from RecordType where developername = 'Forecast'];
         
         system.debug('%%%%%%%%%%%%%%SCOPE'+scope);
         
         for(Forecast_Entry_Form__c a : scope)
         {  
            newFEMs.add(new Forecast_Entry_Form__c(
                            Name=a.Name,
                            CREATEDBYID=a.CREATEDBYID,
                            RecordTypeId=a.RecordTypeId,
                            Account_Category__c=a.Account_Category__c,
                            Capability__c=a.Capability__c,
                            Apr__c=a.Apr__c,
                            Aug__c=a.Aug__c,
                            Adjustment_Year__c=a.Adjustment_Year__c,
                            Capital_Expense__c=a.Capital_Expense__c,
                            Clone__c=a.Clone__c,
                            Dec__c=a.Dec__c,
                            Delivery_Date__c=a.Delivery_Date__c,
                            Director_Group__c=a.Director_Group__c,
                            Effort_Entity__c=a.Effort_Entity__c,
                            End_Date__c=a.End_Date__c,
                            External_Labor__c=a.External_Labor__c,
                            Feb__c=a.Feb__c,
                            Forecast_Calendar__c=Forecast_Calendar,
                            Forecast_Description__c=a.Forecast_Description__c,
                            Forecast_Type__c=a.Forecast_Type__c,
                            Funding_Entity__c=a.Funding_Entity__c,
                            Invoice_Date__c=a.Invoice_Date__c,
                            Jan__c=a.Jan__c,
                            Jul__c=a.Jul__c,
                            Jun__c=a.Jun__c,
                            Justification__c=a.Justification__c,
                            Mar__c=a.Mar__c,
                            May__c=a.May__c,
                            Nov__c=a.Nov__c,
                            Oct__c=a.Oct__c,
                            Off_Shore_Labor__c=a.Off_Shore_Labor__c,
                            Payment_Amount_1__c=a.Payment_Amount_1__c,
                            Payment_Amount_2__c=a.Payment_Amount_2__c,
                            Payment_Amount_3__c=a.Payment_Amount_3__c,
                            Payment_Amount_4__c=a.Payment_Amount_4__c,
                            Payment_Amount_5__c=a.Payment_Amount_5__c,
                            Payment_Date_1__c=a.Payment_Date_1__c,
                            Payment_Date_2__c=a.Payment_Date_2__c,
                            Payment_Date_3__c=a.Payment_Date_3__c,
                            Payment_Date_4__c=a.Payment_Date_4__c,
                            Payment_Date_5__c=a.Payment_Date_5__c,
                            Project_Name__c=a.Project_Name__c,
                            Sep__c=a.Sep__c,
                            Start_Date__c=a.Start_Date__c,
                            Total_Effort_Hours__c=a.Total_Effort_Hours__c,
                            Total_Payment_Amount__c=a.Total_Payment_Amount__c,
                            Total_Spent_Projection__c=a.Total_Spent_Projection__c,
                            Total_SW_License_Amount__c=a.Total_SW_License_Amount__c,
                            Total_SW_Maintenance_Amount__c=a.Total_SW_Maintenance_Amount__c,
                            Vendor__c=a.Vendor__c,
                            Duplicate_ID__c=a.Duplicate_ID__c,
                            Forecast_Job__c=TRUE,
                            Forecast_Flag__c=FALSE,
                            Forecast_Copy__c=TRUE
                            
                            ));  
                   
         }
         
         system.debug('%%%%%%%%%%%%%%SIZE'+newFEMs.size());
         
         if(newFEMs.size() > 0)
         {
             insert newFEMs;
         }
         
    }   
    
    
    global void finish(Database.BatchableContext BC)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {'mahendran_sundarraj@cable.comcast.com', 'hema_manoharan@cable.comcast.com'});
        mail.setReplyTo('mahendran_sundarraj@cable.comcast.com');
        mail.setSenderDisplayName('Forecast Batch Processing');
        mail.setSubject('Copy Forecast Entry Forms - Batch Process Completed');
        mail.setPlainTextBody('Forecast Batch Process has completed');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}