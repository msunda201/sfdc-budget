@isTest
public class IntakeProjectControllerTest {
	
	static testmethod void insertIntakeProject (){
		Funding_Entity__c objFundEntity = new Funding_Entity__c();
     	objFundEntity.Name = 'Octo Group';
     	objFundEntity.Funding_Entity_ID__c = 'FOCT100';
     	insert objFundEntity;
     	
     	Portfolio__c objPortFolio = new Portfolio__c();
     	objPortFolio.Name=' Test Port';
     	objPortFolio.Description__c = 'Test Dec';
	    insert objPortFolio;
	    
	    Comcast_Employees__c objCE = new Comcast_Employees__c();
	    objCE.Name='Hema';
	    objCE.Email_ID__c='hema_manoharan@cable.comcast.com';
	    objCE.First_Name__c='Hema';
	    insert objCE;
	    
	    Intake_Project__c objIntakeProj = new Intake_Project__c();
	    objIntakeProj.Name='test Proj';
	    objIntakeProj.Intake_Project_Description__c = 'test Decs';
	    objIntakeProj.Business_Service_Intake_ID__c='345';
	    objIntakeProj.Analysis_Type__c='Detailed';
	    objIntakeProj.Portfolio__c = objPortFolio.id;
	    objIntakeProj.Status__c='New';
	    objIntakeProj.Project_Type__c='POR';
	    objIntakeProj.Funded_by__c='POR';
	    objIntakeProj.SBI__c='BSS/OSS';
	    objIntakeProj.Funding_Entity__c= objFundEntity.id;
	    objIntakeProj.Budgeting_Year__c='2013';
	    objIntakeProj.Submitted_Date__c=system.today();
	    objIntakeProj.Finance_Owner__c=objCE.id;
	    objIntakeProj.Clone__c = TRUE;
	    insert objIntakeProj;
	    
	    ApexPages.StandardController stdCtrlBudEntry = new ApexPages.StandardController(objIntakeProj); 
	    CreateEditIntakeProjectController objCreateEditIntakePro = new CreateEditIntakeProjectController(stdCtrlBudEntry);
	    objCreateEditIntakePro.getListFunding();
	    objCreateEditIntakePro.saveIntakeProjectEntry();
	    
	}
	
	static testmethod void insertIntakeProject1 (){
		Funding_Entity__c objFundEntity = new Funding_Entity__c();
     	objFundEntity.Name = 'Octo Group';
     	objFundEntity.Funding_Entity_ID__c = 'FOCT100';
     	insert objFundEntity;
     	
     	Portfolio__c objPortFolio = new Portfolio__c();
     	objPortFolio.Name=' Test Port';
     	objPortFolio.Description__c = 'Test Dec';
	    insert objPortFolio;
	    
	    Comcast_Employees__c objCE = new Comcast_Employees__c();
	    objCE.Name='Hema';
	    objCE.Email_ID__c='hema_manoharan@cable.comcast.com';
	    objCE.First_Name__c='Hema';
	    insert objCE;
	    
	    Intake_Project__c objIntakeProj = new Intake_Project__c();
	    objIntakeProj.Name='test Proj';
	    objIntakeProj.Intake_Project_Description__c = 'test desc1';
	    objIntakeProj.Business_Service_Intake_ID__c='345';
	    objIntakeProj.Analysis_Type__c='Detailed';
	    objIntakeProj.Portfolio__c = objPortFolio.id;
	    objIntakeProj.Status__c='New';
	    objIntakeProj.Project_Type__c='POR';
	    objIntakeProj.Funded_by__c='POR';
	    objIntakeProj.SBI__c='BSS/OSS';
	    objIntakeProj.Funding_Entity__c= objFundEntity.id;
	    objIntakeProj.Budgeting_Year__c='2013';
	    objIntakeProj.Submitted_Date__c=system.today();
	    objIntakeProj.Finance_Owner__c=objCE.id;
	    objIntakeProj.Clone__c = TRUE;
	    insert objIntakeProj;
	    
	    ApexPages.StandardController stdCtrlBudEntry = new ApexPages.StandardController(objIntakeProj); 
	    CreateEditIntakeProjectController objCreateEditIntakePro = new CreateEditIntakeProjectController(stdCtrlBudEntry);
	    objCreateEditIntakePro.getListFunding();
	    objCreateEditIntakePro.saveIntakeProjectEntry();
	    
	}
}