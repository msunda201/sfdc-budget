/*
 * EditableContactListExtension.cls
 *
 * Copyright (c)2013, Michael Welburn.
 * License: MIT
 *
 * Usage:
 *   This is the implementation class where Object specific references
 *   need to be defined. Please see EditableList.cls for more information
 *   on what each method is responsible for.
 *
 *   Required methods:
 *     - EditableContactListExtension(ApexPages.StandardController)
 *     - getChildren()
 *     - initChildRecord()
 *
 */
public with sharing class EditableTimesheetListExtension extends EditableList
{
  // Read the explanation in EditableContactListExtension(ApexPages.StandardController)
  // to see when to uncomment the following line.

  // public Account myAccount {get; private set;}
     public Timesheet_Header__c objTimesheetDetail{get;private set;}
     public List<Timesheet__c> Timesheet{get; set;}
            


  public EditableTimesheetListExtension(ApexPages.StandardController stdController) 
  {
    super(stdController);
    
    //objTSEntry=new Timesheet__c();
    //objTSEntry=(Timesheet__c)controller.getrecord();
        
    // If necessary, explicitly query for additional metadata on parent record
    // if you are looking to display things that don't come back with the
    // StandardController. In that case, you will need to replace the "Account.X"
    // references in the Visualforce Page with references to an Account variable
    // that you will need to declare in this class (myAccount.X). I have commented out
    // an example.

    // this.myAccount = [SELECT Id,
    //                            Name,
    //                            Custom_Relationship__r.Name
    //                        FROM Account
    //                        WHERE Id =: stdController.getRecord().Id];
    
    this.childList = [SELECT Id,Timesheet_Calendar__c,Timesheet_Period__c,Portfolio__c,People__c,Manager__c,Resource_Projects__c,Status__c,Task__c,Time_sheet_Hours__c
                      FROM Timesheet__c
                      WHERE Timesheet_Header__c =: mysObject.Id];
  }

  /*
   * This method is necessary for reference on the Visualforce page, 
   * in order to reference non-standard fields.
   */
  public List<Timesheet__c> getChildren()
  {
    return (List<Timesheet__c>)childList;
  }
  
   public List<SelectOption> getListPortfolio() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        
        
        //if(String.IsBlank(objBudEntry.Project_Name__c )==true)
          //  return options;
        for(Portfolio__c p2:[select ID,Name from Portfolio__c  order by Name]) {
            String nameStr = p2.Name;
            if (nameStr.length() > 50) {
            nameStr = p2.Name.substring(0,50);
            } else {
                nameStr = p2.Name;
            }
            options.add(new SelectOption(p2.ID,nameStr));
        }
        return options;
    }
    
    public List<SelectOption> getListProject() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        
        
        //if(String.IsBlank(objBudEntry.Project_Name__c )==true)
          //  return options;
        //for(Projects_NIR__c p2:[select ID,Name,Porfolio_Name__c from Projects_NIR__c where Portfolio_ID__c=:objTSEntry.Portfolio__c order by Name]) 
        for(Projects_NIR__c p2:[select ID,Name,Porfolio_Name__c from Projects_NIR__c order by Name]) 
        {
            String nameStr = p2.Porfolio_Name__c;
            if (nameStr.length() > 50) {
            nameStr = p2.Porfolio_Name__c.substring(0,50);
            } else {
                nameStr = p2.Porfolio_Name__c;
            }
            options.add(new SelectOption(p2.ID,nameStr));
        }
        return options;
    }

    
    public List<SelectOption> getListTask() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        
        
        //if(String.IsBlank(objBudEntry.Project_Name__c )==true)
          //  return options;
        for(Task__c p2:[select ID,Name from Task__c  order by Name]) {
            String nameStr = p2.Name;
            if (nameStr.length() > 50) {
            nameStr = p2.Name.substring(0,50);
            } else {
                nameStr = p2.Name;
            }
            options.add(new SelectOption(p2.ID,nameStr));
        }
        return options;
    }


  
 
  public override sObject initChildRecord()
  {
    
        Timesheet__c child = new Timesheet__c();
        // Can either use mysObject or acct here
        child.Timesheet_Header__c = mysObject.Id;
        return child;
     
  }
  
  
}