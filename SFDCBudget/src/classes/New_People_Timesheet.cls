global class New_People_Timesheet implements Database.Batchable<sObject>
{
    
    String email;
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
    
          date dv = system.today();
          string Timesheet_Calendar_Val;
          List<Timesheet_Calendar__c> resultTC = [select id,End_Date__c,Start_Date__c from Timesheet_Calendar__c where (Start_Date__c <= :dv and End_Date__c >= :dv)];
          if(resultTC.size() > 0)
          {
            Timesheet_Calendar_Val= resultTC[0].id;
          }
             
          String Withdrawn='WithDrawn'; 
          String query = 'select Id, Manager_c__c,Manager_User_ID__c,Name,Resource_Type__c,Manager_Email_ID__c,Work_Email__c,User_ID__c from People__c where Timesheet_Required__c=true AND Empl_Status_Desc__c!=:Withdrawn AND Time_Sheet_Override_Flag__c=false AND Id not In (select People__c from Timesheet_Header__c where Timesheet_Calendar__c=:Timesheet_Calendar_Val)';
          system.debug('%%%%%%%%%%%%%%query'+query);
          return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<People__c> scope)
    {
          date dv = system.today();
          string Timesheet_Calendar_Val;
          string Owner_Userid;
          Date Timesheet_End_Date;
          Timesheet_Calendar__c listTC = null;
          List<Timesheet_Calendar__c> resultTC = [select id,End_Date__c,Start_Date__c from Timesheet_Calendar__c where (Start_Date__c <= :dv and End_Date__c >= :dv)];
          if(resultTC.size() > 0)
          {
            Timesheet_Calendar_Val= resultTC[0].id;
            Timesheet_End_Date= resultTC[0].End_Date__c;
          }
    
         List<Timesheet_Header__c> newFEMs = new List<Timesheet_Header__c>();

         
         for(People__c p : scope)
         {  

            Timesheet_Header__c newpeopleTH = new Timesheet_Header__c();
            newpeopleTH.Timesheet_Calendar__c=Timesheet_Calendar_Val;
            newpeopleTH.People__c =  p.Id;
            newpeopleTH.Name= String.valueOf(p.Name) + ' -' +Timesheet_End_Date.month()+'/'+Timesheet_End_Date.day()+'/'+Timesheet_End_Date.year() ;
            newpeopleTH.Status__c='New'; 
            
            //if(p.User_ID__c!=null)
            //{
                system.debug('%%%%%%%%%%%%%%Manager Email ID'+ p.User_ID__c);   

                if(p.Resource_Type__c=='Employee')
                {
                          if(p.Manager_User_ID__c!=NULL)
                          {
                             Owner_Userid=p.Manager_User_ID__c;  
                          } 
                          else
                          {
                             Owner_Userid=UserInfo.getUserId();
                          }
                 }
                 else
                 {

                      if(p.User_ID__c!=NULL)
                      {
                        Owner_Userid=p.User_ID__c;
                      } 
                      else
                      {
                         Owner_Userid=UserInfo.getUserId();
                      }
                 }
            //}
            //else
            //{
            //        Manager_Userid=UserInfo.getUserId();
            //}    
            

            
            
            newpeopleTH.OwnerId=Owner_Userid;
            newFEMs.add(newpeopleTH);
            system.debug('%%%%%%%%%%%%%%Inside People list'+ newFEMs.size());   
         }
        
       
         if(newFEMs.size() > 0)
         {
             system.debug('%%%%%%%%%%%%%%newFEMs'+newFEMs.size());    
             insert newFEMs;
         }
    
    }   
    
    global void finish(Database.BatchableContext BC)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {'mahendran_sundarraj@cable.comcast.com'});
        mail.setReplyTo('mahendran_sundarraj@cable.comcast.com');
        mail.setSenderDisplayName('Batch Processing - New People Timesheet');
        mail.setSubject('New People Timesheet - Batch Process Completed');
        mail.setPlainTextBody('New People Timesheet - Batch Process has completed');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}