global class Copy_Forecast_Entry_Form implements Database.Batchable<sObject>
{
    
    String email;
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
          date dv = system.today()-1;
          string Forecast_Calendar_Val;

          Forecast_Calendar__c listFC = null;
          //List<Forecast_Calendar__c> resultFC = [select id,Name from Forecast_Calendar__c where (Forecast_Start_Date__c <= :dv and Forecast_End_Date__c >= :dv)];
          List<Forecast_Calendar__c> resultFC = [select id,Name from Forecast_Calendar__c where Status__c='Inprogress'];
          if(resultFC.size() > 0)
          {
            Forecast_Calendar_Val= resultFC[0].id;
          }
    
    
        String query = 'SELECT Id,Name,RecordTypeId,Account_Category__c,Apr__c,Aug__c,Capital_Expense__c,Clone__c,Dec__c,Delivery_Date__c,Director_Group__c,Effort_Entity__c,End_Date__c,External_Labor__c,Feb__c,Forecast_Calendar__c,Forecast_Description__c,Forecast_Type__c,Funding_Entity__c,Invoice_Date__c,Jan__c,Jul__c,Jun__c,Justification__c,Mar__c,May__c,Nov__c,Oct__c,Off_Shore_Labor__c,Payment_Amount_1__c,Payment_Amount_2__c,Payment_Amount_3__c,Payment_Amount_4__c,Payment_Amount_5__c,Payment_Date_1__c,Payment_Date_2__c,Payment_Date_3__c,Payment_Date_4__c,Payment_Date_5__c,Project_Name__c,Sep__c,Start_Date__c,Total_Effort_Hours__c,Total_Payment_Amount__c,Total_Spent_Projection__c,Total_SW_License_Amount__c,Total_SW_Maintenance_Amount__c,Vendor__c,Forecast_Job__c,Duplicate_ID__c FROM Forecast_Entry_Form__c where Forecast_Calendar__c=:Forecast_Calendar_Val';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Forecast_Entry_Form__c> scope)
    {
         date dv = system.today();
         String Forecast_Calendar;

          Forecast_Calendar__c listFC = null;
          List<Forecast_Calendar__c> resultFC = [select id,Name from Forecast_Calendar__c where (Forecast_Start_Date__c <= :dv and Forecast_End_Date__c >= :dv)];
          
          if(resultFC.size() > 0)
          {
            Forecast_Calendar= resultFC[0].id;
          }
          
        
    
         List<Forecast_Entry_Form__c> newFEMs = new List<Forecast_Entry_Form__c>();
         for(Forecast_Entry_Form__c a : scope)
         {  
            Forecast_Entry_Form__c newFEM = a.clone(false, true, false, false);
            newFEM.Forecast_Calendar__c=Forecast_Calendar;
            newFEM.Forecast_Job__c=TRUE;
            newFEM.Forecast_Flag__c=FALSE;
            newFEM.Forecast_Copy__c=TRUE;
            newFEMs.add(newFEM);
         }
         
         if(newFEMs.size() > 0)
         {
         insert newFEMs;
         }
    }   
    
    global void finish(Database.BatchableContext BC)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {'mahendran_sundarraj@cable.comcast.com'});
        mail.setReplyTo('mahendran_sundarraj@cable.comcast.com');
        mail.setSenderDisplayName('Batch Processing');
        mail.setSubject('Copy Forecast Entry Forms - Batch Process Completed');
        mail.setPlainTextBody('Batch Process has completed');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}