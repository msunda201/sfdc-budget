public class selectedSizeSubmit {
    
    ApexPages.StandardSetController setCon;
    public String selids;
    public list<string> lstRecId;
    public list<Timesheet_Header__c> lstTH {get;set;}
    public Timesheet_Header__c tmHead{set;get;}
    public selectedSizeSubmit (ApexPages.StandardSetController controller) {
        tmHead = new Timesheet_Header__c();
        selids = ApexPages.currentPage().getParameters().get('selids');
        string tmpstatus= ApexPages.currentPage().getParameters().get('tmpstatus');    

        lstTH = new list<Timesheet_Header__c>();
       if(selids!=null && selids!=''){
            lstRecId = selids.split(',');
            if(lstRecId != null && lstRecId.size() > 0){
                lstTH = [select Id,Name,Status__c from Timesheet_Header__c where id in : lstRecId];
            }
        }else{
            lstRecId = new list<string>();
            setCon = controller;
        }
        
        tmHead.Status__c=tmpstatus;
    }

    public integer getMySelectedSize() {
        if(!lstRecId.isEmpty())
            return lstRecId.size();
        else
            return setCon.getSelected().size();
    }
    public integer getMyRecordsSize() {
        if(!lstRecId.isEmpty())
            return lstRecId.size();
        else
            return setCon.getRecords().size();
    }
    
    public pagereference saveRec(){
        if(lstTH != null && lstTH.size() > 0)
        {
        
        
 
            for(Timesheet_Header__c tmh : lstTH )
            {
                            
                tmh.Status__c = 'Submit'; 
                
            }
       
            update lstTH;
        }
        return new pagereference('/apex/My_Timesheets');
    }
}