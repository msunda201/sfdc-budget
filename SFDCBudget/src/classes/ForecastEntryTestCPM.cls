@isTest
Private class ForecastEntryTestCPM {
   
@isTest(SeeAllData=true)              
static void insertForecastTest7cpm() {
                
                                  Budget_Category__c objBudCat = new Budget_Category__c();
                                objBudCat.Name = 'Capex - SW License';
                                objBudCat.Budget_Category_Prefix__c='CSWP';
                                objBudCat.Apply_Net_Terms__c=True;                                
                                objBudCat.Capex_Opex__c='capex';
                                objBudCat.Category_Type__c='Capex All Other';
                                objBudCat.Display_Flag__c=True;
                                objBudCat.Finance_Adjustment__c=True;
                                insert objBudCat;
                                
                                Budget_Category__c objBudCat1 = new Budget_Category__c();
                                objBudCat1.Name = 'Opex - SW Maintenance';
                                objBudCat1.Budget_Category_Prefix__c='OSWW';
                                objBudCat1.Apply_Net_Terms__c=True;                                
                                objBudCat1.Capex_Opex__c='opex';
                                objBudCat1.Category_Type__c='Opex All Other';
                                objBudCat1.Display_Flag__c=True;
                                objBudCat1.Finance_Adjustment__c=True;
                                insert objBudCat1;
                                
                                Budget_Category__c objBudCat2 = new Budget_Category__c();
                                objBudCat2.Name = 'Opex - Internal Cap Labor';
                                objBudCat2.Budget_Category_Prefix__c='OILL';
                                objBudCat2.Apply_Net_Terms__c=True;                                
                                objBudCat2.Capex_Opex__c='opex';
                                objBudCat2.Category_Type__c='Opex Internal Labor';
                                objBudCat2.Display_Flag__c=False;
                                objBudCat2.Finance_Adjustment__c=True;
                                insert objBudCat2;
                                
                                Budget_Category__c objBudCat3 = new Budget_Category__c();
                                objBudCat3.Name = 'Opex - External Labor';
                                objBudCat3.Budget_Category_Prefix__c='OELL';
                                objBudCat3.Apply_Net_Terms__c=True;                                
                                objBudCat3.Capex_Opex__c='opex';
                                objBudCat3.Category_Type__c='Opex External Labor';
                                objBudCat3.Display_Flag__c=False;
                                objBudCat3.Finance_Adjustment__c=True;
                                insert objBudCat3;

                                
                              
                
                                Entity__c objEntity = new Entity__c();
                                objEntity.Name = 'Test Entity';
                                 objEntity.POR__c=TRUE;
                                objEntity.Capex_All_Other__c='Project Funded';
                                objEntity.Capex_Internal_Labor__c='Project Funded';
                                objEntity.Capex_Third_Party_Labor__c='Project Funded';
                                objEntity.Opex_All_Other__c='Project Funded';
                                objEntity.Opex_External_Labor__c='Project Funded';
                                objEntity.Opex_Internal_Labor__c='Project Funded';
                                insert objEntity;
                                
                                Director_Group__c objDirGroup = new Director_Group__c();
                                objDirGroup.Name = 'Test DirGroup';
                                objDirGroup.Entity__c = objEntity.id;
                                insert objDirGroup;
              
                                Clarity__c objClarity = new Clarity__c();
                                objClarity.Entity__c = objEntity.id;
                                insert objClarity;
                                
                                Rate__c objRate = new Rate__c();
                                objRate.Director_Group__c = objDirGroup.id;
                                objRate.EXTERNAL_LABOR_RATE_OFFSHORE__c  = 40;
                                objRate.EXTERNAL_LABOR_RATE_ONSHORE__c = 50;
                                objRate.INTERNAL_LABOR_RATE__c = 60;
                                objRate.Year__c = '2013';
                                insert objRate;
                                
                                 Rate__c objRate1 = new Rate__c();
                                objRate1.Director_Group__c = objDirGroup.id;
                                objRate1.EXTERNAL_LABOR_RATE_OFFSHORE__c  = 40;
                                objRate1.EXTERNAL_LABOR_RATE_ONSHORE__c = 50;
                                objRate1.INTERNAL_LABOR_RATE__c = 60;
                                objRate1.Year__c = '2014';
                                insert objRate1;
                                
                                Vendor2014__c vcObj = new Vendor2014__c();
                                 vcObj.Name='TBD';                                
                                vcObj.Payment_Terms__c='Net 90';
                                insert vcObj;
                
                                Projects_2014__c objProject =new Projects_2014__c();
                                objProject.Name='Demo Project';
                                objProject.Clarity__c = objClarity.id;
                                objProject.Project_Type__c='NETO';
                                objProject.Funded_by__c='NETO';
                                objProject.Start_Date__c = system.today();
                                objProject.End_Date__c = system.today();
                                
                                insert objProject;
                                
                                Funding_Entity__c objFundEntity = new Funding_Entity__c();
                                objFundEntity.Name = objProject.Funded_by__c;
                                objFundEntity.Funding_Entity_ID__c = 'FOCT100';
                                insert objFundEntity;
                                
                                Capability__c cObj = new Capability__c();
                                cObj.Name='QA';
                                cObj.Project__c=objProject.id;
                                insert cObj;
                                
                                 
                                                              
                                //User U =[Select id from User where Id='005U000000211jl'];                
                //System.RunAs(U)
                //{
                
               
                
                             Forecast_Entry_Form__c objBudEntry3= new Forecast_Entry_Form__c();
                                objBudEntry3.Project_Name__c =objProject.id;
                                //objForEntry.Capability__c =cObj.id;
                                objBudEntry3.Director_Group__c = objDirGroup.id;
                                objBudEntry3.Effort_Entity__c = objEntity.id;
                                objBudEntry3.Forecast_Type__c='Labor (Lump Sum $)';
                                objBudEntry3.Clone__c=TRUE;
                                objBudEntry3.Total_Spent_Projection__c=40000;
                                objBudEntry3.External_Labor__c =60;
                                objBudEntry3.End_Date__c = system.today()+50;
                                objBudEntry3.Start_Date__c = system.today()-100;
                                objBudEntry3.Off_Shore_Labor__c =40;        
                                objBudEntry3.Capital_Expense__c =50;
                                objBudEntry3.Vendor__c=vcObj.id;
                                insert objBudEntry3;
                                
                               
                               
                ApexPages.currentPage().getParameters().Put('CF00NJ0000000x8dS_lkid',objProject.id);
        ApexPages.StandardController stdCtrlForEntry = new ApexPages.StandardController(objBudEntry3); 
        createEditForecastEntryController objCreateEditFor = new createEditForecastEntryController(stdCtrlForEntry);
        
        objCreateEditFor.currentEntity = objEntity.id;
        objCreateEditFor.currentDirector =objDirGroup.id ;
        objCreateEditFor.getListEntity();
        objCreateEditFor.getListFunding();
        objCreateEditFor.getListVendor();
        objCreateEditFor.currentEntity = objEntity.id;
        objCreateEditFor.getListDirector();
        objCreateEditFor.currentEntity = null;
        objCreateEditFor.getListDirector();
        objCreateEditFor.getListBudgetCategory();
        objCreateEditFor.saveBudgetEntry();    
        
          
                //}
                
                User u2 =[Select id from User where Id='005U000000211jl'];
    
                 User_Preference__c objuserpref = new User_Preference__c();
        objuserpref.Director_Group__c =objDirGroup.id;
        objuserpref.Entity__c=objEntity.id;
        objuserpref.User__c=u2.id;
        insert objuserpref ;
        
        System.debug('@@@@#### '+objuserpref);
                     }  






}