@isTest
public class TestSelectedSizeWorkaround{
    
    static Testmethod void test(){
        Timesheet_Calendar__c timeCal = new Timesheet_Calendar__c(Name='Test Calendar',Start_Date__c=system.today(),End_Date__c=system.today().addDays(7));
        insert timeCal;
        
        Timesheet_Header__c timeHeader = new Timesheet_Header__c(Name='Test Header',Timesheet_Calendar__c=timeCal.Id);
        insert timeHeader;
        
        selectedSizeWorkaround sel = new selectedSizeWorkaround (new ApexPages.StandardSetController(new List<Timesheet_Header__c>{timeHeader}));
        sel.getMySelectedSize();
        sel.getMyRecordsSize();
    }
}