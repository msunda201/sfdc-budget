global class BatchPeopleController implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([select id,Name,email from User where isActive=true]);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<User> listUser = (List<User>)scope;
        Set<Id> userIds = new Set<Id>();
        if(listUser != null && listUser.size() > 0){
            for(User usr : listUser){
                userIds.add(usr.Id);
            }
            SendEmailConroller.sendEmail(userIds);
        }
    }
    
    global void finish(Database.BatchableContext BC){
    
    }
}