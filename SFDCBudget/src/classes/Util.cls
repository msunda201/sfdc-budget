public class Util {
    @future
    public static void insertComcastUser(
        String fullName, String userid, String FirstName, String email) {

        system.debug('inside util'); 
         List<Comcast_Employees__c> sr = new List<Comcast_Employees__c>();
                   
                  
	     system.debug('before adding');
	     sr.add(new Comcast_Employees__c(
	        Name=fullName,
	        User__c=userid,
	        First_Name__c=FirstName,
	        Email_ID__c = email
	        ));   
                            
         if(sr.size() > 0)
            insert sr;
        
        
    }
}