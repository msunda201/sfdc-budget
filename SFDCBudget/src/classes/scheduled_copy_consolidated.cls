global class scheduled_copy_consolidated  implements Schedulable {
   global void execute(SchedulableContext sc) {
      Copy_Forecasts b = new Copy_Forecasts (); 
      database.executebatch(b);
   }
}