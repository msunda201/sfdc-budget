Public Class createEditBudgetEntryController{
    public Budget_Entry_Form__c objBudEntry{get;set;}
    public CommonFunctions common = null;
   
    
    // Constructor method with StandardController object
    
    public createEditBudgetEntryController(ApexPages.StandardController controller){
        objBudEntry=new Budget_Entry_Form__c();
        objBudEntry=(Budget_Entry_Form__c)controller.getrecord();
        common = CommonFunctions.getInstance(controller);
       
        //SandboxCF00NU0000003JOHV
        //String strProjectId = ApexPages.currentPage().getParameters().get('CF00NK0000000mtdc_lkid');
        //Prod
        String strProjectId = ApexPages.currentPage().getParameters().get('CF00NU0000003JOHV_lkid');
        String strCapabilityId = ApexPages.currentPage().getParameters().get('CF00NU0000003JYXH_lkid');
        
        system.debug('### inside get listing strCapabilityId-->'+strCapabilityId);
        
        String strCapabilityname = ApexPages.currentPage().getParameters().get('CF00NU0000003JYXH');
        string strProject = ApexPages.currentPage().getParameters().get('Project__c');    
        string strId = ApexPages.currentPage().getParameters().get('id');    
        
        if(strId==null)
        {
            //User current_user=[SELECT id FROM User WHERE Id= :UserInfo.getUserId()] ;
            List<User_Preference__c> lstUserPref =[select Entity_ID__c,Entity__c,Director_Group__c from User_Preference__c where User__c = :UserInfo.getUserId()];
            if(lstUserPref.size()>0)
            {
                objBudEntry.Effort_Entity__c=lstUserPref[0].Entity_ID__c;
                objBudEntry.Director_Group__c=lstUserPref[0].Director_Group__c;
            }
        }
        
        objBudEntry.Clone__c=FALSE;
        
        if(ApexPages.currentPage().getParameters().get('clone')!=NULL)
        {
             objBudEntry.Clone__c=TRUE;
        }   
        
        //Set Default Vendor
        if(objBudEntry.Vendor2014__c==null)
        {
             string strVendorName='TBD';
             List<Vendor2014__c> lstVendor =[select id from Vendor2014__c where Name = :strVendorName];
             if(lstVendor.size()>0)
             {
                 objBudEntry.Vendor2014__c = lstVendor[0].id;
             }
        }
        
            
        //if(strProjectId !=null && strProjectId!='CF00NK0000000mtdc_lkid')
        if(strCapabilityId !=null)
        {
            List<Capability__c> lstCapability =[select id,Name,Project__c from Capability__c where id = :strCapabilityId];
            if(lstCapability.size()>0)
            {
                objBudEntry.Capability__c = lstCapability[0].id;
                strProjectId=lstCapability[0].Project__c;
            }
        
        }
        system.debug('### inside get listing strProjectId-->'+strProjectId);
        
        if(strProjectId !=null)
        {
             system.debug('### Venkat Garu Came'+strProjectId);
            List<Projects_2014__c> lstProject =[select id,Project_Type__c,Funded_by__c,Clarity__c,Funding_Entity_ID__c,Start_Date__c,End_Date__c from Projects_2014__c where id = :strProjectId];
           
            if(lstProject.size()>0)
            {
                objBudEntry.Project__c    = lstProject[0].id;
                
                objBudEntry.Start_Date__c = lstProject[0].Start_Date__c;
                objBudEntry.End_Date__c = lstProject[0].End_Date__c;
                objBudEntry.Invoice_Date__c = lstProject[0].Start_Date__c;
                if(lstProject[0].Project_Type__c=='POR')
                {
                    objBudEntry.Funding_Entity_ID__c = lstProject[0].Funding_Entity_ID__c;
                }
                else
                {
                    List<Funding_Entity__c> lstFunding =[select id from Funding_Entity__c where Name = :lstProject[0].Funded_by__c];
                    if(lstFunding.size()>0)
                    {
                      objBudEntry.Funding_Entity_ID__c = lstFunding[0].id;             
                    }
                }
            }
           
            
        }
        

             }   
    
    
    public string currentEntity { get; set; }
    public string currentDirector {get;set;}
    
    public Decimal EXTERNAL_LABOR_RATE_OFFSHORE {get;set;}
    public Decimal EXTERNAL_LABOR_RATE_ONSHORE {get;set;}
    public Decimal INTERNAL_LABOR_RATE {get;set;}
    public Date CDate {get;set;}
    public integer CYear {get;set;}
    public integer CYear1 {get;set;}
    public integer CYear2 {get;set;}
    /*// Constructor method
    
    public createEditBudgetEntryController(){
    
       
        currentEntity = currentDirector= null;
    }*/
    
    // saveBudgetEntry method for save BudgetEntry
     public PageReference saveBudgetEntry() 
    {
        System.debug('inside saveBudgetEntry');
        String Date_val='';
                 try{
             if(objBudEntry.Budget_Type__c =='Labor (By Effort)')
            {
                System.debug('inside if L by Eff');
                RecordType rt = [select id from RecordType where developername = 'Labor_By_Effort'];
                objBudEntry.RecordTypeId=rt.id;
                Date_val='True';
            }
            else if(objBudEntry.Budget_Type__c =='Labor (Lump Sum $)')
            {
                RecordType rt = [select id from RecordType where developername = 'Labor_Lump_Sum'];
                objBudEntry.RecordTypeId=rt.id;
                Date_val='True';
            }
            else if(objBudEntry.Budget_Type__c =='Other Costs (Monthly)')
            {
                RecordType rt = [select id from RecordType where developername = 'Other_Costs_Monthly'];
                objBudEntry.RecordTypeId=rt.id;
                Date_val='True';
            }
            else if(objBudEntry.Budget_Type__c =='Other Costs (Single Payment)')
            {
                RecordType rt = [select id from RecordType where developername = 'Other_Costs_Single_Payment'];
                objBudEntry.RecordTypeId=rt.id;
                Date_val='False';
            }
            else if(objBudEntry.Budget_Type__c =='HW Asset')
            {
                RecordType rt = [select id from RecordType where developername = 'HW_Asset'];
                objBudEntry.RecordTypeId=rt.id;
                Date_val='True';
            }
            else if(objBudEntry.Budget_Type__c =='Software (Perpetual)')
            {
                RecordType rt = [select id from RecordType where developername = 'Software_Perpetual'];
                objBudEntry.RecordTypeId=rt.id;
                Date_val='True';
            }
             else if(objBudEntry.Budget_Type__c =='Multiple Month by Account')
            {
                RecordType rt = [select id from RecordType where developername = 'Budget_Multiple_Month_by_Account'];
                objBudEntry.RecordTypeId=rt.id;
                Date_val='True';
            }
            else
            {
            RecordType rt = [select id from RecordType where developername = 'Finance_Adjustment'];
            objBudEntry.RecordTypeId=rt.id;
            Date_val='';
            }
            
            
            List <Projects_2014__c> objBudgProj = [select  budget_setting__c, Name from Projects_2014__c where id =: objBudEntry.Project__c  ] ;
            System.debug('#########################LIst Projects -->' + objBudgProj[0].budget_setting__c );
            List <Budget_Setting__c> objBudSetting = [select id,  Maximum_Date__c, Minimum_Date__c  from Budget_Setting__c where id =: objBudgProj[0].budget_setting__c ];

           if (objBudSetting.size() > 0 && Date_val=='True') 
           {
                            if(objBudEntry.Start_Date__c < objBudSetting[0].Minimum_Date__c)
                             {
                                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter Start Date Greater than '+  String.valueof(objBudSetting[0].Minimum_Date__c));
                                    ApexPages.addMessage(myMsg);
                                    return null;
                             }
                              
                             if(objBudEntry.End_Date__c > objBudSetting[0].Maximum_Date__c)
                             {
                                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter End Date Less than '+ String.valueof(objBudSetting[0].Maximum_Date__c));
                                    ApexPages.addMessage(myMsg);
                                    return null;
                             }
                             
                             if((objBudEntry.Payment_Date_1__c < objBudSetting[0].Minimum_Date__c)||(objBudEntry.Payment_Date_2__c < objBudSetting[0].Minimum_Date__c)||(objBudEntry.Payment_Date_3__c < objBudSetting[0].Minimum_Date__c)||(objBudEntry.Payment_Date_4__c < objBudSetting[0].Minimum_Date__c)||(objBudEntry.Payment_Date_5__c < objBudSetting[0].Minimum_Date__c))
                             {
                                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter Software Perpetual Payment Date Between '+  String.valueof(objBudSetting[0].Minimum_Date__c) +' and '+ String.valueof(objBudSetting[0].Maximum_Date__c) );
                                    ApexPages.addMessage(myMsg);
                                    return null;
                             }
                             
                              if((objBudEntry.Payment_Date_1__c > objBudSetting[0].Maximum_Date__c)||(objBudEntry.Payment_Date_2__c > objBudSetting[0].Maximum_Date__c)||(objBudEntry.Payment_Date_3__c > objBudSetting[0].Maximum_Date__c)||(objBudEntry.Payment_Date_4__c > objBudSetting[0].Maximum_Date__c)||(objBudEntry.Payment_Date_5__c > objBudSetting[0].Maximum_Date__c))
                             {
                                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter Software Perpetual Date Between '+  String.valueof(objBudSetting[0].Minimum_Date__c) +' and '+ String.valueof(objBudSetting[0].Maximum_Date__c) );
                                    ApexPages.addMessage(myMsg);
                                    return null;
                             }
           }
           
           if (objBudSetting.size() > 0 && Date_val=='False') 
           {
                            if((objBudEntry.Invoice_Date__c < objBudSetting[0].Minimum_Date__c) || (objBudEntry.Invoice_Date__c > objBudSetting[0].Maximum_Date__c))
                             {
                                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter Invoice Date Between '+  String.valueof(objBudSetting[0].Minimum_Date__c) +' and '+ String.valueof(objBudSetting[0].Maximum_Date__c) );
                                    ApexPages.addMessage(myMsg);
                                    return null;
                             }
                              
                             if((objBudEntry.Delivery_Date__c > objBudSetting[0].Maximum_Date__c)||(objBudEntry.Delivery_Date__c < objBudSetting[0].Minimum_Date__c))
                             {
                                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter Delivery Date Between '+  String.valueof(objBudSetting[0].Minimum_Date__c) +' and '+ String.valueof(objBudSetting[0].Maximum_Date__c) );
                                    ApexPages.addMessage(myMsg);
                                    return null;
                             }
           }
           objBudEntry.Disable_Triggers__c=FALSE;
           if(objBudEntry.Clone__c==TRUE)
           {
                    objBudEntry.id=NULL;
                    objBudEntry.Clone__c=FALSE;
                    insert objBudEntry;
           }
           else
           {
                    upsert objBudEntry;
           }
            
            PageReference p = new PageReference('/'+objBudEntry.id);
            System.debug('after setting page reference');
            return p;
       
       }
       catch(Exception e)
       {
             //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
             //ApexPages.addMessage(myMsg);
             ApexPages.addMessages(e);
           
             return null;
       }
       
        
    }
    
     
    //  getListEntity method for get List of Entity from Entity__c object
    
    public List<SelectOption> getListEntity(){
              
        List<SelectOption> options = new List<SelectOption>();
        options= common.getCommonListEntity();
        return options;
    }
    
    // getListDirector method for get List of Director from Director_Group__c
    
    public List<SelectOption> getListDirector() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        
        /*if(currentEntity == null || currentEntity == '')
            return options;
        for(Director_Group__c p2:[select ID,Name from Director_Group__c where Entity__c = :currentEntity]) {
            options.add(new SelectOption(p2.ID,p2.Name));
        }*/
        if(String.IsBlank(objBudEntry.Effort_Entity__c)==true)
            return options;
        for(Director_Group__c p2:[select ID,Name from Director_Group__c where Entity__c = :objBudEntry.Effort_Entity__c  order by Name]) {
            options.add(new SelectOption(p2.ID,p2.Name));
        }
        return options;
    }
    
    /////////////////////////////////////////////
    public List<SelectOption> getListVendor() {
     
        List<SelectOption> options = new List<SelectOption>();
        options= common.getListVendor();
        return options;
        
    }
    
    /////////////////////////////////////////////
    public List<SelectOption> getListBudgetCategory() {
    
        List<SelectOption> options = new List<SelectOption>();
        options= common.getListAccountCategory();
        return options; 
    }
    ///////////////////////////////////////////////////////////////////
    public List<SelectOption> getListFunding() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
      system.debug('### inside get listing objBudEntry.Project__c-->'+objBudEntry.Name);
       
      system.debug('###objBudEntry.Project__c-->'+objBudEntry.Project__c);
      
      if(objBudEntry.Funding_Entity_ID__c==null)
      {
        if(String.IsBlank(objBudEntry.Project__c)==true)
            return options;
                
            List<Projects_2014__c> lstProject =[select id,Project_Type__c,Funded_by__c,Clarity__c,Funding_Entity_ID__c,Start_Date__c,End_Date__c from Projects_2014__c where id = :objBudEntry.Project__c];
            
            if(lstProject.size()>0)
            {
               if(lstProject[0].Project_Type__c=='POR')
                {
                    objBudEntry.Funding_Entity_ID__c = lstProject[0].Funding_Entity_ID__c;
                }
                else
                {
                    List<Funding_Entity__c> lstFunding =[select id from Funding_Entity__c where Name = :lstProject[0].Funded_by__c];
                    if(lstFunding.size()>0)
                    {
                      objBudEntry.Funding_Entity_ID__c = lstFunding[0].id;             
                    }
                }
            }    
         }     
     
        for(Funding_Entity__c p2:[select ID,Name from Funding_Entity__c  order by Name]) {
            options.add(new SelectOption(p2.ID,p2.Name));
        }
        return options;
        
      
    }

    
    
    public List<SelectOption> getListCapability() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        
        
        if(String.IsBlank(objBudEntry.Project__c)==true)
            return options;
        for(Capability__c p2:[select ID,Name from Capability__c where Project__c = :objBudEntry.Project__c  order by Name]) {
            String nameStr = p2.Name;
            if (nameStr.length() > 50) {
            nameStr = p2.Name.substring(0,50);
            } else {
                nameStr = p2.Name;
            }
            options.add(new SelectOption(p2.ID,nameStr));
        }
        return options;
    }
    
           
    //////////////////////////////////////////////////
}