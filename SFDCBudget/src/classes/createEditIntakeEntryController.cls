public Class createEditIntakeEntryController {
    public Intake_Entry_Form__c objIntakeEntry {get;set;}
    public string currentEntity { get; set; }
    public string currentDirector {get;set;}
    public string strProjectId {get;set;}
    public CommonFunctions common = null;
    
                   
    public createEditIntakeEntryController(ApexPages.StandardController controller){
        common = CommonFunctions.getInstance(controller);
        objIntakeEntry = new Intake_Entry_Form__c();
        objIntakeEntry =(Intake_Entry_Form__c) controller.getRecord();
        string strId = ApexPages.currentPage().getParameters().get('id');    
        if(strId==null)
        {
            User current_user=[SELECT id FROM User WHERE Id= :UserInfo.getUserId()] ;
            List<User_Preference__c> lstUserPref =[select Entity_ID__c,Entity__c,Director_Group__c from User_Preference__c where User__c = :UserInfo.getUserId()];
            if(lstUserPref.size()>0)
            {
                objIntakeEntry.Effort_Entity__c=lstUserPref[0].Entity_ID__c;
                objIntakeEntry.Director_Group__c=lstUserPref[0].Director_Group__c;
            }
            
             string strVendorName='TBD';
             List<Vendor2014__c> lstVendor =[select ID from Vendor2014__c where Name = :strVendorName];
             if(lstVendor.size()>0)
             {
                 objIntakeEntry.Vendor__c = lstVendor[0].ID;
             }
             system.debug('Vendor__c'+objIntakeEntry.Vendor__c);
        
        }
        objIntakeEntry.Clone__c=FALSE;
        
        if(ApexPages.currentPage().getParameters().get('clone')!=NULL)
        {
             objIntakeEntry.Clone__c=TRUE;
        }
        date dv = system.today();

     
     }
       
           
    //  getListEntity method for get List of Entity from Entity__c object
    
        public List<SelectOption> getCommonListEntity(){
        List<SelectOption> options = new List<SelectOption>();
        options= common.getCommonListEntity();
        return options;
        
    }
    
          
      
   
    // getListDirector method for get List of Director from Director_Group__c
    
    public List<SelectOption> getUsualListDirector() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        
       if(String.IsBlank(objIntakeEntry.Effort_Entity__c)==true)
            return options;
        for(Director_Group__c p2:[select ID,Name from Director_Group__c where Entity__c = :objIntakeEntry.Effort_Entity__c  order by Name]) {
            options.add(new SelectOption(p2.ID,p2.Name));
        }
        return options;
    }
    /////////////////////////////////////////////
    public List<SelectOption> getListVendor() {
        
        List<SelectOption> options = new List<SelectOption>();
        options= common.getListVendor();
        return options;
    }
    
    public List<SelectOption> getListIntakeProjectFunding() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        
       
       system.debug('###objIntakeEntry.Intake_Project__c-->'+objIntakeEntry.Intake_Project__c);
       if(String.IsBlank(objIntakeEntry.Intake_Project__c)==true)
            return options;
                
            List<Intake_Project__c> lstProject =[select id,Project_Type__c,Funded_by__c,Funding_Entity__c from Intake_Project__c where id = :objIntakeEntry.Intake_Project__c];
            
            if(lstProject.size()>0)
            {
                        system.debug('### selected Projects funding entity ->'+ lstProject[0].Funding_Entity__c);
                    List<Funding_Entity__c> lstFunding =[select id from Funding_Entity__c where id = :lstProject[0].Funding_Entity__c];
                    if(lstFunding.size()>0)
                    {
                      objIntakeEntry.Funding_Entity__c = lstFunding[0].id;             
                    }
               
            }    
              
     
        for(Funding_Entity__c p2:[select ID,Name from Funding_Entity__c  order by Name]) {
            options.add(new SelectOption(p2.ID,p2.Name));
        }
        return options;
        
      
    }
    
    
    /////////////////////////////////////////////
    public List<SelectOption> getListAccountCategory() {
       
        List<SelectOption> options = new List<SelectOption>();
        options= common.getListAccountCategory();
        return options; 
        
    }
    
   public List<SelectOption> getListCapability() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Choose --'));
        
        if(String.IsBlank(objIntakeEntry.Intake_Project__c)==true)
            return options;
        for(Intake_Capabilities__c p2:[select ID,Name from Intake_Capabilities__c where Intake_Project__c = :objIntakeEntry.Intake_Project__c  order by Name]) {
            options.add(new SelectOption(p2.ID,p2.Name));
        }
        return options;
    }
    
    
    public PageReference saveIntakeEntry() 
    {
          try{
        
            if(objIntakeEntry.Intake_Type__c =='Labor (By Effort)')
            {
                RecordType rt = [select id from RecordType where developername = 'Intake_Labor_By_Effort'];
                objIntakeEntry.RecordTypeId=rt.id;
            }
            else if(objIntakeEntry.Intake_Type__c =='Labor (Lump Sum $)')
            {
                RecordType rt = [select id from RecordType where developername = 'Intake_Labor_By_Lump_Sum'];
                objIntakeEntry.RecordTypeId=rt.id;
            }
            else if(objIntakeEntry.Intake_Type__c =='Other Costs (Monthly)')
            {
                RecordType rt = [select id from RecordType where developername = 'Intake_Other_Costs_Monthly'];
                objIntakeEntry.RecordTypeId=rt.id;
            }
            else if(objIntakeEntry.Intake_Type__c =='Other Costs (Single Payment)')
            {
                RecordType rt = [select id from RecordType where developername = 'Intake_Other_Costs_Single_Payment'];
                objIntakeEntry.RecordTypeId=rt.id;
            }
            else if(objIntakeEntry.Intake_Type__c =='Software (Perpetual)')
            {
                RecordType rt = [select id from RecordType where developername = 'Intake_Software_Perpetual'];
                objIntakeEntry.RecordTypeId=rt.id;
            }
            else if(objIntakeEntry.Intake_Type__c == 'Multiple Month by Account')
            {
                RecordType rt = [select id from RecordType where developername = 'Intake_Multiple_Month_by_Account'];
                objIntakeEntry.RecordTypeId=rt.id;
            }
            else
            {
            RecordType rt = [select id from RecordType where developername = 'Intake_Labor_By_Effort'];
            objIntakeEntry.RecordTypeId=rt.id;
            }
         /////////////////////////////////////////////   
          if(objIntakeEntry.Clone__c==TRUE)
                {
                    //objBudEntry.Forecast_Created_User__c=current_user.id;
                    objIntakeEntry.id=NULL;
                    objIntakeEntry.Clone__c=FALSE;
                    insert objIntakeEntry;
                }
                else
                {
                    upsert objIntakeEntry;
                }
               
        PageReference pgRef = new PageReference('/'+objIntakeEntry.id);
        return pgRef;
       
       }
       catch(Exception e)
       {
             ApexPages.addMessages(e);
             return null;
       }
        
    
    }

}