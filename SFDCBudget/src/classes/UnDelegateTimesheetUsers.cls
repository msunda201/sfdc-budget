Public Class UnDelegateTimesheetUsers{

    public People__c objPeopleEntry{get;set;}

    public UnDelegateTimesheetUsers(ApexPages.StandardController controller) 
    {
        objPeopleEntry=new People__c();
        objPeopleEntry=(People__c)controller.getrecord();

        
    }
     
   
    
    public PageReference savePeopleEntry() 
    {
        String People_ID;
        List<People__c > lstPeople =[select ID from People__c WHERE User_ID__c= :UserInfo.getUserId()];
        if(lstPeople.size()>0)
        { 
        
             People_ID=lstPeople[0].ID;
        }
        objPeopleEntry.ID=People_ID;
      
         update objPeopleEntry;
         return null;

    }
    
    //  getListEntity method for get List of Entity from Entity__c object
    
    
   
}