/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PeopleallocationDeleteTriggerTest {

    static testMethod void insertPeopleallocation() {
        // TO DO: implement unit test
        
                Projects_NIR__c resourceprgm = new Projects_NIR__c();
        resourceprgm.Name = 'SFDC2';
        resourceprgm.Project_Description__c = 'SFDC2';
        resourceprgm.Scope__c='SFDC2';
        resourceprgm.Justification__c='SFDC2';
        insert resourceprgm;
        
        People__c ppl = new People__c();
        ppl.Name = 'Hemalatha Sukumaran';
        ppl.Resource_Type__c ='Contractor';
        ppl.Position_Description__c ='Mobile Tester';
        insert ppl;
               
        PeoplewithProjects__c resources = new PeoplewithProjects__c();
        resources.Resource_Programs__c = resourceprgm.id; //sfdc2
        resources.People__c = ppl.id;//hemalatha
        resources.Year__c = '2014';
        resources.Jan__c = 50;
        resources.Feb__c= 50;
        insert resources;     
        
  }
  
  static Testmethod void deletePeopleallocation()
    {
        Projects_NIR__c resourceprgm = new Projects_NIR__c();
        resourceprgm.Name = 'SFDC2';
        resourceprgm.Project_Description__c = 'SFDC2';
        resourceprgm.Scope__c='SFDC2';
        resourceprgm.Justification__c='SFDC2';
        insert resourceprgm;
        
        People__c ppl = new People__c();
        ppl.Name = 'Hemalatha Sukumaran';
        ppl.Resource_Type__c ='Contractor';
        ppl.Position_Description__c ='Mobile Tester';
        insert ppl;
        
        PeoplewithProjects__c resources = new PeoplewithProjects__c();
        resources.Resource_Programs__c = resourceprgm.id; //sfdc2
        resources.People__c = ppl.id;//hemalatha
        resources.Year__c = '2014';
        resources.Jan__c = 50;
        resources.Feb__c= 50;
        insert resources;    
        
        List<PeoplewithProjects__c> pplprojectlist = new List <PeoplewithProjects__c>();
        pplprojectlist = [select id from PeoplewithProjects__c where People__c =: ppl.id ];
        if (pplprojectlist.size() > 0)
        delete pplprojectlist;
        
    }
}